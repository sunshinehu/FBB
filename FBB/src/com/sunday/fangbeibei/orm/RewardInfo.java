package com.sunday.fangbeibei.orm;
/**
 * 

* @ClassName;// RewardInfo 

* @Description;// TODO(佣金列表) 

* @date 2015年2月26日 下午2;//52;//20 

*
 */
public class RewardInfo {

	private String id;// 6,
	private String createTime;// "2015-02-09 21;//11;//42",
	private String updateTime;// "2015-02-12 18;//04;//40",
	private String isDeleted;// 1,
	private String premisesId;// 6,
	private String userId;// 12,
	private String storeId;// 4,
	private String build;// "14",
	private String unit;// "3",
	private String floor;
	private String room;// "702",
	private String reportId;// 17,
	private String commission;// 10000,
	private String nopyedcommission;// 9000,
	private String premisesName;// "小雅店铺",
	private String userName;// "孙雯1"
	private String amount;//: 1000,
	private String managerName;
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getCreateTime() {
		return createTime;
	}
	public void setCreateTime(String createTime) {
		this.createTime = createTime;
	}
	public String getUpdateTime() {
		return updateTime;
	}
	public void setUpdateTime(String updateTime) {
		this.updateTime = updateTime;
	}
	public String getIsDeleted() {
		return isDeleted;
	}
	public void setIsDeleted(String isDeleted) {
		this.isDeleted = isDeleted;
	}
	public String getPremisesId() {
		return premisesId;
	}
	public void setPremisesId(String premisesId) {
		this.premisesId = premisesId;
	}
	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}
	public String getStoreId() {
		return storeId;
	}
	public void setStoreId(String storeId) {
		this.storeId = storeId;
	}
	public String getBuild() {
		return build;
	}
	public void setBuild(String build) {
		this.build = build;
	}
	public String getUnit() {
		return unit;
	}
	public void setUnit(String unit) {
		this.unit = unit;
	}
	public String getRoom() {
		return room;
	}
	public void setRoom(String room) {
		this.room = room;
	}
	public String getReportId() {
		return reportId;
	}
	public void setReportId(String reportId) {
		this.reportId = reportId;
	}
	public String getCommission() {
		return commission;
	}
	public void setCommission(String commission) {
		this.commission = commission;
	}
	public String getNopyedcommission() {
		return nopyedcommission;
	}
	public void setNopyedcommission(String nopyedcommission) {
		this.nopyedcommission = nopyedcommission;
	}
	public String getPremisesName() {
		return premisesName;
	}
	public void setPremisesName(String premisesName) {
		this.premisesName = premisesName;
	}
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	public String getAmount() {
		return amount;
	}
	public void setAmount(String amount) {
		this.amount = amount;
	}
	public String getManagerName() {
		return managerName;
	}
	public void setManagerName(String managerName) {
		this.managerName = managerName;
	}
	public String getFloor() {
		return floor;
	}
	public void setFloor(String floor) {
		this.floor = floor;
	}
	
	
}
