package com.sunday.fangbeibei.orm;

/**
 * 基本结果
 * @author 晨曦
 *
 */

public class BaseResult<T> {

	private int code;
	private boolean ok;
	private String message;
	private T result;
	
	
	public int getCode() {
		return code;
	}
	public void setCode(int code) {
		this.code = code;
	}
	public boolean isOk() {
		return ok;
	}
	public void setOk(boolean ok) {
		this.ok = ok;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public T getResult() {
		return result;
	}
	public void setResult(T result) {
		this.result = result;
	}
	
	
	
}
