package com.sunday.fangbeibei.orm;
/**
 * 

* @ClassName;// HouseLayout 

* @Description;// TODO(户型) 

* @author sunshineHu hucx@itouch.com.cn

* @date 2015年2月6日 下午2;//20;//37 

*
 */
public class HouseLayout {

	private String ct;// 0,
	private String ut;// 0,
	private String id;// 1,
	private String premisesId;// 6,
	private String type;// "户型A",
	private String name;// "一室一厅",
	private String content;// "这样的户型让人舒服",
	private String area;// null,
	private String isDeleted;// null,
	private String images;// null
	
	
	public String getCt() {
		return ct;
	}
	public void setCt(String ct) {
		this.ct = ct;
	}
	public String getUt() {
		return ut;
	}
	public void setUt(String ut) {
		this.ut = ut;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getPremisesId() {
		return premisesId;
	}
	public void setPremisesId(String premisesId) {
		this.premisesId = premisesId;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getContent() {
		return content;
	}
	public void setContent(String content) {
		this.content = content;
	}
	public String getArea() {
		return area;
	}
	public void setArea(String area) {
		this.area = area;
	}
	public String getIsDeleted() {
		return isDeleted;
	}
	public void setIsDeleted(String isDeleted) {
		this.isDeleted = isDeleted;
	}
	public String getImages() {
		return images;
	}
	public void setImages(String images) {
		this.images = images;
	}
	
	
	
}
