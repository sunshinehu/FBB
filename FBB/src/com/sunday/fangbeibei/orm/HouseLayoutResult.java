package com.sunday.fangbeibei.orm;

import java.util.List;

public class HouseLayoutResult extends BaseResult{

	private List<HouseLayout> list;

	public List<HouseLayout> getList() {
		return list;
	}

	public void setList(List<HouseLayout> list) {
		this.list = list;
	}
	
}
