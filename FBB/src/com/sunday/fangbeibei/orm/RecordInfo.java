package com.sunday.fangbeibei.orm;
/**
 * 记录信息
 * @author 晨曦
 *
 */
public class RecordInfo {

	private String ct;// 1423323526922,
	private String ut;// 1423323526922,
	private String id;// 8,
	private String isDeleted;// 0,
	private String reportId;// 16,
	private int status;// 1,
	private String creatTime;// "2015-02-07 23;//38;//46"
	public String getCt() {
		return ct;
	}
	public void setCt(String ct) {
		this.ct = ct;
	}
	public String getUt() {
		return ut;
	}
	public void setUt(String ut) {
		this.ut = ut;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getIsDeleted() {
		return isDeleted;
	}
	public void setIsDeleted(String isDeleted) {
		this.isDeleted = isDeleted;
	}
	public String getReportId() {
		return reportId;
	}
	public void setReportId(String reportId) {
		this.reportId = reportId;
	}
	public String getCreatTime() {
		return creatTime;
	}
	public void setCreatTime(String creatTime) {
		this.creatTime = creatTime;
	}
	public int getStatus() {
		return status;
	}
	public void setStatus(int status) {
		this.status = status;
	}
	
	
	
}
