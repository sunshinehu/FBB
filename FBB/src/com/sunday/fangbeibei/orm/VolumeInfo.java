package com.sunday.fangbeibei.orm;
/**
 * 报备客户
 * @author 晨曦
 *
 */
public class VolumeInfo {

	private String id;// 7,
	private String createTime;// "2015-02-27 11;//41;//21",
	private String updateTime;// "2015-02-27 11;//41;//21",
	private String isDeleted;// 1,
	private String premisesId;// 20,
	private String userId;// 9,
	private String storeId;// 4,
	private String build;// "1",
	private String unit;// "1",
	private String room;// "101",
	private String reportId;// 121,
	private long commission;// 5000,
	private long nopyedcommission;// 5000,
	private String premisesName;// "香山四季公馆",
	private String userName;// "于洲",
	private String managerName;// "liu"
	
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getCreateTime() {
		return createTime;
	}
	public void setCreateTime(String createTime) {
		this.createTime = createTime;
	}
	public String getUpdateTime() {
		return updateTime;
	}
	public void setUpdateTime(String updateTime) {
		this.updateTime = updateTime;
	}
	public String getIsDeleted() {
		return isDeleted;
	}
	public void setIsDeleted(String isDeleted) {
		this.isDeleted = isDeleted;
	}
	public String getPremisesId() {
		return premisesId;
	}
	public void setPremisesId(String premisesId) {
		this.premisesId = premisesId;
	}
	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}
	public String getStoreId() {
		return storeId;
	}
	public void setStoreId(String storeId) {
		this.storeId = storeId;
	}
	public String getBuild() {
		return build;
	}
	public void setBuild(String build) {
		this.build = build;
	}
	public String getUnit() {
		return unit;
	}
	public void setUnit(String unit) {
		this.unit = unit;
	}
	public String getRoom() {
		return room;
	}
	public void setRoom(String room) {
		this.room = room;
	}
	public String getReportId() {
		return reportId;
	}
	public void setReportId(String reportId) {
		this.reportId = reportId;
	}
	public String getPremisesName() {
		return premisesName;
	}
	public void setPremisesName(String premisesName) {
		this.premisesName = premisesName;
	}
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	public String getManagerName() {
		return managerName;
	}
	public void setManagerName(String managerName) {
		this.managerName = managerName;
	}
	public long getCommission() {
		return commission;
	}
	public void setCommission(long commission) {
		this.commission = commission;
	}
	public long getNopyedcommission() {
		return nopyedcommission;
	}
	public void setNopyedcommission(long nopyedcommission) {
		this.nopyedcommission = nopyedcommission;
	}
	
	
}
