package com.sunday.fangbeibei.orm;

import java.util.List;

public class RecordHouseResult extends BaseResult{

	private List<RecordHouse> list;

	public List<RecordHouse> getList() {
		return list;
	}

	public void setList(List<RecordHouse> list) {
		this.list = list;
	}
	
	
}
