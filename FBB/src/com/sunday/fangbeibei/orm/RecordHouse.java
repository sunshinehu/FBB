package com.sunday.fangbeibei.orm;

import java.util.List;

public class RecordHouse {

	private long ct;
	private long ut;
	
	private String id;
	
	private Long userId;// '报备的经纪人的id',
	private Long premisesId;//'报备的楼盘的id',
	private Long customerId;//'客户信息id',
	private Integer status;//报备状态


	private String mobile;//'被报备人的电话号码',
	private String name;// '被报备人的姓名',
	private String premisesName;//'报备的楼盘的名称',
	
	private String headerMobi;//: "15068885754",
	private String headerName;//: "小楼",
	
	private List<RecordInfo> records;
	
	private boolean flag;
	
	public Long getUserId() {
		return userId;
	}
	public void setUserId(Long userId) {
		this.userId = userId;
	}
	public Long getPremisesId() {
		return premisesId;
	}
	public void setPremisesId(Long premisesId) {
		this.premisesId = premisesId;
	}
	public Long getCustomerId() {
		return customerId;
	}
	public void setCustomerId(Long customerId) {
		this.customerId = customerId;
	}
	public Integer getStatus() {
		return status;
	}
	public void setStatus(Integer status) {
		this.status = status;
	}
	public String getMobile() {
		return mobile;
	}
	public void setMobile(String mobile) {
		this.mobile = mobile;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getPremisesName() {
		return premisesName;
	}
	public void setPremisesName(String premisesName) {
		this.premisesName = premisesName;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public List<RecordInfo> getRecords() {
		return records;
	}
	public void setRecords(List<RecordInfo> records) {
		this.records = records;
	}
	public boolean isFlag() {
		return flag;
	}
	public void setFlag(boolean flag) {
		this.flag = flag;
	}
	public String getHeaderMobi() {
		return headerMobi;
	}
	public void setHeaderMobi(String headerMobi) {
		this.headerMobi = headerMobi;
	}
	public String getHeaderName() {
		return headerName;
	}
	public void setHeaderName(String headerName) {
		this.headerName = headerName;
	}
	public long getCt() {
		return ct;
	}
	public void setCt(long ct) {
		this.ct = ct;
	}
	public long getUt() {
		return ut;
	}
	public void setUt(long ut) {
		this.ut = ut;
	}
	
	
	
}
