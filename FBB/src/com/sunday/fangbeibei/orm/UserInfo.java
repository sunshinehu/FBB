package com.sunday.fangbeibei.orm;
/**
 * 

* @ClassName;// UserInfo 

* @Description;// TODO(用户信息) 

* @author sunshineHu hucx@itouch.com.cn

* @date 2015年2月5日 下午3;//31;//52 

*
 */
public class UserInfo {

	private String ct;// 0,
	private String ut;// 0,
	private String id;// 4,
	private String isDeleted;// 0,
	private String name;// null,
	private String nickname;// null,
	private String mobi;// "13588247463",
	private String storeId;// null,
	private String password;// "",
	private int type;// 3,
	private String storeName;// null
	private String storeCode;
	private String districtId;// null,
	private String cityName;// null,
	
	private String path;
	private int sex;//: 3,;
	
	public String getCt() {
		return ct;
	}
	public void setCt(String ct) {
		this.ct = ct;
	}
	public String getUt() {
		return ut;
	}
	public void setUt(String ut) {
		this.ut = ut;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getIsDeleted() {
		return isDeleted;
	}
	public void setIsDeleted(String isDeleted) {
		this.isDeleted = isDeleted;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getNickname() {
		return nickname;
	}
	public void setNickname(String nickname) {
		this.nickname = nickname;
	}
	public String getMobi() {
		return mobi;
	}
	public void setMobi(String mobi) {
		this.mobi = mobi;
	}
	public String getStoreId() {
		return storeId;
	}
	public void setStoreId(String storeId) {
		this.storeId = storeId;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public int getType() {
		return type;
	}
	public void setType(int type) {
		this.type = type;
	}
	public String getStoreName() {
		return storeName;
	}
	public void setStoreName(String storeName) {
		if(storeName==null){
			storeName="";
		}
		this.storeName = storeName;
	}
	public String getDistrictId() {
		return districtId;
	}
	public void setDistrictId(String districtId) {
		this.districtId = districtId;
	}
	public String getCityName() {
		return cityName;
	}
	public void setCityName(String cityName) {
		this.cityName = cityName;
	}
	public String getPath() {
		return path;
	}
	public void setPath(String path) {
		this.path = path;
	}
	public int getSex() {
		return sex;
	}
	public void setSex(int sex) {
		this.sex = sex;
	}
	public String getStoreCode() {
		return storeCode;
	}
	public void setStoreCode(String storeCode) {
		this.storeCode = storeCode;
	}
	
	
}
