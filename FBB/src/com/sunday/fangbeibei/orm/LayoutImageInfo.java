package com.sunday.fangbeibei.orm;

import java.io.Serializable;
import java.util.ArrayList;

public class LayoutImageInfo implements Serializable{

	 /**
	 * 
	 */
	private static final long serialVersionUID = 6114088073417156893L;
	private String ct;// 0,
     private String ut;// 0,
     private String id;// 5,
     private String premisesId;// 3,
     private String type;// private String 三室两厅private String ,
     private String name;// private String 上下private String ,
     private String content;// private String 最新户型private String ,
     private String area;// null,
     private String isDeleted;// null,
     private ArrayList<ImageInfo> images;// [
     
     
	public String getCt() {
		return ct;
	}
	public void setCt(String ct) {
		this.ct = ct;
	}
	public String getUt() {
		return ut;
	}
	public void setUt(String ut) {
		this.ut = ut;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getPremisesId() {
		return premisesId;
	}
	public void setPremisesId(String premisesId) {
		this.premisesId = premisesId;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getContent() {
		return content;
	}
	public void setContent(String content) {
		this.content = content;
	}
	public String getArea() {
		return area;
	}
	public void setArea(String area) {
		this.area = area;
	}
	public String getIsDeleted() {
		return isDeleted;
	}
	public void setIsDeleted(String isDeleted) {
		this.isDeleted = isDeleted;
	}
	public ArrayList<ImageInfo> getImages() {
		return images;
	}
	public void setImages(ArrayList<ImageInfo> images) {
		this.images = images;
	}
     
	
}
