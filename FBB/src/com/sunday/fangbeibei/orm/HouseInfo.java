package com.sunday.fangbeibei.orm;

import java.io.Serializable;
import java.util.List;

/**
 * 

* @ClassName: HouseInfo 

* @Description: TODO(这里用一句话描述这个类的作用) 

* @date 2015年2月5日 下午5:04:22 

*
 */
public class HouseInfo implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 3676745550222726137L;
	private String id;
	private String image;// '主图',
	private String realestateName; // '开发商',
	private String name; // '楼盘的名字',
	private String openingTime;// '开盘时间',
	private String ropertyCompany; // '物业公司',
	private String propertyType; // '物业类型',
	private String buildingType; // '建筑类型',
	private String area ; // '建筑面积',
	private String decoration; // '装修情况',
	private String totalamount; // '总户数',
	private String parkingamount; // '车位数',
	private String floorAreaRatio; // '容积率',
	private String greenRation; // '绿化率',
	private String propertyCost; // '物业费',
	private String address; // '地址',
	private String activity; // '活动',
	private String benefit; // '优惠',
	private String reward; // '奖励',
	private String commission;// '佣金比例',
	private String activityremark; // '活动备注',
	private String benefitremark; // '优惠备注',
	private String rewardremark; // '奖励备注',
	private String commissionremark;// '佣金备注', 
	private String manageramount;// '合作经纪人数',
	private String customeramount; // '客户数',
	private String mycustomeramount;
	private String proviceId;// '省ID',
    private String districtId;//市id',
    private String areaId;// '区域id',
    private String postion;//位置信息
    private String price;
    private int isfav;
    private int isReported;// 标记这个楼盘是否已经报备
    private String lng;
    private String lat;
    
    private String headerName;//: "暂无",
    private String headerMobi;//: "13175016608",
    
    private long ct;//: 1423560013076,
    private long ut;//: 1423560013076,结佣时间
    
    private List<BlogContent> blogs;
    
    
    
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getImage() {
		return image;
	}
	public void setImage(String image) {
		this.image = image;
	}
	public String getRealestateName() {
		return realestateName;
	}
	public void setRealestateName(String realestateName) {
		this.realestateName = realestateName;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getOpeningTime() {
		return openingTime;
	}
	public void setOpeningTime(String openingTime) {
		this.openingTime = openingTime;
	}
	public String getRopertyCompany() {
		return ropertyCompany;
	}
	public void setRopertyCompany(String ropertyCompany) {
		this.ropertyCompany = ropertyCompany;
	}
	public String getPropertyType() {
		return propertyType;
	}
	public void setPropertyType(String propertyType) {
		this.propertyType = propertyType;
	}
	public String getBuildingType() {
		return buildingType;
	}
	public void setBuildingType(String buildingType) {
		this.buildingType = buildingType;
	}
	public String getArea() {
		return area;
	}
	public void setArea(String area) {
		this.area = area;
	}
	public String getDecoration() {
		return decoration;
	}
	public void setDecoration(String decoration) {
		this.decoration = decoration;
	}
	public String getTotalamount() {
		return totalamount;
	}
	public void setTotalamount(String totalamount) {
		this.totalamount = totalamount;
	}
	public String getParkingamount() {
		return parkingamount;
	}
	public void setParkingamount(String parkingamount) {
		this.parkingamount = parkingamount;
	}
	public String getFloorAreaRatio() {
		return floorAreaRatio;
	}
	public void setFloorAreaRatio(String floorAreaRatio) {
		this.floorAreaRatio = floorAreaRatio;
	}
	public String getGreenRation() {
		return greenRation;
	}
	public void setGreenRation(String greenRation) {
		this.greenRation = greenRation;
	}
	public String getPropertyCost() {
		return propertyCost;
	}
	public void setPropertyCost(String propertyCost) {
		this.propertyCost = propertyCost;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public String getActivity() {
		return activity;
	}
	public void setActivity(String activity) {
		this.activity = activity;
	}
	public String getBenefit() {
		return benefit;
	}
	public void setBenefit(String benefit) {
		this.benefit = benefit;
	}
	public String getReward() {
		if(reward==null){
			reward="";
		}
		return reward;
	}
	public void setReward(String reward) {
		this.reward = reward;
	}
	public String getCommission() {
		return commission;
	}
	public void setCommission(String commission) {
		this.commission = commission;
	}
	public String getActivityremark() {
		return activityremark;
	}
	public void setActivityremark(String activityremark) {
		this.activityremark = activityremark;
	}
	public String getBenefitremark() {
		return benefitremark;
	}
	public void setBenefitremark(String benefitremark) {
		this.benefitremark = benefitremark;
	}
	public String getRewardremark() {
		return rewardremark;
	}
	public void setRewardremark(String rewardremark) {
		this.rewardremark = rewardremark;
	}
	public String getCommissionremark() {
		return commissionremark;
	}
	public void setCommissionremark(String commissionremark) {
		this.commissionremark = commissionremark;
	}
	public String getProviceId() {
		return proviceId;
	}
	public void setProviceId(String proviceId) {
		this.proviceId = proviceId;
	}
	public String getDistrictId() {
		return districtId;
	}
	public void setDistrictId(String districtId) {
		this.districtId = districtId;
	}
	public String getAreaId() {
		return areaId;
	}
	public void setAreaId(String areaId) {
		this.areaId = areaId;
	}
	public String getPostion() {
		return postion;
	}
	public void setPostion(String postion) {
		this.postion = postion;
	}
	public String getPrice() {
		if(price==null){
			price="";
		}
		return price;
	}
	public void setPrice(String price) {
		this.price = price;
	}
	public int getIsfav() {
		return isfav;
	}
	public void setIsfav(int isfav) {
		this.isfav = isfav;
	}
	public String getLng() {
		return lng;
	}
	public void setLng(String lng) {
		this.lng = lng;
	}
	public String getLat() {
		return lat;
	}
	public void setLat(String lat) {
		this.lat = lat;
	}
	public int getIsReported() {
		return isReported;
	}
	public void setIsReported(int isReported) {
		this.isReported = isReported;
	}
	public String getManageramount() {
		if(manageramount==null||manageramount.equals("")){
			manageramount="0";
		}
		return manageramount;
	}
	public void setManageramount(String manageramount) {
		this.manageramount = manageramount;
	}
	public String getCustomeramount() {
		if(customeramount==null||customeramount.equals("")){
			customeramount="0";
		}
		return customeramount;
	}
	public void setCustomeramount(String customeramount) {
		this.customeramount = customeramount;
	}
	public String getMycustomeramount() {
		if(mycustomeramount==null||mycustomeramount.equals("")){
			mycustomeramount="0";
		}
		return mycustomeramount;
	}
	public void setMycustomeramount(String mycustomeramount) {
		this.mycustomeramount = mycustomeramount;
	}
	public String getHeaderName() {
		return headerName;
	}
	public void setHeaderName(String headerName) {
		this.headerName = headerName;
	}
	public String getHeaderMobi() {
		return headerMobi;
	}
	public void setHeaderMobi(String headerMobi) {
		this.headerMobi = headerMobi;
	}
	public long getCt() {
		return ct;
	}
	public void setCt(long ct) {
		this.ct = ct;
	}
	public long getUt() {
		return ut;
	}
	public void setUt(long ut) {
		this.ut = ut;
	}
	public List<BlogContent> getBlogs() {
		return blogs;
	}
	public void setBlogs(List<BlogContent> blogs) {
		this.blogs = blogs;
	}
    
	
}
