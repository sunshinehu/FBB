package com.sunday.fangbeibei.orm;

import java.util.List;

/**
 * 
 * @author SunshineHu
 *
 */
public class MessageResult extends BaseResult{

	private List<MessageInfo> list;

	public List<MessageInfo> getList() {
		return list;
	}

	public void setList(List<MessageInfo> list) {
		this.list = list;
	}
	
}
