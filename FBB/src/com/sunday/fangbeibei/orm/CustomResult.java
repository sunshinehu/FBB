package com.sunday.fangbeibei.orm;

import java.util.List;

public class CustomResult extends BaseResult{

	private List<CustomInfo> list;

	public List<CustomInfo> getList() {
		return list;
	}

	public void setList(List<CustomInfo> list) {
		this.list = list;
	}
	
	
	
}
