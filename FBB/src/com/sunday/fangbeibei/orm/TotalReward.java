package com.sunday.fangbeibei.orm;
/**
 * 

* @ClassName;// TotalReward 

* @Description;// TODO(这里用一句话描述这个类的作用) 

* @date 2015年2月26日 下午12;//42;//10 

*
 */
public class TotalReward {

	private String id;// 3,
	private String createTime;// "2015-02-09 21;//10;//15",
	private String updateTime;// "2015-02-12 18;//04;//40",
	private String isDeleted;// 1,
	private String storeId;// 4,
	private String totalcommission;// 25000,
	private String payedcommission;// 4000,
	private String notpayedcommission;// 21000
	
	
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getCreateTime() {
		return createTime;
	}
	public void setCreateTime(String createTime) {
		this.createTime = createTime;
	}
	public String getUpdateTime() {
		return updateTime;
	}
	public void setUpdateTime(String updateTime) {
		this.updateTime = updateTime;
	}
	public String getIsDeleted() {
		return isDeleted;
	}
	public void setIsDeleted(String isDeleted) {
		this.isDeleted = isDeleted;
	}
	public String getStoreId() {
		return storeId;
	}
	public void setStoreId(String storeId) {
		this.storeId = storeId;
	}
	public String getTotalcommission() {
		return totalcommission;
	}
	public void setTotalcommission(String totalcommission) {
		this.totalcommission = totalcommission;
	}
	public String getPayedcommission() {
		return payedcommission;
	}
	public void setPayedcommission(String payedcommission) {
		this.payedcommission = payedcommission;
	}
	public String getNotpayedcommission() {
		return notpayedcommission;
	}
	public void setNotpayedcommission(String notpayedcommission) {
		this.notpayedcommission = notpayedcommission;
	}
	
	
	
}
