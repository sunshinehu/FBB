package com.sunday.fangbeibei.orm;

import java.util.List;

public class BlogInfo {

	private String ct;// 0,
	private String ut;// 0,
	private String 	id;// 0,
	private String 	premisesId;// 6,
	private String 	blogId;// 8,
	private String memberId;// null,
	private String storeId;// null,
	private String type;// null,
	private String createTime;// "1970-01-01 08;//00;//00",
	private BlogContent blog;// {},
	private String author;// null,
	private String storeName;// null,
	private List<BlogImage> images;// [ ],
	private List<BlogReply> replys;// [ ],
	private List<BlogUser> favUser;// [],
	private String isGood;// 1,
	private String premisesName;// "小雅店铺"
	public String getCt() {
		return ct;
	}
	public void setCt(String ct) {
		this.ct = ct;
	}
	public String getUt() {
		return ut;
	}
	public void setUt(String ut) {
		this.ut = ut;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getPremisesId() {
		return premisesId;
	}
	public void setPremisesId(String premisesId) {
		this.premisesId = premisesId;
	}
	public String getBlogId() {
		return blogId;
	}
	public void setBlogId(String blogId) {
		this.blogId = blogId;
	}
	public String getMemberId() {
		return memberId;
	}
	public void setMemberId(String memberId) {
		this.memberId = memberId;
	}
	public String getStoreId() {
		return storeId;
	}
	public void setStoreId(String storeId) {
		this.storeId = storeId;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getCreateTime() {
		return createTime;
	}
	public void setCreateTime(String createTime) {
		this.createTime = createTime;
	}
	
	public BlogContent getBlog() {
		return blog;
	}
	public void setBlog(BlogContent blog) {
		this.blog = blog;
	}
	public String getAuthor() {
		return author;
	}
	public void setAuthor(String author) {
		this.author = author;
	}
	public String getStoreName() {
		return storeName;
	}
	public void setStoreName(String storeName) {
		this.storeName = storeName;
	}
	public List<BlogImage> getImages() {
		return images;
	}
	public void setImages(List<BlogImage> images) {
		this.images = images;
	}
	public List<BlogReply> getReplys() {
		return replys;
	}
	public void setReplys(List<BlogReply> replys) {
		this.replys = replys;
	}
	public List<BlogUser> getFavUser() {
		return favUser;
	}
	public void setFavUser(List<BlogUser> favUser) {
		this.favUser = favUser;
	}
	public String getIsGood() {
		return isGood;
	}
	public void setIsGood(String isGood) {
		this.isGood = isGood;
	}
	public String getPremisesName() {
		return premisesName;
	}
	public void setPremisesName(String premisesName) {
		this.premisesName = premisesName;
	}
	
	
}
