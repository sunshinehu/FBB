package com.sunday.fangbeibei.orm;

import java.util.List;


public class HouseInfoResult extends BaseResult{

	private List<HouseInfo> list;

	public List<HouseInfo> getList() {
		return list;
	}

	public void setList(List<HouseInfo> list) {
		this.list = list;
	}
	
}
