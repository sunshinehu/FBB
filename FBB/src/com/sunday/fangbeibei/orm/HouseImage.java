package com.sunday.fangbeibei.orm;

import java.io.Serializable;
import java.util.List;

public class HouseImage implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -7467687908911279290L;
	private String type;// "1",
	private String name;// "效果图",
	private List<ImageInfo> atts;// 
	
	
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public List<ImageInfo> getAtts() {
		return atts;
	}
	public void setAtts(List<ImageInfo> atts) {
		this.atts = atts;
	}
	
	
}
