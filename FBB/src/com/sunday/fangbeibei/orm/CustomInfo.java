package com.sunday.fangbeibei.orm;

import java.io.Serializable;


public class CustomInfo implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -292232089842981267L;
	private String mobile;//'被报备人的电话号码',
	private String name;// '被报备人的姓名',
	private Integer status;//报备状态
	private String premisesName;//'报备的楼盘的名称',
	private Integer maxPrice;//承受最大价格
	private String area;//需求面积
	private String layout;//户型类型名称
	private String id;
	
	private Integer sex;
	
	private boolean selected;
	
	public String getMobile() {
		return mobile;
	}
	public void setMobile(String mobile) {
		this.mobile = mobile;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public Integer getStatus() {
		return status;
	}
	public void setStatus(Integer status) {
		this.status = status;
	}
	public String getPremisesName() {
		return premisesName;
	}
	public void setPremisesName(String premisesName) {
		this.premisesName = premisesName;
	}
	public Integer getMaxPrice() {
		return maxPrice;
	}
	public void setMaxPrice(Integer maxPrice) {
		this.maxPrice = maxPrice;
	}
	public String getArea() {
		return area;
	}
	public void setArea(String area) {
		this.area = area;
	}
	public String getLayout() {
		return layout;
	}
	public void setLayout(String layout) {
		this.layout = layout;
	}
	public boolean isSelected() {
		return selected;
	}
	public void setSelected(boolean selected) {
		this.selected = selected;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public Integer getSex() {
		return sex;
	}
	public void setSex(Integer sex) {
		this.sex = sex;
	}
	
}
