package com.sunday.fangbeibei.orm;

import java.util.List;

/**
 * 

* @ClassName: StoreFilter 

* @Description: TODO(佣金查询参数) 

* @date 2015年2月26日 下午1:55:07 

*
 */
public class StoreFilter {

	private List<HouseInfo> premisesInfos;
	private List<UserInfo> users;
	
	public List<HouseInfo> getPremisesInfos() {
		return premisesInfos;
	}
	public void setPremisesInfos(List<HouseInfo> premisesInfos) {
		this.premisesInfos = premisesInfos;
	}
	public List<UserInfo> getUsers() {
		return users;
	}
	public void setUsers(List<UserInfo> users) {
		this.users = users;
	}
	
}
