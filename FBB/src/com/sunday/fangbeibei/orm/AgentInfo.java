package com.sunday.fangbeibei.orm;

import java.io.Serializable;

/**
 * 

* @ClassName;// AgentInfo 

* @Description;// TODO(这里用一句话描述这个类的作用) 

* @date 2015年2月11日 下午4;//35;//28 

*
 */
public class AgentInfo implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -8830178408938241084L;
	private Long ct;// 0,
	private Long ut;// 1423580474766,
	private String id;// 9,
	private String isDeleted;// 0,
	private String name;// "liu",
	private String nickname;// null,
	private String mobi;// "15990194062",
	private String storeId;// 4,
	private String password;// "96e79218965eb72c92a549dd5a330112",
	private String type;// 2,
	private String districtId;// null,
	private String cityName;// null,
	private String path;// null,
	private String sex;// 1,
	private Integer status;// 2,待审核，已通过，被冻结  123
	private String storeName;// null

	private String reportcount;//: 31,
	private String dealcount;//: 1,
	
	
	public Long getCt() {
		return ct;
	}
	public void setCt(Long ct) {
		this.ct = ct;
	}
	public Long getUt() {
		return ut;
	}
	public void setUt(Long ut) {
		this.ut = ut;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getIsDeleted() {
		return isDeleted;
	}
	public void setIsDeleted(String isDeleted) {
		this.isDeleted = isDeleted;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getNickname() {
		return nickname;
	}
	public void setNickname(String nickname) {
		this.nickname = nickname;
	}
	public String getMobi() {
		return mobi;
	}
	public void setMobi(String mobi) {
		this.mobi = mobi;
	}
	public String getStoreId() {
		return storeId;
	}
	public void setStoreId(String storeId) {
		this.storeId = storeId;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getDistrictId() {
		return districtId;
	}
	public void setDistrictId(String districtId) {
		this.districtId = districtId;
	}
	public String getCityName() {
		return cityName;
	}
	public void setCityName(String cityName) {
		this.cityName = cityName;
	}
	public String getPath() {
		return path;
	}
	public void setPath(String path) {
		this.path = path;
	}
	public String getSex() {
		return sex;
	}
	public void setSex(String sex) {
		this.sex = sex;
	}
	public String getStoreName() {
		return storeName;
	}
	public void setStoreName(String storeName) {
		this.storeName = storeName;
	}
	public Integer getStatus() {
		return status;
	}
	public void setStatus(Integer status) {
		this.status = status;
	}
	public String getReportcount() {
		return reportcount;
	}
	public void setReportcount(String reportcount) {
		this.reportcount = reportcount;
	}
	public String getDealcount() {
		return dealcount;
	}
	public void setDealcount(String dealcount) {
		this.dealcount = dealcount;
	}
	
	
	
}
