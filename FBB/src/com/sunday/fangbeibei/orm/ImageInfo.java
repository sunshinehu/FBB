package com.sunday.fangbeibei.orm;

import java.io.Serializable;

/**
 * 

* @ClassName;// ImageInfo 

* @Description;// TODO(图片信息) 

* @author sunshineHu hucx@itouch.com.cn

* @date 2015年2月6日 下午1;//18;//39 

*
 */
public class ImageInfo implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -1467184185991543434L;
	private String ct;// 0,
	private String ut;// 0,
	private String id;// 6,
	private String objId;// "6",
	private String type;// "1",
	private String path;// "http;////fbbimage.53xsd.com/2015/1/323d7429-8b02-46ae-910f-42c60d73eea3.jpg",
	private String name;// "效果图",
	private String fileName;// "1.jpg",
	private String contentType;// "image/jpeg",
	private String size;// 19159,
	private String sign;// "6f444b6591b73b3dd99e64bd020db28a",
	private String relName;// "效果图"
	
	
	public String getCt() {
		return ct;
	}
	public void setCt(String ct) {
		this.ct = ct;
	}
	public String getUt() {
		return ut;
	}
	public void setUt(String ut) {
		this.ut = ut;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getObjId() {
		return objId;
	}
	public void setObjId(String objId) {
		this.objId = objId;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getPath() {
		return path;
	}
	public void setPath(String path) {
		this.path = path;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getFileName() {
		return fileName;
	}
	public void setFileName(String fileName) {
		this.fileName = fileName;
	}
	public String getContentType() {
		return contentType;
	}
	public void setContentType(String contentType) {
		this.contentType = contentType;
	}
	public String getSize() {
		return size;
	}
	public void setSize(String size) {
		this.size = size;
	}
	public String getSign() {
		return sign;
	}
	public void setSign(String sign) {
		this.sign = sign;
	}
	public String getRelName() {
		return relName;
	}
	public void setRelName(String relName) {
		this.relName = relName;
	}
	
}
