package com.sunday.fangbeibei.orm;
/***
 * 

* @ClassName: UpdateInfo 

* @Description: TODO(更新信息) 

* @author sunshineHu hucx@itouch.com.cn

* @date 2015年1月16日 下午5:18:22 

*
 */
public class UpdateInfo {
	
	private String id;// 305,
	private String type;// null,
	private String fileName;// null,
	private String version;// "1.8.6",
	private String flag;// null,
	private String fileSize;// null,
	private String updateTime;// "2015-01-15",
	private String filePath;// "http;////www.machineinfo-zj.com/testapp/xsd/android/newStroe.apk",
	private String updator;// null,
	private String appId;// 91
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getFileName() {
		return fileName;
	}
	public void setFileName(String fileName) {
		this.fileName = fileName;
	}
	public String getVersion() {
		return version;
	}
	public void setVersion(String version) {
		this.version = version;
	}
	public String getFlag() {
		return flag;
	}
	public void setFlag(String flag) {
		this.flag = flag;
	}
	public String getFileSize() {
		return fileSize;
	}
	public void setFileSize(String fileSize) {
		this.fileSize = fileSize;
	}
	public String getUpdateTime() {
		return updateTime;
	}
	public void setUpdateTime(String updateTime) {
		this.updateTime = updateTime;
	}
	public String getFilePath() {
		return filePath;
	}
	public void setFilePath(String filePath) {
		this.filePath = filePath;
	}
	public String getUpdator() {
		return updator;
	}
	public void setUpdator(String updator) {
		this.updator = updator;
	}
	public String getAppId() {
		return appId;
	}
	public void setAppId(String appId) {
		this.appId = appId;
	}
	
	
	
}
