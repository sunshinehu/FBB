package com.sunday.fangbeibei.orm;
/**
 * 
 * @author 晨曦
 *
 */
public class DynamicMessage {

	private BlogReply blogReply;
	private BlogContent blog;
	
	
	public BlogReply getBlogReply() {
		return blogReply;
	}
	public void setBlogReply(BlogReply blogReply) {
		this.blogReply = blogReply;
	}
	public BlogContent getBlog() {
		return blog;
	}
	public void setBlog(BlogContent blog) {
		this.blog = blog;
	}
	
}
