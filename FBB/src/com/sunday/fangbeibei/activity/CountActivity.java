package com.sunday.fangbeibei.activity;

import java.util.List;

import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.gson.reflect.TypeToken;
import com.loopj.android.http.RequestParams;
import com.sunday.fangbeibei.R;
import com.sunday.fangbeibei.adapter.AgentAdapter;
import com.sunday.fangbeibei.cache.ApplicationCache;
import com.sunday.fangbeibei.constants.ApplicationConstants;
import com.sunday.fangbeibei.http.HttpResponseInterface;
import com.sunday.fangbeibei.http.HttpTool;
import com.sunday.fangbeibei.orm.AgentInfo;
import com.sunday.fangbeibei.orm.BaseResult;
import com.sunday.fangbeibei.orm.CountInfo;

/**
 * 统计
 * @author SunshineHu
 *
 */
public class CountActivity extends BaseActivity implements OnClickListener,HttpResponseInterface{

	private TextView total;
	private TextView yesterday;
	private TextView today;
	
	@Override
	protected void createActivity(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		setContentView(R.layout.count_layout);
		loadView();
		loadData();
	}

	
	private void loadView(){
		

		
		TextView title=(TextView) findViewById(R.id.title);
		ImageView back=(ImageView) findViewById(R.id.back);
		
		yesterday=(TextView) findViewById(R.id.yesterday);
		today=(TextView) findViewById(R.id.today);
		total=(TextView) findViewById(R.id.total);
		
		title.setText("报备统计");
		
		back.setOnClickListener(this);
		
	}


	private void loadData(){
		
		RequestParams params=new RequestParams();
		params.add("userId", ApplicationCache.INSTANCE.getUserInfo().getId());
		HttpTool.HttpPost(0, mContext, ApplicationConstants.SERVER_URL+"/mobi/report/statistics", params, this, new TypeToken<BaseResult<CountInfo>>() {
		}.getType());
		
	}
	
	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		switch(v.getId()){
		case R.id.back:
			finish();
			break;
		}
			
	}


	@Override
	public void onResponse(int requsetCode, boolean resultStatus, Object result) {
		// TODO Auto-generated method stub
		switch(requsetCode){
		case 0:
			if(resultStatus){
				BaseResult<CountInfo> re=(BaseResult<CountInfo>) result;
				total.setText(re.getResult().getTotal());
				yesterday.setText(re.getResult().getYestodayCount());
				today.setText(re.getResult().getTodayCount());
			}
			break;
		}
	}


	@Override
	public void onFailure() {
		// TODO Auto-generated method stub
		
	}

	
}
