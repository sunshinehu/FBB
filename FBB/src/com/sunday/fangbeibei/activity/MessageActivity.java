package com.sunday.fangbeibei.activity;

import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.google.gson.reflect.TypeToken;
import com.handmark.pulltorefresh.library.PullToRefreshBase;
import com.handmark.pulltorefresh.library.PullToRefreshBase.Mode;
import com.handmark.pulltorefresh.library.PullToRefreshBase.OnRefreshListener2;
import com.handmark.pulltorefresh.library.PullToRefreshListView;
import com.loopj.android.http.RequestParams;
import com.sunday.fangbeibei.R;
import com.sunday.fangbeibei.adapter.MessageAdapter;
import com.sunday.fangbeibei.constants.ApplicationConstants;
import com.sunday.fangbeibei.http.HttpResponseInterface;
import com.sunday.fangbeibei.http.HttpTool;
import com.sunday.fangbeibei.orm.MessageResult;

/**
 * 

* @ClassName: MessageActivity 

* @Description: TODO(我的消息) 

* @date 2015年2月27日 下午4:18:48 

*
 */
public class MessageActivity extends BaseActivity implements OnClickListener, OnRefreshListener2<ListView>,HttpResponseInterface{
	
	private PullToRefreshListView mPullToRefreshListView;
	private ListView listview;

	private int pageNo=1;
	
	@Override
	protected void createActivity(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		setContentView(R.layout.message_layout);
		loadView();
		loadData();
	}
	
	


	private void loadView(){
		
		TextView title=(TextView) findViewById(R.id.title);
		title.setText("我的消息");
		ImageView back=(ImageView) findViewById(R.id.back);
		back.setOnClickListener(this);
		

		mPullToRefreshListView=(PullToRefreshListView) findViewById(R.id.listview);
		listview=mPullToRefreshListView.getRefreshableView();
		
		mPullToRefreshListView.setMode(Mode.BOTH);
		mPullToRefreshListView.getLoadingLayoutProxy(false, true).setPullLabel("上拉加载");
		mPullToRefreshListView.getLoadingLayoutProxy(false, true).setRefreshingLabel("正在加载");
		mPullToRefreshListView.getLoadingLayoutProxy(false, true).setReleaseLabel("松开加载更多");
		
		mPullToRefreshListView.setOnRefreshListener(this);
		
		
	}

	
	private void loadData(){
		
		RequestParams params=new RequestParams();
		HttpTool.HttpPost(0, this, ApplicationConstants.SERVER_URL+"/mobi/myInfo/list", params, this, new TypeToken<MessageResult>() {
		}.getType());
		
	}
	


	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		if(v.getId()==R.id.back){
			finish();
		}
	}




	@Override
	public void onResponse(int requsetCode, boolean resultStatus, Object result) {
		// TODO Auto-generated method stub
		if(requsetCode==0&&resultStatus){
			MessageResult mResult=(MessageResult) result;
			MessageAdapter adapter=new MessageAdapter(mContext, mResult.getList());
			listview.setAdapter(adapter);
		}
	}




	@Override
	public void onFailure() {
		// TODO Auto-generated method stub
		
	}




	@Override
	public void onPullDownToRefresh(PullToRefreshBase<ListView> refreshView) {
		// TODO Auto-generated method stub
		pageNo=1;
		loadData();
	}




	@Override
	public void onPullUpToRefresh(PullToRefreshBase<ListView> refreshView) {
		// TODO Auto-generated method stub
		pageNo++;
		loadData();
	}

}
