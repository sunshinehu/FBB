package com.sunday.fangbeibei.activity;

import android.content.Intent;
import android.os.Bundle;
import android.view.animation.Animation;
import android.view.animation.Animation.AnimationListener;
import android.view.animation.AnimationUtils;
import android.widget.LinearLayout;

import com.igexin.sdk.PushManager;
import com.sunday.fangbeibei.R;
import com.sunday.fangbeibei.cache.ApplicationCache;
import com.testin.agent.TestinAgent;

/**
 * 
 * @author 晨曦
 *
 */
public class WelcomeActivity extends BaseActivity{

	@Override
	protected void createActivity(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		setContentView(R.layout.welcome_layout);
		
		//个推初始化
		PushManager.getInstance().initialize(this.getApplicationContext());
		
		//Testin sdk
		TestinAgent.init(this, "496f7fdd59e456ce67b4f4556a095c82", "");
		
		LinearLayout background=(LinearLayout) findViewById(R.id.back_ll);
		
		Animation anim=AnimationUtils.loadAnimation(this, R.anim.alpha);
		anim.setAnimationListener(new AnimationListener() {
			
			@Override
			public void onAnimationStart(Animation animation) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void onAnimationRepeat(Animation animation) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void onAnimationEnd(Animation animation) {
				// TODO Auto-generated method stub
				if(ApplicationCache.INSTANCE.getUserInfo()==null){
					Intent intent = new Intent();
					intent.setClass(WelcomeActivity.this, LoginActivity.class);
					startActivity(intent);
				}else{
					Intent in=new Intent(mContext, HomeActivity.class);
					startActivity(in);
				}
				//设置切换动画，从右边进入，左边退出
				overridePendingTransition(R.anim.in_from_right, R.anim.out_to_left);	
			}
		});
		background.startAnimation(anim);
	}
	
}



