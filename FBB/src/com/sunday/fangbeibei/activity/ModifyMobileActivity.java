package com.sunday.fangbeibei.activity;

import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.reflect.TypeToken;
import com.loopj.android.http.RequestParams;
import com.sunday.fangbeibei.R;
import com.sunday.fangbeibei.cache.ApplicationCache;
import com.sunday.fangbeibei.constants.ApplicationConstants;
import com.sunday.fangbeibei.http.HttpResponseInterface;
import com.sunday.fangbeibei.http.HttpTool;
import com.sunday.fangbeibei.orm.BaseResult;

/**
 * 修改手机号
 * @author SunshineHu
 *
 */
public class ModifyMobileActivity extends BaseActivity implements OnClickListener,HttpResponseInterface{

	
	private EditText oldmobile;
	private EditText verifycode;

	private TextView verify;
	private TextView commit1;
	
	private long lastVerify;
	
	private boolean flag=false;
	
	@Override
	protected void createActivity(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		
		setContentView(R.layout.modify_mobile_layout);
		loadView();
		
	}

	private void loadView(){
		
		oldmobile=(EditText) findViewById(R.id.oldmobile);
		verifycode=(EditText) findViewById(R.id.verifycode);
		
		verify=(TextView) findViewById(R.id.verify);
		commit1=(TextView) findViewById(R.id.commit1);
		commit1.setOnClickListener(this);
		verify.setOnClickListener(this);
		
		TextView title=(TextView) findViewById(R.id.title);
		ImageView back=(ImageView) findViewById(R.id.back);
		
		title.setText("修改手机号");
		back.setOnClickListener(this);
		
	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		switch(v.getId()){
		case R.id.back:
			finish();
			break;
		case R.id.verify:
			if((System.currentTimeMillis()-lastVerify)<=60*1000){
				return;
			}
			show("");
			RequestParams param=new RequestParams();
			param.add("userId", ApplicationCache.INSTANCE.getUserInfo().getId());
			param.add("mobile", oldmobile.getText().toString());
			HttpTool.HttpPost(0, mContext, ApplicationConstants.SERVER_URL+"/mobi/account/validateMobile", param, this, new TypeToken<BaseResult>() {
			}.getType());
			break;
		case R.id.commit1:
			RequestParams param1=new RequestParams();
			param1.add("mobile", oldmobile.getText().toString());
			param1.add("activeCode", verifycode.getText().toString());
			HttpTool.HttpPost(1, mContext, ApplicationConstants.SERVER_URL+"/mobi/account/valActiveCode", param1, this, new TypeToken<BaseResult>() {
			}.getType());
			/*}else{
				RequestParams param2=new RequestParams();
				param2.add("userId", ApplicationCache.INSTANCE.getUserInfo().getId());
				param2.add("mobile", oldmobile.getText().toString());
				param2.add("password", verifycode.getText().toString());
				HttpTool.HttpPost(2, mContext, ApplicationConstants.SERVER_URL+"/mobi/account/updateMobile", param2, this, new TypeToken<BaseResult>() {
				}.getType());
			}*/
			break;
		}
	}

	@Override
	public void onResponse(int requsetCode, boolean resultStatus, Object result) {
		// TODO Auto-generated method stub
		disMiss();
		switch(requsetCode){
		case 0:
			if(resultStatus){
				
				show("");
				RequestParams param=new RequestParams();
				param.add("mobile", oldmobile.getText().toString());
				HttpTool.HttpPost(3, mContext, ApplicationConstants.SERVER_URL+"/mobi/account/sendActiveCode", param, this, new TypeToken<BaseResult>() {
				}.getType());
				
			}else{
				BaseResult re=(BaseResult) result;
				switch(re.getCode()){
				case -1:
					Toast.makeText(mContext, "系统异常！", Toast.LENGTH_SHORT).show();
					break;
				case 1:
					Toast.makeText(mContext, "号码验证失败！", Toast.LENGTH_SHORT).show();
					break;
				}
			}
			break;
		case 1:
			if(resultStatus){
				
				if(!flag){
					
					oldmobile.setHint("输入新手机号");
					oldmobile.setText("");
					verifycode.setText("");
					flag=true;
					verify.setText("获取验证码");
					commit1.setText("提交");
					lastVerify=0;
					
				}else{
					
					RequestParams param2=new RequestParams();
					param2.add("userId", ApplicationCache.INSTANCE.getUserInfo().getId());
					param2.add("mobile", oldmobile.getText().toString());
					param2.add("password", verifycode.getText().toString());
					HttpTool.HttpPost(2, mContext, ApplicationConstants.SERVER_URL+"/mobi/account/updateMobile", param2, this, new TypeToken<BaseResult>() {
					}.getType());
					
				}
				
				
			}else{
				BaseResult re=(BaseResult) result;
				switch(re.getCode()){
				case -1:
					Toast.makeText(mContext, "系统异常！", Toast.LENGTH_SHORT).show();
					break;
				case -2:
					Toast.makeText(mContext, "验证码错误！", Toast.LENGTH_SHORT).show();
					break;
				}
			}
			break;
		case 2:
			if(resultStatus){
				Toast.makeText(mContext, "修改成功!", Toast.LENGTH_SHORT).show();
				ApplicationCache.INSTANCE.getUserInfo().setMobi(oldmobile.getText().toString());
				finish();
			}else{
				Toast.makeText(mContext, "修改失败!", Toast.LENGTH_SHORT).show();
			}
			break;
		case 3:
			if(resultStatus){

				lastVerify=System.currentTimeMillis();
				Toast.makeText(mContext, "验证码已发送至您的手机，请注意查收!", Toast.LENGTH_SHORT).show();
				VerifyCheck check=new VerifyCheck();
				check.start();
			
			}else{
				BaseResult re=(BaseResult) result;
				switch(re.getCode()){
				case -1:
					Toast.makeText(mContext, "系统异常！", Toast.LENGTH_SHORT).show();
					break;
				case -2:
					Toast.makeText(mContext, "该号码已被注册！", Toast.LENGTH_SHORT).show();
					break;
				}
			}
			break;
		}
	}

	@Override
	public void onFailure() {
		// TODO Auto-generated method stub
		disMiss();
	}
	
	


	class VerifyCheck extends Thread{

		@Override
		public void run() {
			// TODO Auto-generated method stub
			super.run();
			while(System.currentTimeMillis()-lastVerify<=60*1000){
				runOnUiThread(updateVerify);
				try {
					sleep(1000);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			
			runOnUiThread(new Runnable() {
				
				@Override
				public void run() {
					// TODO Auto-generated method stub
					if(lastVerify==0){
						verify.setText("获取验证码");
					}else{
						verify.setText("重新获取验证码");
					}
				}
			});
		}
		
	};
	

	Runnable updateVerify=new Runnable() {
		
		@Override
		public void run() {
			// TODO Auto-generated method stub
			if(verify!=null){
				verify.setText(60-(System.currentTimeMillis()-lastVerify)/1000+"秒后重新获取");
			}
		}
	};
	
	
}
