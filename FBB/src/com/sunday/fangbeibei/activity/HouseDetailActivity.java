package com.sunday.fangbeibei.activity;

import java.util.ArrayList;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup.LayoutParams;
import android.view.ViewTreeObserver.OnPreDrawListener;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import cn.sharesdk.framework.ShareSDK;
import cn.sharesdk.onekeyshare.OnekeyShare;

import com.google.gson.reflect.TypeToken;
import com.loopj.android.http.RequestParams;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.sunday.fangbeibei.R;
import com.sunday.fangbeibei.cache.ApplicationCache;
import com.sunday.fangbeibei.constants.ApplicationConstants;
import com.sunday.fangbeibei.http.HttpResponseInterface;
import com.sunday.fangbeibei.http.HttpTool;
import com.sunday.fangbeibei.orm.BaseResult;
import com.sunday.fangbeibei.orm.BlogContent;
import com.sunday.fangbeibei.orm.HouseImage;
import com.sunday.fangbeibei.orm.HouseInfo;
import com.sunday.fangbeibei.orm.HouseLayout;
import com.sunday.fangbeibei.orm.HouseLayoutResult;
import com.sunday.fangbeibei.orm.LayoutImageInfo;

/**
 * 楼盘详情
 * @author 晨曦
 *
 */
public class HouseDetailActivity extends BaseActivity implements OnClickListener,HttpResponseInterface{

	private HouseInfo info;
	private ArrayList<HouseImage> lists;
	
	private ImageView favourate;
	
	
	@Override
	protected void createActivity(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		setContentView(R.layout.house_detail_layout);
		info=(HouseInfo) getIntent().getSerializableExtra("info");
		loadView();
		getDetail();
		getImages();
		getLayouts();
	}

	private void loadView(){
		
		LinearLayout left=(LinearLayout) findViewById(R.id.left);
		LinearLayout right=(LinearLayout) findViewById(R.id.right);
		
		left.setOnClickListener(this);
		right.setOnClickListener(this);
		
		ImageView background=(ImageView) findViewById(R.id.background);
		
		WindowManager wm = (WindowManager) getSystemService(Context.WINDOW_SERVICE);
		background.setLayoutParams(new RelativeLayout.LayoutParams(android.widget.RelativeLayout.LayoutParams.MATCH_PARENT, wm.getDefaultDisplay().getWidth()*155/320));
		
		ImageLoader.getInstance().displayImage(info.getImage(), background);
		
		
		TextView parameter=(TextView) findViewById(R.id.parameter);		
		TextView address=(TextView) findViewById(R.id.address);		
		
		address.setText(info.getPostion());
		address.setOnClickListener(this);
		
		ImageView back=(ImageView) findViewById(R.id.back);
		back.setOnClickListener(this);
		parameter.setOnClickListener(this);
		
		LinearLayout ll1=(LinearLayout) findViewById(R.id.ll1);
		RelativeLayout rl1=(RelativeLayout) findViewById(R.id.rl1);
		RelativeLayout rl2=(RelativeLayout) findViewById(R.id.rl2);
		
		favourate=(ImageView) findViewById(R.id.favourate);
		ImageView share=(ImageView) findViewById(R.id.share);
		
		if(info.getIsfav()==1){
			favourate.setImageDrawable(mContext.getResources().getDrawable(R.drawable.favourate_icon_s));
		}else{
			favourate.setImageDrawable(mContext.getResources().getDrawable(R.drawable.favourate_icon));
		}
		
		TextView tv1=(TextView) findViewById(R.id.tv1);
		TextView tv2=(TextView) findViewById(R.id.tv2);
		TextView tv3=(TextView) findViewById(R.id.tv3);
		final TextView tv4=(TextView) findViewById(R.id.tv4);
		final TextView tv5=(TextView) findViewById(R.id.tv5);
		final TextView tv6=(TextView) findViewById(R.id.tv6);
		final TextView tv7=(TextView) findViewById(R.id.tv7);
		TextView tv8=(TextView) findViewById(R.id.tv8);
		TextView tv9=(TextView) findViewById(R.id.tv9);
		
		TextView name=(TextView) findViewById(R.id.name);
		TextView price=(TextView) findViewById(R.id.price);
		
		tv3.setText(info.getMycustomeramount());
		
		final TextView more=(TextView) findViewById(R.id.more);
		more.setTag(false);
		more.getViewTreeObserver().addOnPreDrawListener(
				new OnPreDrawListener() {

					@Override
					public boolean onPreDraw() {
						// TODO Auto-generated method stub
						if (tv7.getLineCount() >= 3) {
							more.setVisibility(View.VISIBLE);
						}else{
							more.setVisibility(View.GONE);
						}
						return true;
					}
				});
		more.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				boolean flag=(Boolean) arg0.getTag();
				if(flag){
					tv7.setLines(3);
					tv7.setMaxLines(3);
					more.setText("展开");
				}else{
					tv7.setLines(tv7.getLineCount());
					tv7.setMaxLines(10);
					more.setText("收缩");
				}
				more.setTag(!flag);
			}
		});
		


		
		final TextView more1=(TextView) findViewById(R.id.more1);
		more1.setTag(false);
		more1.getViewTreeObserver().addOnPreDrawListener(
				new OnPreDrawListener() {

					@Override
					public boolean onPreDraw() {
						// TODO Auto-generated method stub
						if (tv4.getLineCount() >= 3) {
							more1.setVisibility(View.VISIBLE);
						}else{
							more1.setVisibility(View.GONE);
						}
						return true;
					}
				});
		more1.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				boolean flag=(Boolean) arg0.getTag();
				if(flag){
					tv4.setLines(3);
					tv4.setMaxLines(3);
					more1.setText("展开");
				}else{
					tv4.setLines(tv4.getLineCount());
					tv4.setMaxLines(10);
					more1.setText("收缩");
				}
				more1.setTag(!flag);
			}
		});
		
		
		
		final TextView more2=(TextView) findViewById(R.id.more2);
		more2.setTag(false);
		more2.getViewTreeObserver().addOnPreDrawListener(
				new OnPreDrawListener() {

					@Override
					public boolean onPreDraw() {
						// TODO Auto-generated method stub
						if (tv5.getLineCount() >= 3) {
							more2.setVisibility(View.VISIBLE);
						}else{
							more2.setVisibility(View.GONE);
						}
						return true;
					}
				});
		more2.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				boolean flag=(Boolean) arg0.getTag();
				if(flag){
					tv5.setLines(3);
					tv5.setMaxLines(3);
					more2.setText("展开");
				}else{
					tv5.setLines(tv5.getLineCount());
					tv5.setMaxLines(10);
					more2.setText("收缩");
				}
				more2.setTag(!flag);
			}
		});
		
		
		
		final TextView more3=(TextView) findViewById(R.id.more3);
		more3.setTag(false);
		more3.getViewTreeObserver().addOnPreDrawListener(
				new OnPreDrawListener() {

					@Override
					public boolean onPreDraw() {
						// TODO Auto-generated method stub
						if (tv6.getLineCount() >= 3) {
							more3.setVisibility(View.VISIBLE);
						}else{
							more3.setVisibility(View.GONE);
						}
						return true;
					}
				});
		more3.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				boolean flag=(Boolean) arg0.getTag();
				if(flag){
					tv6.setLines(3);
					tv6.setMaxLines(3);
					more3.setText("展开");
				}else{
					tv6.setLines(tv6.getLineCount());
					tv6.setMaxLines(10);
					more3.setText("收缩");
				}
				more3.setTag(!flag);
			}
		});
		
				

		tv6.setMaxLines(3);
		tv5.setMaxLines(3);
		tv4.setMaxLines(3);
		tv7.setMaxLines(3);
		
		
		name.setText("   "+info.getName());
		price.setText("￥   "+info.getPrice());
		
		tv1.setText(info.getManageramount());
		tv2.setText(info.getCustomeramount());
		
		if(info.getActivityremark()!=null&&!info.getActivityremark().equals("")){
			tv4.setText(info.getActivity()+"\n"+info.getActivityremark());
		}else{
			tv4.setText(info.getActivity());
		}
		if(info.getBenefitremark()!=null&&!info.getBenefitremark().equals("")){
			tv5.setText(info.getBenefit()+"\n"+info.getBenefitremark());
		}else{
			tv5.setText(info.getBenefit());
		}
		if(info.getRewardremark()!=null&&!info.getRewardremark().equals("")){
			tv6.setText(info.getReward()+"\n"+info.getRewardremark());
		}else{
			tv6.setText(info.getReward());
		}
		if(info.getCommissionremark()!=null&&!info.getCommissionremark().equals("")){
			tv7.setText(info.getCommission()+"\n"+info.getCommissionremark());
		}else{
			tv7.setText(info.getCommission());
		}
		
		tv8.setOnClickListener(this);
		tv9.setOnClickListener(this);
		
		favourate.setOnClickListener(this);
		share.setOnClickListener(this);
		
		RelativeLayout activerl=(RelativeLayout) findViewById(R.id.activerl);
		RelativeLayout benifitrl=(RelativeLayout) findViewById(R.id.benifitrl);

		if(info.getActivity()==null||info.getActivity().equals("")){
			activerl.setVisibility(View.GONE);
		}
		if(info.getBenefit()==null||info.getBenefit().equals("")){
			benifitrl.setVisibility(View.GONE);
		}
		if(info.getReward()==null||info.getReward().equals("")){
			rl1.setVisibility(View.GONE);
		}
		if(info.getCommission()==null||info.getCommission().equals("")){
			rl2.setVisibility(View.GONE);
		}
		if(ApplicationCache.INSTANCE.getUserInfo().getType()==3){
			ll1.setVisibility(View.GONE);
			rl1.setVisibility(View.GONE);
			rl2.setVisibility(View.GONE);
			tv9.setText("  我要看房");
		}else if(ApplicationCache.INSTANCE.getUserInfo().getType()==2){
			rl2.setVisibility(View.GONE);
		}
		
		
		
		if(info.getBlogs()!=null&&info.getBlogs().size()>0){
			
			LinearLayout newslayout=(LinearLayout) findViewById(R.id.newslayout);
			newslayout.removeAllViews();
			LayoutInflater inflater=LayoutInflater.from(mContext);
			
			for(BlogContent content:info.getBlogs()){
				
				View newsItem=inflater.inflate(R.layout.house_news_item, null);
				TextView newsContent=(TextView) newsItem.findViewById(R.id.newscontent);
				newsContent.setText(content.getSimpleContent());
				
				newsItem.setId(12321);
				newsItem.setTag(content.getId());
				newsItem.setOnClickListener(this);
				
				newslayout.addView(newsItem);
				
			}
			
		}
		
		
	}

	
	
	private void getDetail(){
		RequestParams params=new RequestParams();
		params.add("id", info.getId());
		params.add("userId", ApplicationCache.INSTANCE.getUserInfo().getId());
		HttpTool.HttpPost(4, this, ApplicationConstants.SERVER_URL+"/mobi/premises/getdetails", params, this, new TypeToken<BaseResult<HouseInfo>>() {
		}.getType());
	}
	
	
	
	
	private void getImages(){
		RequestParams params=new RequestParams();
		params.add("id", info.getId());
		HttpTool.HttpPost(0, this, ApplicationConstants.SERVER_URL+"/mobi/premises/getimage", params, this, new TypeToken<BaseResult<ArrayList<HouseImage>>>() {
		}.getType());
	}
	
	
	private void getLayouts(){
		RequestParams params=new RequestParams();
		params.add("premisesInfoId", info.getId());
		HttpTool.HttpPost(1, this, ApplicationConstants.SERVER_URL+"/mobi/premises/getlayouts", params, this, new TypeToken<HouseLayoutResult>() {
		}.getType());
	}
	

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		switch(v.getId()){
		case R.id.back:
			finish();
			break;
		case R.id.parameter:
			Intent in=new Intent(mContext,HouseParameterActivity.class);
			in.putExtra("info", info);
			startActivity(in);
			break;
		case 12363:
			Intent ins=new Intent(mContext, ImageBrowseActivity.class);
			int pos=(Integer) v.getTag();
			ins.putExtra("position", pos);
			ins.putExtra("images", lists);
			startActivity(ins);
			break;
		case 12321:
			//点击到帖子详情
			String blogId=(String) v.getTag();
			Intent in2=new Intent(mContext, DynamicDetailActivity.class);
			in2.putExtra("id", blogId);
			startActivity(in2);
			break;
		case R.id.tv8:
			Intent intents = new Intent(Intent.ACTION_DIAL,Uri.parse("tel:"+info.getHeaderMobi()));
			mContext.startActivity(intents);
			break;
		case R.id.tv9:
			if(ApplicationCache.INSTANCE.getUserInfo().getType()!=3){
				
				Intent intent=new Intent(mContext, RecordCustomActivity.class);
				intent.putExtra("info", info);
				startActivity(intent);
				
			}else{
				
				Intent intent=new Intent(mContext, OrderActivity.class);
				intent.putExtra("id", info.getId());
				startActivity(intent);
				
			}
			break;
		case R.id.favourate:
			show("");
			RequestParams params=new RequestParams();
			params.add("premisesId", info.getId());
			params.add("userId", ApplicationCache.INSTANCE.getUserInfo().getId());
			HttpTool.HttpPost(2, this, ApplicationConstants.SERVER_URL+"/mobi/premises/addfav", params, this, new TypeToken<BaseResult>() {
			}.getType());
			
			break;
		case R.id.address:
			Intent intent=new Intent(mContext, MapActivity.class);
			String[] loc={info.getLat(),info.getLng()};
			intent.putExtra("location", loc);
			startActivity(intent);
			break;
		case R.id.share:
			showShare();
			break;
		case 12345:
			String id=(String) v.getTag();
			show("");
			RequestParams params2=new RequestParams();
			params2.add("layoutId", id);
			HttpTool.HttpPost(3, this, ApplicationConstants.SERVER_URL+"/mobi/premises/getlayoutimages", params2, this, new TypeToken<BaseResult<LayoutImageInfo>>() {
			}.getType());
			break;
		}
	}

	private void showShare(){
		   ShareSDK.initSDK(mContext);
		   OnekeyShare oks = new OnekeyShare(); 
		   // 分享时Notification的图标和文字
		   oks.setNotification(R.drawable.icon, getString(R.string.app_name));
		   oks.setText("http://fbbadmin.53xsd.com/fangbb/premises/sharepage?id="+info.getId());
		   oks.setDialogMode();
		   // 启动分享GUI
		   oks.show(mContext);
		}
	
	
	
	@Override
	public void onResponse(int requsetCode, boolean resultStatus, Object result) {
		// TODO Auto-generated method stub
		switch(requsetCode){
		case 0:
			BaseResult<ArrayList<HouseImage>> re=(BaseResult<ArrayList<HouseImage>>) result;
			if(resultStatus){
				LinearLayout typell=(LinearLayout) findViewById(R.id.typell);
				lists=re.getResult();
				for(int i=0;i<lists.size();i++){
					View v=LayoutInflater.from(mContext).inflate(R.layout.image_type, null);
					v.setLayoutParams(new LayoutParams(this.getWindowManager().getDefaultDisplay().getWidth()/4, LayoutParams.WRAP_CONTENT));
					TextView name=(TextView) v.findViewById(R.id.imgname);
					name.setText(lists.get(i).getName());
					v.setId(12363);
					v.setTag(i);
					v.setOnClickListener(this);
					typell.addView(v);
				}
				
			}
			break;
		case 1:
			HouseLayoutResult res=(HouseLayoutResult) result;
			
			LinearLayout houselayout=(LinearLayout) findViewById(R.id.houselayout);
			LayoutInflater inflater=LayoutInflater.from(mContext);
			if(resultStatus){
				for(HouseLayout layout:res.getList()){
					View v=inflater.inflate(R.layout.house_layout_item, null);
					TextView name=(TextView) v.findViewById(R.id.name);
					name.setText("  "+layout.getName()+":   "+layout.getContent());
					v.setId(12345);
					v.setTag(layout.getId());
					v.setOnClickListener(this);
					houselayout.addView(v);
				}
			}
			break;
		case 2:
			disMiss();
			if(resultStatus){
				if(info.getIsfav()==0){
					
					favourate.setImageDrawable(mContext.getResources().getDrawable(R.drawable.favourate_icon_s));
					
					Toast.makeText(mContext, "收藏成功", Toast.LENGTH_SHORT).show();
					info.setIsfav(1);
				}else{
					
					favourate.setImageDrawable(mContext.getResources().getDrawable(R.drawable.favourate_icon));
					
					Toast.makeText(mContext, "取消收藏成功", Toast.LENGTH_SHORT).show();
					info.setIsfav(0);
				}
			}else{
				if(info.getIsfav()==0){
					Toast.makeText(mContext, "收藏失败", Toast.LENGTH_SHORT).show();
				}else{
					Toast.makeText(mContext, "取消收藏失败", Toast.LENGTH_SHORT).show();
				}
			}
			break;
		case 3:
			disMiss();
			if(resultStatus){
				BaseResult<LayoutImageInfo> resu=(BaseResult<LayoutImageInfo>) result;
				Intent ins=new Intent(mContext, ImageBrowseActivity.class);
				ins.putExtra("images", resu.getResult());
				ins.putExtra("flag", true);
				startActivity(ins);
			}
			break;
		case 4:
			if(resultStatus){
				BaseResult<HouseInfo> resul=(BaseResult<HouseInfo>) result;
				info.setMycustomeramount(resul.getResult().getMycustomeramount());
				info.setBlogs(resul.getResult().getBlogs());
				loadView();
			}
			break;
		}
	}

	@Override
	public void onFailure() {
		// TODO Auto-generated method stub
		disMiss();
	}
	
}
