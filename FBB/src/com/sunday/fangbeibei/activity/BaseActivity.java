package com.sunday.fangbeibei.activity;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;

import com.sunday.fangbeibei.app.AppManager;
import com.sunday.fangbeibei.http.HttpTool;
import com.sunday.fangbeibei.widget.LoadingDialog;
/**
 * 基类activity
 * @author 晨曦
 *
 */
public abstract class BaseActivity extends Activity{

	protected Context mContext;
	
	private LoadingDialog loadingDialog;
		
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		AppManager.INSTANCE.addActivity(this);//加入activity管理
		mContext=this;
		createActivity(savedInstanceState);
	}

	protected abstract void createActivity(Bundle savedInstanceState);

	@Override
	protected void onDestroy() {
		// TODO Auto-generated method stub
		super.onDestroy();
		HttpTool.cancelRequest(this);
		AppManager.INSTANCE.finishActivity(this);//退出栈
	}
	
	/**
	 * 显示进度加载
	 */
	protected void show(String message) {
		if (loadingDialog == null) {
			loadingDialog = LoadingDialog.createDialog(this);
			// 点击外部区域，是否消失loadingDialog
			loadingDialog.setCanceledOnTouchOutside(false);
		}
		if(message.equals("")){
			message="正在加载";
		}
		loadingDialog.setMessage(message);
		loadingDialog.show();
	}

	/**
	 * 关闭进度加载
	 */
	protected void disMiss() {
		if (loadingDialog != null) {
			loadingDialog.dismiss();
			loadingDialog = null;
		}
	}
}
