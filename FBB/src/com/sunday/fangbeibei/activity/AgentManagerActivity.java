package com.sunday.fangbeibei.activity;

import java.util.List;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.google.gson.reflect.TypeToken;
import com.loopj.android.http.RequestParams;
import com.sunday.fangbeibei.R;
import com.sunday.fangbeibei.adapter.AgentAdapter;
import com.sunday.fangbeibei.cache.ApplicationCache;
import com.sunday.fangbeibei.constants.ApplicationConstants;
import com.sunday.fangbeibei.http.HttpResponseInterface;
import com.sunday.fangbeibei.http.HttpTool;
import com.sunday.fangbeibei.orm.AgentInfo;
import com.sunday.fangbeibei.orm.BaseResult;

/**
 * 

* @ClassName: AgentManagerActivity 

* @Description: TODO(经纪人管理) 

* @date 2015年2月11日 下午4:12:44 

*
 */
public class AgentManagerActivity extends BaseActivity implements OnItemClickListener,OnClickListener,HttpResponseInterface{


	private ListView listview;
	private EditText searchet;
	private ImageView searchiv;

	
	@Override
	protected void createActivity(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		setContentView(R.layout.agent_manager_layout);
		loadView();
	}

	
	@Override
	protected void onStart() {
		// TODO Auto-generated method stub
		super.onStart();
		loadData();
	}


	private void loadView(){
		

		listview=(ListView) findViewById(R.id.listview);
		
		listview.setOnItemClickListener(this);
		
		searchet=(EditText) findViewById(R.id.searchet);
		searchiv=(ImageView) findViewById(R.id.searchiv);
		
		TextView title=(TextView) findViewById(R.id.title);
		TextView right=(TextView) findViewById(R.id.right);
		ImageView back=(ImageView) findViewById(R.id.back);
		
		title.setText("经纪人管理");
		right.setText("统计");
		right.setVisibility(View.VISIBLE);
		
		back.setOnClickListener(this);
		searchiv.setOnClickListener(this);
		right.setOnClickListener(this);
		
	}


	private void loadData(){
		
		RequestParams params=new RequestParams();
		params.add("userId", ApplicationCache.INSTANCE.getUserInfo().getId());
		params.add("key", searchet.getText().toString());
		HttpTool.HttpPost(0, mContext, ApplicationConstants.SERVER_URL+"/mobi/account/getbindrequest", params, this, new TypeToken<BaseResult<List<AgentInfo>>>() {
		}.getType());
		
	}
	
	
	@Override
	public void onResponse(int requsetCode, boolean resultStatus, Object result) {
		// TODO Auto-generated method stub
		switch(requsetCode){
		case 0:
			if(resultStatus){
				BaseResult<List<AgentInfo>> re=(BaseResult<List<AgentInfo>>) result;
				AgentAdapter adapter=new AgentAdapter(mContext, re.getResult());
				listview.setAdapter(adapter);
			}
			break;
		}
	}


	@Override
	public void onFailure() {
		// TODO Auto-generated method stub
		
	}


	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		switch(v.getId()){
		case R.id.searchiv:
			loadData();
			break;
		case R.id.back:
			finish();
			break;
		case R.id.right:
			Intent in=new Intent(mContext, CountActivity.class);
			startActivity(in);
			break;
		}
	}


	@Override
	public void onItemClick(AdapterView<?> parent, View view, int position,
			long id) {
		// TODO Auto-generated method stub
		AgentInfo info=(AgentInfo) view.getTag();
		Intent in=new Intent(mContext, AgentDetailActivity.class);
		in.putExtra("info", info);
			//待审核
			//通过审核
			//已冻结
		startActivity(in);	
	}
	
}
