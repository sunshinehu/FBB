package com.sunday.fangbeibei.activity;

import java.util.List;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.google.gson.reflect.TypeToken;
import com.loopj.android.http.RequestParams;
import com.sunday.fangbeibei.R;
import com.sunday.fangbeibei.adapter.CustomListAdapter;
import com.sunday.fangbeibei.constants.ApplicationConstants;
import com.sunday.fangbeibei.http.HttpResponseInterface;
import com.sunday.fangbeibei.http.HttpTool;
import com.sunday.fangbeibei.orm.BaseResult;
import com.sunday.fangbeibei.orm.CustomInfo;

/**
 * 报备客户列表（经纪人管理）
 * @author 晨曦
 *
 */
public class CustomListActivity extends BaseActivity implements OnClickListener,OnItemClickListener,HttpResponseInterface{

	private ListView listview;
	private EditText searchet;
	private ImageView searchiv;
	
	@Override
	protected void createActivity(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		setContentView(R.layout.custom_list_layout);
		loadView();
		loadData();
	}

	
	private void loadView(){
		
		TextView title=(TextView) findViewById(R.id.title);
		title.setText("报备客户");
		ImageView back=(ImageView) findViewById(R.id.back);
		back.setOnClickListener(this);
	
		listview=(ListView) findViewById(R.id.listview);
		listview.setOnItemClickListener(this);

		searchet=(EditText) findViewById(R.id.searchet);
		searchiv=(ImageView) findViewById(R.id.searchiv);
		
		searchiv.setOnClickListener(this);
	}
	
	
	private void loadData(){
		
		
		RequestParams params=new RequestParams();
		params.add("userId", getIntent().getStringExtra("id"));
		params.add("key", searchet.getText().toString());
		HttpTool.HttpPost(0, mContext, ApplicationConstants.SERVER_URL+"/mobi/customer/searchcustomer", params, this, new TypeToken<BaseResult<List<CustomInfo>>>() {
		}.getType());
		
		
	}
	
	
	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		switch(v.getId()){
		case R.id.back:
			finish();
			break;
		case R.id.searchiv:
			loadData();
			break;
		}
	}


	@Override
	public void onResponse(int requsetCode, boolean resultStatus, Object result) {
		// TODO Auto-generated method stub
		if(resultStatus){
			BaseResult<List<CustomInfo>> re=(BaseResult<List<CustomInfo>>) result;
			List<CustomInfo> list=re.getResult();
			CustomListAdapter adapter=new CustomListAdapter(mContext, list);
			listview.setAdapter(adapter);
		}
	}


	@Override
	public void onFailure() {
		// TODO Auto-generated method stub
		
	}


	@Override
	public void onItemClick(AdapterView<?> parent, View view, int position,
			long id) {
		// TODO Auto-generated method stub
		Intent in=new Intent(this, CustomDetailActivity.class);
		CustomInfo info=(CustomInfo) view.getTag();
		in.putExtra("info", info);
		in.putExtra("flag", false);
		startActivity(in);
	}
	
}
