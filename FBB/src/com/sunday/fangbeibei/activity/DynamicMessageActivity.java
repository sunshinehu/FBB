package com.sunday.fangbeibei.activity;

import java.util.List;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.google.gson.reflect.TypeToken;
import com.loopj.android.http.RequestParams;
import com.sunday.fangbeibei.R;
import com.sunday.fangbeibei.adapter.DynamicMessageAdapter;
import com.sunday.fangbeibei.cache.ApplicationCache;
import com.sunday.fangbeibei.constants.ApplicationConstants;
import com.sunday.fangbeibei.http.HttpResponseInterface;
import com.sunday.fangbeibei.http.HttpTool;
import com.sunday.fangbeibei.orm.BaseResult;
import com.sunday.fangbeibei.orm.DynamicMessage;

/**
 * 
 * @author 晨曦
 *
 */
public class DynamicMessageActivity extends BaseActivity implements OnClickListener,OnItemClickListener,HttpResponseInterface{

	private ListView listview;
	
	@Override
	protected void createActivity(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		setContentView(R.layout.dynamic_message_layout);
		loadView();
		loadData();
	}

	

	private void loadView(){
		
		TextView title=(TextView) findViewById(R.id.title);
		title.setText("我的消息");
		ImageView back=(ImageView) findViewById(R.id.back);
		back.setOnClickListener(this);
		
		listview=(ListView) findViewById(R.id.listview);
		listview.setOnItemClickListener(this);
	}

	
	private void loadData(){
		
		RequestParams params=new RequestParams();
		params.add("userId", ApplicationCache.INSTANCE.getUserInfo().getId());
		params.add("type", getIntent().getStringExtra("type"));
		HttpTool.HttpPost(0, mContext, ApplicationConstants.SERVER_URL+"/mobi/blog/newList", params, this, new TypeToken<BaseResult<List<DynamicMessage>>>() {
		}.getType());
		
	}
	


	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		switch(v.getId()){
		
		case R.id.back:
			finish();
			break;
	
		}
	}



	@Override
	public void onResponse(int requsetCode, boolean resultStatus, Object result) {
		// TODO Auto-generated method stub
		if(requsetCode==0&&resultStatus){
			
			BaseResult<List<DynamicMessage>> re=(BaseResult<List<DynamicMessage>>) result;
			
			DynamicMessageAdapter adapter=new DynamicMessageAdapter(mContext, re.getResult());
			listview.setAdapter(adapter);
			
		}
	}



	@Override
	public void onFailure() {
		// TODO Auto-generated method stub
		
	}



	@Override
	public void onItemClick(AdapterView<?> parent, View view, int position,
			long id) {
		// TODO Auto-generated method stub
		String blogId=(String) view.getTag();
		Intent intent=new Intent(mContext, DynamicDetailActivity.class);
		intent.putExtra("id", blogId);
		startActivity(intent);
	}

	
}
