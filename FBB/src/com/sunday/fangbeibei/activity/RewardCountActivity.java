package com.sunday.fangbeibei.activity;

import java.util.ArrayList;
import java.util.List;

import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.google.gson.reflect.TypeToken;
import com.loopj.android.http.RequestParams;
import com.sunday.fangbeibei.R;
import com.sunday.fangbeibei.adapter.RewardAdapter;
import com.sunday.fangbeibei.cache.ApplicationCache;
import com.sunday.fangbeibei.constants.ApplicationConstants;
import com.sunday.fangbeibei.http.HttpResponseInterface;
import com.sunday.fangbeibei.http.HttpTool;
import com.sunday.fangbeibei.orm.BaseResult;
import com.sunday.fangbeibei.orm.HouseInfo;
import com.sunday.fangbeibei.orm.RewardInfo;
import com.sunday.fangbeibei.orm.StoreFilter;
import com.sunday.fangbeibei.orm.TotalReward;
import com.sunday.fangbeibei.orm.UserInfo;
import com.sunday.fangbeibei.widget.CoPopupWindow;
import com.sunday.fangbeibei.widget.CoPopupWindow.OnPopupWindowClickListener;

/**
 * 

* @ClassName: RewardCountActivity 

* @Description: TODO(佣金统计) 

* @date 2015年2月26日 上午11:11:10 

*
 */
public class RewardCountActivity extends BaseActivity implements OnClickListener,HttpResponseInterface,OnPopupWindowClickListener{

	
	private TextView tv1;
	private TextView tv2;
	private TextView tv3;
	
	private TextView btn1;
	private TextView btn2;
	private TextView btn3;
	
	private ImageView arrow1;
	private ImageView arrow2;
	private ImageView arrow3;
	
	private StoreFilter filter;
	
	private int index;
	
	private int type=1;
	
	private String userId="";
	private String premisesId="";
	
	private ListView listview;
	
	private View currentView;
	
	@Override
	protected void createActivity(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		setContentView(R.layout.reward_count_layout);
		loadView();
		loadTotal();
		loadList();
	}

	

	private void loadView(){
		
		TextView title=(TextView) findViewById(R.id.title);
		title.setText("佣金统计");
		ImageView back=(ImageView) findViewById(R.id.back);
		back.setOnClickListener(this);
		
		tv1=(TextView) findViewById(R.id.tv1);		
		tv2=(TextView) findViewById(R.id.tv2);		
		tv3=(TextView) findViewById(R.id.tv3);
		
		btn1=(TextView) findViewById(R.id.btn1);		
		btn2=(TextView) findViewById(R.id.btn2);		
		btn3=(TextView) findViewById(R.id.btn3);
		
		arrow1=(ImageView) findViewById(R.id.arrow1);
		arrow2=(ImageView) findViewById(R.id.arrow2);
		arrow3=(ImageView) findViewById(R.id.arrow3);
		
		listview=(ListView) findViewById(R.id.listview);
		
		btn1.setOnClickListener(this);
		btn2.setOnClickListener(this);
		btn3.setOnClickListener(this);
		
		LinearLayout type1=(LinearLayout) findViewById(R.id.type1);
		LinearLayout type2=(LinearLayout) findViewById(R.id.type2);
		LinearLayout type3=(LinearLayout) findViewById(R.id.type3);
	
		type1.setOnClickListener(this);
		type2.setOnClickListener(this);
		type3.setOnClickListener(this);
		
	}


	
	private void loadTotal(){
		
		RequestParams params=new RequestParams();
		params.add("storeId", ApplicationCache.INSTANCE.getUserInfo().getStoreId());
		params.add("filter_L_userId", userId);
		params.add("filter_L_premisesId", premisesId);
		HttpTool.HttpPost(0, this, ApplicationConstants.SERVER_URL+"/mobi/commission/getTotalCommission", params, this, new TypeToken<BaseResult<List<TotalReward>>>() {
		}.getType());
		
	}
	
	
	private void loadList(){
		
		show("");
		
		String url="";
		if(type==1||type==3){
			url="/mobi/commission/getCommission";
		}else{
			url="/mobi/commission/getPayedCommission";
		}
		RequestParams params=new RequestParams();
		params.add("storeId", ApplicationCache.INSTANCE.getUserInfo().getStoreId());
		params.add("filter_L_userId", userId);
		params.add("filter_L_premisesId", premisesId);
		HttpTool.HttpPost(2, this, ApplicationConstants.SERVER_URL+url, params, this, new TypeToken<BaseResult<List<RewardInfo>>>() {
		}.getType());
	}
	
	


	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		switch(v.getId()){
		case R.id.back:
			finish();
			break;
		case R.id.btn1:
			loadMenu(1);
			premisesId="";
			userId="";
			loadTotal();
			loadList();
			break;
		case R.id.btn2:
			loadMenu(2);
			index=2;
			if(filter==null){
				currentView=v;
				RequestParams params=new RequestParams();
				params.add("storeId", ApplicationCache.INSTANCE.getUserInfo().getStoreId());
				HttpTool.HttpPost(1, this, ApplicationConstants.SERVER_URL+"/mobi/commission/getStoreFilter", params, this, new TypeToken<BaseResult<StoreFilter>>() {
				}.getType());
			}else{
				CoPopupWindow co=new CoPopupWindow(this);
				List<String> list=new ArrayList<String>();
				for(HouseInfo info:filter.getPremisesInfos()){
					list.add(info.getName());
				}
				co.changeData(list);
				co.showAsDropDown(v,0,0);
				co.setOnPopupWindowClickListener(this);
			}
			break;
		case R.id.btn3:
			loadMenu(3);
			index=3;
			if(filter==null){
				currentView=v;
				RequestParams params=new RequestParams();
				params.add("storeId", ApplicationCache.INSTANCE.getUserInfo().getStoreId());
				HttpTool.HttpPost(1, this, ApplicationConstants.SERVER_URL+"/mobi/commission/getStoreFilter", params, this, new TypeToken<BaseResult<StoreFilter>>() {
				}.getType());
			}else{
				CoPopupWindow co=new CoPopupWindow(this);
				List<String> list=new ArrayList<String>();
				for(UserInfo info:filter.getUsers()){
					list.add(info.getName());
				}
				co.changeData(list);
				co.showAsDropDown(v,0,0);
				co.setOnPopupWindowClickListener(this);
			}
			break;
		case R.id.type1:
			type=1;
			arrow1.setVisibility(View.VISIBLE);
			arrow2.setVisibility(View.INVISIBLE);
			arrow3.setVisibility(View.INVISIBLE);
			loadList();
			break;
		case R.id.type2:
			type=2;
			arrow2.setVisibility(View.VISIBLE);
			arrow1.setVisibility(View.INVISIBLE);
			arrow3.setVisibility(View.INVISIBLE);
			loadList();
			break;
		case R.id.type3:
			type=3;
			arrow3.setVisibility(View.VISIBLE);
			arrow2.setVisibility(View.INVISIBLE);
			arrow1.setVisibility(View.INVISIBLE);
			loadList();
			break;
			}
		}


	private void loadMenu(int pos){
		
		btn1.setBackgroundDrawable(mContext.getResources().getDrawable(R.drawable.btn1_n));
		btn2.setBackgroundDrawable(mContext.getResources().getDrawable(R.drawable.btn2_n));
		btn3.setBackgroundDrawable(mContext.getResources().getDrawable(R.drawable.btn3_n));
		
		btn1.setTextColor(mContext.getResources().getColor(R.color.menugrey));		
		btn2.setTextColor(mContext.getResources().getColor(R.color.menugrey));		
		btn3.setTextColor(mContext.getResources().getColor(R.color.menugrey));
		
		if(pos==1){
			btn1.setBackgroundDrawable(mContext.getResources().getDrawable(R.drawable.btn1_s));
			btn1.setTextColor(mContext.getResources().getColor(R.color.white));		
		}else if(pos==2){
			btn2.setBackgroundDrawable(mContext.getResources().getDrawable(R.drawable.btn2_s));
			btn2.setTextColor(mContext.getResources().getColor(R.color.white));
		}else if(pos==3){
			btn3.setBackgroundDrawable(mContext.getResources().getDrawable(R.drawable.btn3_s));
			btn3.setTextColor(mContext.getResources().getColor(R.color.white));
		}
	}
	
	

	@Override
	public void onResponse(int requsetCode, boolean resultStatus, Object result) {
		// TODO Auto-generated method stub
		disMiss();
		switch(requsetCode){
		case 0:
			if(resultStatus){
				BaseResult<List<TotalReward>> result1=(BaseResult<List<TotalReward>>) result;
				if(result1.getResult()!=null&&result1.getResult().size()>0){
					TotalReward reward=result1.getResult().get(0);
					String[] ss=new String[3];
					ss[0]=reward.getTotalcommission().substring(0,reward.getTotalcommission().indexOf("."));
					ss[1]=reward.getPayedcommission().substring(0,reward.getPayedcommission().indexOf("."));
					ss[2]=reward.getNotpayedcommission().substring(0,reward.getNotpayedcommission().indexOf("."));
					tv1.setText(ss[0]);
					tv2.setText(ss[1]);
					tv3.setText(ss[2]);
				}
			}else{
				
			}
			break;
		case 1:
			if(resultStatus){
				BaseResult<StoreFilter> result2=(BaseResult<StoreFilter>) result;
				filter=result2.getResult();
				if(index==2){
					CoPopupWindow co=new CoPopupWindow(this);
					List<String> list=new ArrayList<String>();
					for(HouseInfo info:filter.getPremisesInfos()){
						list.add(info.getName());
					}
					co.changeData(list);
					co.showAsDropDown(currentView,0,0);
					co.setOnPopupWindowClickListener(this);
				}else if(index==3){
					CoPopupWindow co=new CoPopupWindow(this);
					List<String> list=new ArrayList<String>();
					for(UserInfo info:filter.getUsers()){
						list.add(info.getName());
					}
					co.changeData(list);
					co.showAsDropDown(currentView,0,0);
					co.setOnPopupWindowClickListener(this);
				}
			}else{
				
			}
			break;
		case 2:
			if(resultStatus){
				BaseResult<List<RewardInfo>> result3=(BaseResult<List<RewardInfo>>) result;
				RewardAdapter adapter=new RewardAdapter(mContext, result3.getResult(), type);
				listview.setAdapter(adapter);
			}else{
				
			}
			break;
		}
	}



	@Override
	public void onFailure() {
		// TODO Auto-generated method stub
		disMiss();
	}



	@Override
	public void onPopupWindowItemClick(int position) {
		// TODO Auto-generated method stub
		if(index==2){
			premisesId=filter.getPremisesInfos().get(position).getId();
			loadTotal();
			loadList();
		}else if(index==3){
			userId=filter.getUsers().get(position).getId();
			loadTotal();
			loadList();
		}
	}
	
}
