package com.sunday.fangbeibei.activity;

import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import android.widget.TextView;

import com.sunday.fangbeibei.R;
import com.sunday.fangbeibei.orm.HouseInfo;

/**
 * 房型参数
 * @author 晨曦
 *
 */
public class HouseParameterActivity extends BaseActivity implements OnClickListener{

	private HouseInfo info;
	
	
	@Override
	protected void createActivity(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		setContentView(R.layout.house_parameter_layout);
		info=(HouseInfo) getIntent().getSerializableExtra("info");
		loadView();
	}

	private void loadView(){
		
		TextView title=(TextView) findViewById(R.id.title);
		title.setText("楼盘参数");
		ImageView back=(ImageView) findViewById(R.id.back);
		back.setOnClickListener(this);
		
		TextView tv1=(TextView) findViewById(R.id.tv1);
		TextView tv2=(TextView) findViewById(R.id.tv2);
		TextView tv3=(TextView) findViewById(R.id.tv3);
		TextView tv4=(TextView) findViewById(R.id.tv4);
		TextView tv5=(TextView) findViewById(R.id.tv5);
		TextView tv6=(TextView) findViewById(R.id.tv6);
		TextView tv7=(TextView) findViewById(R.id.tv7);
		TextView tv8=(TextView) findViewById(R.id.tv8);
		TextView tv9=(TextView) findViewById(R.id.tv9);
		TextView tv10=(TextView) findViewById(R.id.tv10);
		TextView tv11=(TextView) findViewById(R.id.tv11);
		
		
		tv1.setText(info.getOpeningTime());
		tv2.setText(info.getRealestateName());
		tv3.setText(info.getRopertyCompany());
		tv4.setText(info.getPropertyType());
		if(info.getArea()==null){
			info.setArea("");
		}
		tv5.setText(info.getArea());
		tv6.setText(info.getDecoration());
		tv7.setText(info.getTotalamount());
		tv8.setText(info.getParkingamount());
		tv9.setText(info.getFloorAreaRatio());
		tv10.setText(info.getGreenRation());
		tv11.setText(info.getPropertyCost());
	}
	

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		switch(v.getId()){
		case R.id.back:
			finish();
			break;
		}
	}
	
}
