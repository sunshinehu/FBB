package com.sunday.fangbeibei.activity;

import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.reflect.TypeToken;
import com.loopj.android.http.RequestParams;
import com.sunday.fangbeibei.R;
import com.sunday.fangbeibei.cache.ApplicationCache;
import com.sunday.fangbeibei.constants.ApplicationConstants;
import com.sunday.fangbeibei.http.HttpResponseInterface;
import com.sunday.fangbeibei.http.HttpTool;
import com.sunday.fangbeibei.orm.BaseResult;

/**
 * 

* @ClassName: FeedBackActivity 

* @Description: TODO(意见反馈) 

* @date 2015年2月27日 下午4:22:28 

*
 */
public class FeedbackActivity extends BaseActivity implements OnClickListener,HttpResponseInterface{

	private EditText content;
	
	@Override
	protected void createActivity(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		setContentView(R.layout.feedback_layout);
		loadView();
	}


	private void loadView(){
		
		TextView title=(TextView) findViewById(R.id.title);
		title.setText("意见反馈");
		ImageView back=(ImageView) findViewById(R.id.back);
		back.setOnClickListener(this);
		
		content=(EditText) findViewById(R.id.content);
		TextView commit=(TextView) findViewById(R.id.commit);
		
		commit.setOnClickListener(this);
		
	}



	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		switch(v.getId()){
		
		case R.id.back:
			finish();
			break;
		case R.id.commit:
			RequestParams params=new RequestParams();
			params.add("userId", ApplicationCache.INSTANCE.getUserInfo().getId());
			params.add("content", content.getText().toString());
			HttpTool.HttpPost(0, mContext, ApplicationConstants.SERVER_URL+"/mobi/feedback/save", params, this, new TypeToken<BaseResult>() {
			}.getType());
			break;
			
		}
	}


	@Override
	public void onResponse(int requsetCode, boolean resultStatus, Object result) {
		// TODO Auto-generated method stub
		switch(requsetCode){
		case 0:
			if(resultStatus){
				Toast.makeText(mContext, "感谢您的反馈！", Toast.LENGTH_SHORT).show();
				finish();
			}else{
				Toast.makeText(mContext, "提交失败！", Toast.LENGTH_SHORT).show();
			}
			break;
		}
	}


	@Override
	public void onFailure() {
		// TODO Auto-generated method stub
		
	}

}
