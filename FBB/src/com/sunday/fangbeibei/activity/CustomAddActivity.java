package com.sunday.fangbeibei.activity;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.adw.library.widgets.discreteseekbar.DiscreteSeekBar;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.reflect.TypeToken;
import com.loopj.android.http.RequestParams;
import com.sunday.fangbeibei.R;
import com.sunday.fangbeibei.cache.ApplicationCache;
import com.sunday.fangbeibei.constants.ApplicationConstants;
import com.sunday.fangbeibei.http.HttpResponseInterface;
import com.sunday.fangbeibei.http.HttpTool;
import com.sunday.fangbeibei.orm.AreaInfo;
import com.sunday.fangbeibei.orm.BaseResult;
import com.sunday.fangbeibei.orm.CustomInfo;

/**
 * 添加客户
 * @author 晨曦
 *
 */
public class CustomAddActivity extends BaseActivity implements OnClickListener,HttpResponseInterface{

	private TextView total;
	
	private List<String> selectArea;
	
	private TextView layout1;
	private TextView layout2;
	private TextView layout3;
	private TextView layout4;
	
	private EditText name;
	private EditText phone;
	
	private TextView male;
	private TextView female;
		
	private int sex=-1;
	
	private String layout="不限";
	
	private int price;
	
	private CustomInfo info;
	private boolean flag;
	
	private String id;//houseid
	
	private OnClickListener areaClick=new OnClickListener() {
		
		@Override
		public void onClick(View v) {
			// TODO Auto-generated method stub
			TextView tv=(TextView) v;
			AreaInfo tag=(AreaInfo) v.getTag();
			if(selectArea.contains(tag.getName())){
				tv.setBackgroundDrawable(mContext.getResources().getDrawable(R.drawable.add_normal));
				tv.setTextColor(mContext.getResources().getColor(R.color.black));
				selectArea.remove(tag.getName());
			}else{
				tv.setBackgroundDrawable(mContext.getResources().getDrawable(R.drawable.add_select));
				tv.setTextColor(mContext.getResources().getColor(R.color.orange));
				selectArea.add(tag.getName());
			}
			tv.setPadding(0, 10, 0, 10);
		}
	};
	
	@Override
	protected void createActivity(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		setContentView(R.layout.custom_add_layout);
		info=(CustomInfo) getIntent().getSerializableExtra("info");
		flag=getIntent().getBooleanExtra("flag", false);
		loadView();
		loadData();
	}

	
	private void loadView(){
		
		TextView title=(TextView) findViewById(R.id.title);
		if(flag){
			title.setText("编辑用户");
		}else{
			title.setText("添加客户");
		}
		ImageView back=(ImageView) findViewById(R.id.back);
		back.setOnClickListener(this);
		total=(TextView) findViewById(R.id.total);
		
		name=(EditText) findViewById(R.id.name);
		phone=(EditText) findViewById(R.id.phone);
		
		layout1=(TextView) findViewById(R.id.layout1);
		layout2=(TextView) findViewById(R.id.layout2);
		layout3=(TextView) findViewById(R.id.layout3);
		layout4=(TextView) findViewById(R.id.layout4);
		
		layout1.setPadding(0, 10, 0, 10);
		layout2.setPadding(0, 10, 0, 10);
		layout3.setPadding(0, 10, 0, 10);
		layout4.setPadding(0, 10, 0, 10);
		
		male=(TextView) findViewById(R.id.male);
		female=(TextView) findViewById(R.id.female);
		
		male.setPadding(0, 5, 0, 5);
		female.setPadding(0, 5, 0, 5);
		
		male.setOnClickListener(this);
		female.setOnClickListener(this);
		
		layout1.setOnClickListener(this);
		layout2.setOnClickListener(this);
		layout3.setOnClickListener(this);
		layout4.setOnClickListener(this);
		TextView commit=(TextView) findViewById(R.id.commit);
		commit.setOnClickListener(this);
		
		String houseId=getIntent().getStringExtra("id");
		if(houseId!=null&&!houseId.equals("")){
			id=houseId;
			commit.setText("报备");
		}
		
		name=(EditText) findViewById(R.id.name);
		phone=(EditText) findViewById(R.id.phone);
		
		
		String na=getIntent().getStringExtra("name");
		String ph=getIntent().getStringExtra("phone");
		
		
		
		if(na!=null&&!na.equals("")){
			
			ph=ph.replace("-", "");
			ph=ph.replace("+", "");
			
			name.setText(na);
			phone.setText(ph);
		}
		
		DiscreteSeekBar discreteSeekBar = (DiscreteSeekBar) findViewById(R.id.discrete);
        discreteSeekBar.setNumericTransformer(new DiscreteSeekBar.NumericTransformer() {
            @Override
            public int transform(int value) {
            	if(value==20){
            		total.setText("总价  "+"不限");
            		price=0;
            	}else{
            		total.setText("总价  "+value*50+"万");
            		price=value*50;
            	}
            	return value * 50;
            }
        });

        if(flag){
        	//加载数据
        	name.setText(info.getName());
        	if(info.getSex()!=null){
        		if(info.getSex()==0){
            		sex=1;
            		female.setBackgroundDrawable(mContext.getResources().getDrawable(R.drawable.add_normal));
        			male.setBackgroundDrawable(mContext.getResources().getDrawable(R.drawable.add_select));
        			
        			male.setTextColor(mContext.getResources().getColor(R.color.orange));
        			female.setTextColor(mContext.getResources().getColor(R.color.black));
        			
        			male.setPadding(0, 5, 0, 5);
        			female.setPadding(0, 5, 0, 5);
        			
            	}else if(info.getSex()==1){
            		male.setBackgroundDrawable(mContext.getResources().getDrawable(R.drawable.add_normal));
        			female.setBackgroundDrawable(mContext.getResources().getDrawable(R.drawable.add_select));
        			
        			female.setTextColor(mContext.getResources().getColor(R.color.orange));
        			male.setTextColor(mContext.getResources().getColor(R.color.black));
        			
        			male.setPadding(0, 5, 0, 5);
        			female.setPadding(0, 5, 0, 5);
        			
            	}
        	}
        	
        	phone.setText(info.getMobile());
        	total.setText("总价  "+info.getMaxPrice());
        	
        	discreteSeekBar.setProgress(info.getMaxPrice()/50);
        	selectLayout(info.getLayout());
        	//area=info.getArea();
        	String[] areas=info.getArea().split(",");
        	List<String> areaList=Arrays.asList(areas);
        	selectArea=new ArrayList<String>();
        	for(String area:areaList){
        		
        		selectArea.add(area);
        		
        	}
        	
        }else{
        	selectArea=new ArrayList<String>();
        	selectArea.add("全城");
        }
        
	}
	


	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		switch(v.getId()){
		case R.id.back:
			finish();
			break;
		case R.id.layout1:
			layout1.setBackgroundDrawable(mContext.getResources().getDrawable(R.drawable.add_select));
			layout2.setBackgroundDrawable(mContext.getResources().getDrawable(R.drawable.add_normal));
			layout3.setBackgroundDrawable(mContext.getResources().getDrawable(R.drawable.add_normal));
			layout4.setBackgroundDrawable(mContext.getResources().getDrawable(R.drawable.add_normal));
			layout="不限";

			layout1.setTextColor(mContext.getResources().getColor(R.color.orange));
			layout2.setTextColor(mContext.getResources().getColor(R.color.black));
			layout3.setTextColor(mContext.getResources().getColor(R.color.black));
			layout4.setTextColor(mContext.getResources().getColor(R.color.black));			
			
			layout1.setPadding(0, 10, 0, 10);
			layout2.setPadding(0, 10, 0, 10);
			layout3.setPadding(0, 10, 0, 10);
			layout4.setPadding(0, 10, 0, 10);
			
			break;
		case R.id.layout2:
			layout1.setBackgroundDrawable(mContext.getResources().getDrawable(R.drawable.add_normal));
			layout2.setBackgroundDrawable(mContext.getResources().getDrawable(R.drawable.add_select));
			layout3.setBackgroundDrawable(mContext.getResources().getDrawable(R.drawable.add_normal));
			layout4.setBackgroundDrawable(mContext.getResources().getDrawable(R.drawable.add_normal));
			layout="一室";

			layout2.setTextColor(mContext.getResources().getColor(R.color.orange));
			layout1.setTextColor(mContext.getResources().getColor(R.color.black));
			layout3.setTextColor(mContext.getResources().getColor(R.color.black));
			layout4.setTextColor(mContext.getResources().getColor(R.color.black));
			
			layout1.setPadding(0, 10, 0, 10);
			layout2.setPadding(0, 10, 0, 10);
			layout3.setPadding(0, 10, 0, 10);
			layout4.setPadding(0, 10, 0, 10);
			
			break;
		case R.id.layout3:
			layout1.setBackgroundDrawable(mContext.getResources().getDrawable(R.drawable.add_normal));
			layout2.setBackgroundDrawable(mContext.getResources().getDrawable(R.drawable.add_normal));
			layout3.setBackgroundDrawable(mContext.getResources().getDrawable(R.drawable.add_select));
			layout4.setBackgroundDrawable(mContext.getResources().getDrawable(R.drawable.add_normal));
			layout="二室";

			layout3.setTextColor(mContext.getResources().getColor(R.color.orange));
			layout2.setTextColor(mContext.getResources().getColor(R.color.black));
			layout1.setTextColor(mContext.getResources().getColor(R.color.black));
			layout4.setTextColor(mContext.getResources().getColor(R.color.black));
			
			layout1.setPadding(0, 10, 0, 10);
			layout2.setPadding(0, 10, 0, 10);
			layout3.setPadding(0, 10, 0, 10);
			layout4.setPadding(0, 10, 0, 10);
			
			break;
		case R.id.layout4:
			layout1.setBackgroundDrawable(mContext.getResources().getDrawable(R.drawable.add_normal));
			layout2.setBackgroundDrawable(mContext.getResources().getDrawable(R.drawable.add_normal));
			layout3.setBackgroundDrawable(mContext.getResources().getDrawable(R.drawable.add_normal));
			layout4.setBackgroundDrawable(mContext.getResources().getDrawable(R.drawable.add_select));
			layout="三室";

			layout4.setTextColor(mContext.getResources().getColor(R.color.orange));
			layout2.setTextColor(mContext.getResources().getColor(R.color.black));
			layout3.setTextColor(mContext.getResources().getColor(R.color.black));
			layout1.setTextColor(mContext.getResources().getColor(R.color.black));
			
			layout1.setPadding(0, 10, 0, 10);
			layout2.setPadding(0, 10, 0, 10);
			layout3.setPadding(0, 10, 0, 10);
			layout4.setPadding(0, 10, 0, 10);
			
			break;
		case R.id.commit:
			if(name.getText().toString().equals("")){
				Toast.makeText(mContext, "请输入姓名", Toast.LENGTH_SHORT).show();
				return;
			}
			if(phone.getText().toString().equals("")){
				Toast.makeText(mContext, "请输入手机号", Toast.LENGTH_SHORT).show();
				return;
			}else{
				Pattern p = null;  
		        Matcher m = null;  
		        boolean b = false;   
		        p = Pattern.compile("^[1][3,4,5,8][0-9]{9}$"); // 验证手机号  
		        m = p.matcher(phone.getText().toString());  
		        b = m.matches();   
		        if(!b){
		        	Toast.makeText(mContext, "手机号码不合法，请重新输入！", Toast.LENGTH_SHORT).show();
		        	return;
		        }
			}
			if(sex==-1){
				Toast.makeText(mContext, "请选择性别", Toast.LENGTH_SHORT).show();
				return;
			}
			show("正在保存");
			RequestParams params=new RequestParams();
			params.add("userId", ApplicationCache.INSTANCE.getUserInfo().getId());
			params.add("mobile", phone.getText().toString());
			params.add("name", name.getText().toString());
			params.add("maxPrice", price+"");
			
			String area="";
			if(selectArea.size()>0){
				for(String name:selectArea){
					if(area.equals("")){
						area=name;
					}else{
						area=area+","+name;
					}
				}
			}
			
			params.add("area", area);
			params.add("layout",layout);
			params.add("sex",sex+"");
			if(flag){
				params.add("id",info.getId());
			}
			if(id!=null){
				params.add("premisesId",id);
				HttpTool.HttpPost(1, mContext, ApplicationConstants.SERVER_URL+"/mobi/report/savecusandrep", params, this, new TypeToken<BaseResult>() {
				}.getType());
			}else{
				HttpTool.HttpPost(1, mContext, ApplicationConstants.SERVER_URL+"/mobi/customer/savecustomer", params, this, new TypeToken<BaseResult>() {
				}.getType());
			}
			break;
		case R.id.male:
			male.setBackgroundDrawable(mContext.getResources().getDrawable(R.drawable.add_select));
			female.setBackgroundDrawable(mContext.getResources().getDrawable(R.drawable.add_normal));
			sex=0;
			
			male.setTextColor(mContext.getResources().getColor(R.color.orange));
			female.setTextColor(mContext.getResources().getColor(R.color.black));
			
			male.setPadding(0, 5, 0, 5);
			female.setPadding(0, 5, 0, 5);
			break;
		case R.id.female:
			female.setBackgroundDrawable(mContext.getResources().getDrawable(R.drawable.add_select));
			male.setBackgroundDrawable(mContext.getResources().getDrawable(R.drawable.add_normal));
			sex=1;
			
			female.setTextColor(mContext.getResources().getColor(R.color.orange));
			male.setTextColor(mContext.getResources().getColor(R.color.black));
			
			male.setPadding(0, 5, 0, 5);
			female.setPadding(0, 5, 0, 5);
			break;
		}
	}
	
	
	private void selectLayout(String layout){
		
		if(layout.equals("不限")){
			layout1.setBackgroundDrawable(mContext.getResources().getDrawable(R.drawable.add_select));
			layout2.setBackgroundDrawable(mContext.getResources().getDrawable(R.drawable.add_normal));
			layout3.setBackgroundDrawable(mContext.getResources().getDrawable(R.drawable.add_normal));
			layout4.setBackgroundDrawable(mContext.getResources().getDrawable(R.drawable.add_normal));

			layout1.setTextColor(mContext.getResources().getColor(R.color.orange));
			layout2.setTextColor(mContext.getResources().getColor(R.color.black));
			layout3.setTextColor(mContext.getResources().getColor(R.color.black));
			layout4.setTextColor(mContext.getResources().getColor(R.color.black));
			layout="不限";

		}else if(layout.equals("一室")){
			layout1.setBackgroundDrawable(mContext.getResources().getDrawable(R.drawable.add_normal));
			layout2.setBackgroundDrawable(mContext.getResources().getDrawable(R.drawable.add_select));
			layout3.setBackgroundDrawable(mContext.getResources().getDrawable(R.drawable.add_normal));
			layout4.setBackgroundDrawable(mContext.getResources().getDrawable(R.drawable.add_normal));
			
			layout2.setTextColor(mContext.getResources().getColor(R.color.orange));
			layout1.setTextColor(mContext.getResources().getColor(R.color.black));
			layout3.setTextColor(mContext.getResources().getColor(R.color.black));
			layout4.setTextColor(mContext.getResources().getColor(R.color.black));
			layout="一室";

		}else if(layout.equals("二室")){
			layout1.setBackgroundDrawable(mContext.getResources().getDrawable(R.drawable.add_normal));
			layout2.setBackgroundDrawable(mContext.getResources().getDrawable(R.drawable.add_normal));
			layout3.setBackgroundDrawable(mContext.getResources().getDrawable(R.drawable.add_select));
			layout4.setBackgroundDrawable(mContext.getResources().getDrawable(R.drawable.add_normal));

			layout3.setTextColor(mContext.getResources().getColor(R.color.orange));
			layout2.setTextColor(mContext.getResources().getColor(R.color.black));
			layout1.setTextColor(mContext.getResources().getColor(R.color.black));
			layout4.setTextColor(mContext.getResources().getColor(R.color.black));
			layout="二室";

		}else if(layout.equals("三室")){
			layout1.setBackgroundDrawable(mContext.getResources().getDrawable(R.drawable.add_normal));
			layout2.setBackgroundDrawable(mContext.getResources().getDrawable(R.drawable.add_normal));
			layout3.setBackgroundDrawable(mContext.getResources().getDrawable(R.drawable.add_normal));
			layout4.setBackgroundDrawable(mContext.getResources().getDrawable(R.drawable.add_select));

			layout4.setTextColor(mContext.getResources().getColor(R.color.orange));
			layout2.setTextColor(mContext.getResources().getColor(R.color.black));
			layout3.setTextColor(mContext.getResources().getColor(R.color.black));
			layout1.setTextColor(mContext.getResources().getColor(R.color.black));
			layout="三室";

		}

		layout1.setPadding(0, 10, 0, 10);
		layout2.setPadding(0, 10, 0, 10);
		layout3.setPadding(0, 10, 0, 10);
		layout4.setPadding(0, 10, 0, 10);
		
	}
	
	
	private void loadData(){
		show("");
		RequestParams params=new RequestParams();
		params.add("cityId", ApplicationCache.INSTANCE.getUserInfo().getDistrictId());
		HttpTool.HttpPost(0, mContext, ApplicationConstants.SERVER_URL+"/fangbb/city/getAreas", params, this, new TypeToken<BaseResult<List<AreaInfo>>>() {
		}.getType());
		
	}

	@Override
	public void onResponse(int requsetCode, boolean resultStatus, Object result) {
		// TODO Auto-generated method stub
		disMiss();
		switch(requsetCode){
		case 0:
			LinearLayout areall=(LinearLayout) findViewById(R.id.areall);
			BaseResult<List<AreaInfo>> re=(BaseResult<List<AreaInfo>>) result;
			AreaInfo info=new AreaInfo();
			info.setName("全城");
			info.setId(ApplicationCache.INSTANCE.getUserInfo().getDistrictId());
			re.getResult().add(0,info);
			LayoutInflater inflater=LayoutInflater.from(mContext);
			for(int i=0;i<=(re.getResult().size()-1)/4;i++){
				View view=inflater.inflate(R.layout.area_item, null);
				for(int j=4*i;j<4*i+4;j++){
					if(j==re.getResult().size()){
						break;
					}
					TextView[] tvs={(TextView) view.findViewById(R.id.name1),(TextView) view.findViewById(R.id.name2),(TextView) view.findViewById(R.id.name3),(TextView) view.findViewById(R.id.name4)};
					tvs[j%4].setVisibility(View.VISIBLE);
					tvs[j%4].setText(re.getResult().get(j).getName());
					tvs[j%4].setTag(re.getResult().get(j));
					tvs[j%4].setOnClickListener(areaClick);
					
					if(selectArea.contains(re.getResult().get(j).getName())){
						tvs[j%4].setBackgroundDrawable(mContext.getResources().getDrawable(R.drawable.add_select));
						tvs[j%4].setTextColor(mContext.getResources().getColor(R.color.orange));
					}else{
						tvs[j%4].setBackgroundDrawable(mContext.getResources().getDrawable(R.drawable.add_normal));
					}
					
					tvs[j%4].setPadding(0, 10, 0, 10);
					/*if(j==0){
						tvs[j%4].setBackgroundDrawable(mContext.getResources().getDrawable(R.drawable.add_select));
						tvs[j%4].setPadding(0, 10, 0, 10);
						lastSelect=tvs[j%4];
						area=re.getResult().get(0).getId();
					}*/
				}
				
				areall.addView(view);
				
			}
			break;
		case 1:
			if(resultStatus){
				Toast.makeText(mContext, "保存成功", Toast.LENGTH_SHORT).show();
				finish();
			}else{
				Toast.makeText(mContext, "保存失败", Toast.LENGTH_SHORT).show();
			}
			break;
		}
	}

	@Override
	public void onFailure() {
		// TODO Auto-generated method stub
		disMiss();
	}


	
}
