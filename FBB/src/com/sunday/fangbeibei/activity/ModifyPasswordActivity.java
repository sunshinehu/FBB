package com.sunday.fangbeibei.activity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.reflect.TypeToken;
import com.loopj.android.http.RequestParams;
import com.sunday.fangbeibei.R;
import com.sunday.fangbeibei.cache.ApplicationCache;
import com.sunday.fangbeibei.constants.ApplicationConstants;
import com.sunday.fangbeibei.http.HttpResponseInterface;
import com.sunday.fangbeibei.http.HttpTool;
import com.sunday.fangbeibei.orm.BaseResult;

/**
 * 

* @ClassName: ModifyPasswordActivity 

* @Description: TODO(修改密码) 

* @author sunshineHu hucx@itouch.com.cn

* @date 2015年2月12日 下午2:56:53 

*
 */
public class ModifyPasswordActivity extends BaseActivity implements OnClickListener,HttpResponseInterface{

	private EditText oldpwd;
	private EditText newpwd;
	private EditText repwd;
	
	@Override
	protected void createActivity(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		setContentView(R.layout.modify_password_layout);
		loadView();
	}

	private void loadView(){
		
		TextView title=(TextView) findViewById(R.id.title);
		title.setText("修改密码");
		ImageView back=(ImageView) findViewById(R.id.back);
		back.setOnClickListener(this);
	
		oldpwd=(EditText) findViewById(R.id.oldpwd);
		newpwd=(EditText) findViewById(R.id.newpwd);
		repwd=(EditText) findViewById(R.id.repwd);
		
		TextView commit=(TextView) findViewById(R.id.commit);
		commit.setOnClickListener(this);
	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		switch(v.getId()){
		case R.id.back:
			finish();
			break;
		case R.id.commit:
			if(newpwd.getText().toString().equals("")){
				Toast.makeText(mContext, "新密码不能为空！", Toast.LENGTH_SHORT).show();
				return;
			}
			if(repwd.getText().toString().equals("")){
				Toast.makeText(mContext, "重复密码不能为空！", Toast.LENGTH_SHORT).show();
				return;
			}
			if(oldpwd.getText().toString().equals("")){
				Toast.makeText(mContext, "旧密码不能为空！", Toast.LENGTH_SHORT).show();
				return;
			}
			show("");
			RequestParams params=new RequestParams();
			params.add("userId", ApplicationCache.INSTANCE.getUserInfo().getId());
			params.add("password", newpwd.getText().toString());
			params.add("rePassword", repwd.getText().toString());
			params.add("oldPassword", oldpwd.getText().toString());
			HttpTool.HttpPost(0, mContext, ApplicationConstants.SERVER_URL+"/mobi/account/modifypassword", params, this, new TypeToken<BaseResult>() {
			}.getType());
			break;
		}
	}

	@Override
	public void onResponse(int requsetCode, boolean resultStatus, Object result) {
		// TODO Auto-generated method stub
		switch(requsetCode){
		case 0:
			disMiss();
			if(resultStatus){
				Toast.makeText(mContext, "修改密码成功,请重新登录！", Toast.LENGTH_SHORT).show();
				ApplicationCache.INSTANCE.setUserInfo(null);
				SharedPreferences settings = getSharedPreferences("fbb", 0);
				SharedPreferences.Editor editor = settings.edit();
				editor.putString("password", "");
				editor.commit();
				Intent in=new Intent(mContext, LoginActivity.class);
				startActivity(in);
			}else{
				BaseResult re=(BaseResult) result;
				switch(re.getCode()){
				case -1:
					Toast.makeText(mContext, "系统异常！", Toast.LENGTH_SHORT).show();
					break;
				case -2:
					Toast.makeText(mContext, "没有该用户！", Toast.LENGTH_SHORT).show();
					break;
				case -3:
					Toast.makeText(mContext, "两次密码不一致！", Toast.LENGTH_SHORT).show();
					break;
				}
			}
			break;
		}
	}

	@Override
	public void onFailure() {
		// TODO Auto-generated method stub
		disMiss();
	}
	
}
