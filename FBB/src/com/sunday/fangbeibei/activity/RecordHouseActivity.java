package com.sunday.fangbeibei.activity;

import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.content.DialogInterface;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.reflect.TypeToken;
import com.loopj.android.http.RequestParams;
import com.sunday.fangbeibei.R;
import com.sunday.fangbeibei.adapter.RecordHouseAdapter;
import com.sunday.fangbeibei.cache.ApplicationCache;
import com.sunday.fangbeibei.constants.ApplicationConstants;
import com.sunday.fangbeibei.http.HttpResponseInterface;
import com.sunday.fangbeibei.http.HttpTool;
import com.sunday.fangbeibei.orm.BaseResult;
import com.sunday.fangbeibei.orm.CustomInfo;
import com.sunday.fangbeibei.orm.HouseInfo;
import com.sunday.fangbeibei.orm.HouseInfoResult;

/**
 * 报备楼盘
 * @author 晨曦
 *
 */
public class RecordHouseActivity extends BaseActivity implements OnClickListener,OnItemClickListener,HttpResponseInterface{

	private ListView listview;
	private CustomInfo info;
	private RecordHouseAdapter adapter;
	
	private String cityId="383";
	
	private TextView city1;
	private TextView city2;
	
	@Override
	protected void createActivity(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		setContentView(R.layout.record_house_layout);
		info=(CustomInfo) getIntent().getSerializableExtra("info");
		loadView();
		loadData();
	}

	private void loadView(){
		
		TextView title=(TextView) findViewById(R.id.title);
		title.setText("报备楼盘");
		ImageView back=(ImageView) findViewById(R.id.back);
		back.setOnClickListener(this);
		listview=(ListView) findViewById(R.id.listview);
		listview.setOnItemClickListener(this);
		
		city1=(TextView) findViewById(R.id.city1);
		city2=(TextView) findViewById(R.id.city2);
		city1.setOnClickListener(this);
		city2.setOnClickListener(this);
		//city1.setText(ApplicationCache.INSTANCE.getUserInfo().getCityName());
	}
	
	
	private void loadData(){
		
		RequestParams params=new RequestParams();
		params.add("customerId", info.getId());
		params.add("userId", ApplicationCache.INSTANCE.getUserInfo().getId());
		params.add("filter_L_districtId", cityId);
		HttpTool.HttpPost(0, this, ApplicationConstants.SERVER_URL+"/mobi/premises/getreportpis", params, this, new TypeToken<HouseInfoResult>() {
		}.getType());
		
		
	}
	
	

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		switch(v.getId()){
		case R.id.back:
			finish();
			break;
		case R.id.city1:
			cityId="383";
			Drawable drawable= getResources().getDrawable(R.drawable.ico_selct_s);
			/// 这一步必须要做,否则不会显示.
			drawable.setBounds(0, 0, drawable.getMinimumWidth(), drawable.getMinimumHeight());
			city1.setCompoundDrawables(null,null,drawable,null);
			city2.setCompoundDrawables(null,null,null,null);
			loadData();
			break;
		case R.id.city2:
			cityId="321";
			Drawable drawable2= getResources().getDrawable(R.drawable.ico_selct_s);
			/// 这一步必须要做,否则不会显示.
			drawable2.setBounds(0, 0, drawable2.getMinimumWidth(), drawable2.getMinimumHeight());
			city2.setCompoundDrawables(null,null,drawable2,null);
			city1.setCompoundDrawables(null,null,null,null);
			loadData();
			break;
		
		}
	}

	@Override
	public void onResponse(int requsetCode, boolean resultStatus, Object result) {
		// TODO Auto-generated method stub
		switch(requsetCode){
		case 0:
			HouseInfoResult re=(HouseInfoResult) result;
			if(resultStatus){
				adapter=new RecordHouseAdapter(mContext,re.getList());
				listview.setAdapter(adapter);
			}else{
				switch(re.getCode()){
				case -1:
					Toast.makeText(mContext, "系统异常！", Toast.LENGTH_SHORT).show();
					break;
				}
			}
			break;
		case 1:
			BaseResult res=(BaseResult) result;
			if(resultStatus){
				Toast.makeText(mContext, "报备成功！", Toast.LENGTH_SHORT).show();
				loadData();
			}else{
				switch(res.getCode()){
				case -1:
					Toast.makeText(mContext, "系统异常！", Toast.LENGTH_SHORT).show();
					break;
				case -2:
					Toast.makeText(mContext, "该用户已被报备", Toast.LENGTH_SHORT).show();
					break;
				}
			}
			break;
		}
	}

	@Override
	public void onFailure() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onItemClick(AdapterView<?> parent, View view, int position,
			long id) {
		// TODO Auto-generated method stub
		final HouseInfo house=(HouseInfo) view.getTag();
		if(house.getIsReported()==0){
			AlertDialog.Builder builer = new Builder(mContext) ; 
			builer.setTitle("报备提醒");
			builer.setMessage("是否报备到楼盘"+house.getName());
			builer.setPositiveButton("确定", new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int which) {
					RequestParams params=new RequestParams();
					params.add("customerId", info.getId());
					params.add("premisesId", house.getId());
					params.add("userId", ApplicationCache.INSTANCE.getUserInfo().getId());
					HttpTool.HttpPost(1, RecordHouseActivity.this, ApplicationConstants.SERVER_URL+"/mobi/report/savereport", params, RecordHouseActivity.this, new TypeToken<BaseResult>() {
					}.getType());
				}   
			});
			//当点取消按钮时进行登录
			builer.setNegativeButton("取消", new DialogInterface.OnClickListener() {
				public void onClick(DialogInterface dialog, int which) {
					// TODO Auto-generated method stub
				}
			});
			AlertDialog dialog = builer.create();
			dialog.show();
		}
	}
	

}
