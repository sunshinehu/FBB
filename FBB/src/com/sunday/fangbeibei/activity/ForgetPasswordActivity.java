package com.sunday.fangbeibei.activity;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.google.gson.reflect.TypeToken;
import com.loopj.android.http.RequestParams;
import com.sunday.fangbeibei.R;
import com.sunday.fangbeibei.activity.RegisterActivity.VerifyCheck;
import com.sunday.fangbeibei.cache.ApplicationCache;
import com.sunday.fangbeibei.constants.ApplicationConstants;
import com.sunday.fangbeibei.http.HttpResponseInterface;
import com.sunday.fangbeibei.http.HttpTool;
import com.sunday.fangbeibei.orm.BaseResult;

import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

/**
 * 

* @ClassName: ForgetPasswordActivity 

* @Description: TODO(忘记密码) 

* @date 2015年2月12日 下午4:25:27 

*
 */
public class ForgetPasswordActivity extends BaseActivity implements OnClickListener,HttpResponseInterface{

	private EditText newpwd;
	private EditText repwd;
	private EditText phone;
	private EditText verifycode;
	private TextView verify;
	
	private long lastVerify=0l;
	
	@Override
	protected void createActivity(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		setContentView(R.layout.forget_password_layout);
		loadView();
	}

	private void loadView(){
		

		TextView title=(TextView) findViewById(R.id.title);
		title.setText("忘记密码");
		ImageView back=(ImageView) findViewById(R.id.back);
		back.setOnClickListener(this);
	
		newpwd=(EditText) findViewById(R.id.newpwd);
		repwd=(EditText) findViewById(R.id.repwd);
		phone=(EditText) findViewById(R.id.phone);
		verifycode=(EditText) findViewById(R.id.verifycode);
		verify=(TextView) findViewById(R.id.verify);
		
		verify.setOnClickListener(this);
		
		TextView commit=(TextView) findViewById(R.id.commit);
		commit.setOnClickListener(this);
	}
	
	
	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		switch(v.getId()){
		case R.id.back:
			finish();
			break;
		case R.id.commit:
			if(newpwd.getText().toString().equals("")){
				Toast.makeText(mContext, "新密码不能为空！", Toast.LENGTH_SHORT).show();
				return;
			}
			if(repwd.getText().toString().equals("")){
				Toast.makeText(mContext, "重复密码不能为空！", Toast.LENGTH_SHORT).show();
				return;
			}
			if(verifycode.getText().toString().equals("")){
				Toast.makeText(mContext, "验证码不能为空！", Toast.LENGTH_SHORT).show();
				return;
			}
			if(phone.getText().toString().equals("")){
				Toast.makeText(mContext, "手机号不能为空！", Toast.LENGTH_SHORT).show();
				return;
			}
			show("");
			RequestParams params=new RequestParams();
			params.add("mobi", phone.getText().toString());
			params.add("password", newpwd.getText().toString());
			params.add("rePassword", repwd.getText().toString());
			params.add("activeCode", verifycode.getText().toString());
			HttpTool.HttpPost(1, mContext, ApplicationConstants.SERVER_URL+"/mobi/account/forgetpassword", params, this, new TypeToken<BaseResult>() {
			}.getType());
			break;
		case R.id.verify:
			if((System.currentTimeMillis()-lastVerify)<=60*1000){
				return;
			}
			String telephone=phone.getText().toString();
			Pattern p = Pattern.compile("^[1][3,4,5,8][0-9]{9}$"); // 验证手机号  
			Matcher m = p.matcher(telephone);  
	        if(m.matches()){
	        	//获取验证码的http请求
	        	show("");
				RequestParams param=new RequestParams();
				param.add("mobile", telephone);
				HttpTool.HttpPost(0, mContext, ApplicationConstants.SERVER_URL+"/mobi/account/sendActiveCode", param, this, new TypeToken<BaseResult>() {
				}.getType());
	        }else{
	        	Toast.makeText(mContext, "请输入正确的手机号！", Toast.LENGTH_SHORT).show();
	        	return;
	        } 
			break;
		}
	}

	


	class VerifyCheck extends Thread{

		@Override
		public void run() {
			// TODO Auto-generated method stub
			super.run();
			while(System.currentTimeMillis()-lastVerify<=60*1000){
				runOnUiThread(updateVerify);
				try {
					sleep(1000);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			
			runOnUiThread(new Runnable() {
				
				@Override
				public void run() {
					// TODO Auto-generated method stub
					verify.setText("重新获取验证码");
				}
			});
		}
		
	};
	

	Runnable updateVerify=new Runnable() {
		
		@Override
		public void run() {
			// TODO Auto-generated method stub
			verify.setText(60-(System.currentTimeMillis()-lastVerify)/1000+"秒后重新获取");
		}
	};

	@Override
	public void onResponse(int requsetCode, boolean resultStatus, Object result) {
		// TODO Auto-generated method stub
		disMiss();
		switch(requsetCode){
		case 0:
			if(resultStatus){
				lastVerify=System.currentTimeMillis();
				Toast.makeText(mContext, "验证码已发送至您的手机，请注意查收!", Toast.LENGTH_SHORT).show();
				VerifyCheck check=new VerifyCheck();
				check.start();
			}else{
				Toast.makeText(mContext, "获取验证码失败!", Toast.LENGTH_SHORT).show();
			}
			break;
		case 1:
			if(resultStatus){
				Toast.makeText(mContext, "重置密码成功!", Toast.LENGTH_SHORT).show();
				finish();
			}else{
				Toast.makeText(mContext, "重置密码失败!", Toast.LENGTH_SHORT).show();
			}
			break;
		}
	}

	@Override
	public void onFailure() {
		// TODO Auto-generated method stub
		disMiss();
	}
	
}
