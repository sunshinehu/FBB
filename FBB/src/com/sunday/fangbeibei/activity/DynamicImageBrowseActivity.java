package com.sunday.fangbeibei.activity;

import android.app.Activity;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.view.ViewGroup.LayoutParams;

import com.sunday.fangbeibei.adapter.DynamicImageBrowseAdapter;

/**
 * 

* @ClassName: ImageBrowseActivity 

* @Description: TODO(图片浏览activity) 

* @author sunshineHu hucx@itouch.com.cn

* @date 2015年1月26日 上午10:30:57 

*
 */
public class DynamicImageBrowseActivity extends Activity{

	private ViewPager viewPager;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		viewPager=new ViewPager(this);
		viewPager.setLayoutParams(new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT));
		setContentView(viewPager);
		String[] array=getIntent().getStringArrayExtra("images");
		viewPager.setAdapter(new DynamicImageBrowseAdapter(this, array));
		int pos=getIntent().getIntExtra("pos", 0);
		viewPager.setCurrentItem(pos);
		}

	
}
