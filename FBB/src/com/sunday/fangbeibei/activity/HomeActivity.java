package com.sunday.fangbeibei.activity;

import java.text.SimpleDateFormat;
import java.util.Date;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.RelativeLayout.LayoutParams;

import com.google.gson.reflect.TypeToken;
import com.loopj.android.http.RequestParams;
import com.sunday.fangbeibei.R;
import com.sunday.fangbeibei.app.AppManager;
import com.sunday.fangbeibei.cache.ApplicationCache;
import com.sunday.fangbeibei.constants.ApplicationConstants;
import com.sunday.fangbeibei.fragment.CustomFragment;
import com.sunday.fangbeibei.fragment.DynamicFragment;
import com.sunday.fangbeibei.fragment.HouseFragment;
import com.sunday.fangbeibei.fragment.MineFragment;
import com.sunday.fangbeibei.http.HttpResponseInterface;
import com.sunday.fangbeibei.http.HttpTool;
import com.sunday.fangbeibei.orm.BaseResult;
import com.sunday.fangbeibei.tool.VersionUpdate;

/**
 * 

* @ClassName: HomeActivity 

* @Description: TODO(首页) 

* @author sunshineHu 

* @date 2015年1月19日 下午1:01:19 

*
 */
public class HomeActivity extends BaseFragmentActivity implements OnClickListener,HttpResponseInterface{

	private long ExitTime = 0;
	
	private LinearLayout ll1;
	private RelativeLayout ll2;
	private LinearLayout ll3;
	private LinearLayout ll4;
	
	private ImageView iv1;
	private ImageView iv2;
	private ImageView iv3;
	private ImageView iv4;
	
	private TextView tv1;
	private TextView tv2;
	private TextView tv3;
	private TextView tv4;
	
	private Fragment fragment1;
	private Fragment fragment2;
	private Fragment fragment3;
	private Fragment fragment4;
	
	private TextView unread;
	
	private RelativeLayout bottom;
	
	private LinearLayout bottomMenu;
	
	@Override
	protected void createActivity(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		setContentView(R.layout.home_layout);
		loadview();
		setMenu(1);
		checkVersion();
		
		loadUnread();
	}

	

	private void loadview(){
		
		ll1=(LinearLayout) findViewById(R.id.ll1);
		ll2=(RelativeLayout) findViewById(R.id.ll2);
		ll3=(LinearLayout) findViewById(R.id.ll3);
		ll4=(LinearLayout) findViewById(R.id.ll4);
		
		iv1=(ImageView) findViewById(R.id.iv1);
		iv2=(ImageView) findViewById(R.id.iv2);
		iv3=(ImageView) findViewById(R.id.iv3);
		iv4=(ImageView) findViewById(R.id.iv4);
		
		tv1=(TextView) findViewById(R.id.tv1);
		tv2=(TextView) findViewById(R.id.tv2);
		tv3=(TextView) findViewById(R.id.tv3);
		tv4=(TextView) findViewById(R.id.tv4);
		
		unread=(TextView) findViewById(R.id.unread);
		
		
		ll1.setOnClickListener(this);
		ll2.setOnClickListener(this);
		ll3.setOnClickListener(this);
		ll4.setOnClickListener(this);
		
		
		//动态需要的输入框
		
		LayoutInflater inflater=LayoutInflater.from(this);
		bottom=(RelativeLayout) inflater.inflate(R.layout.reply_box, null);
		
		RelativeLayout parentrl=(RelativeLayout) findViewById(R.id.parentrl);
		parentrl.addView(bottom);
		bottom.setVisibility(View.GONE);
		
		bottomMenu=(LinearLayout) findViewById(R.id.bottom_menu);
		
	}


	private void loadUnread(){
		

		RequestParams param=new RequestParams();
		param.add("userId", ApplicationCache.INSTANCE.getUserInfo().getId());	
		HttpTool.HttpPost(0, this, ApplicationConstants.SERVER_URL+"/mobi/blog/getInfoCount", param, this, new TypeToken<BaseResult<Integer>>() {
		}.getType());
		
	}
	


	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		switch(v.getId()){
		case R.id.ll1:
			setMenu(1);
			break;
		case R.id.ll2:
			setMenu(2);
			break;
		case R.id.ll3:
			setMenu(3);
			break;
		case R.id.ll4:
			setMenu(4);
			break;
		}
	}

	private void checkVersion(){
		
		SharedPreferences settings = getSharedPreferences("fbb", 0);
		String updateTime=settings.getString("date", "");
		SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd");
		String now=sdf.format(new Date());
		if(!updateTime.equals(now)){
			SharedPreferences.Editor editor = settings.edit();
			editor.putString("date", sdf.format(new Date()));
			editor.commit();
			VersionUpdate update=new VersionUpdate(mContext);
			update.checkVersion(true);
		}
		
		
	}
	
	

	private void setMenu(int pos){
		
		bottom.setVisibility(View.GONE);
		bottomMenu.setVisibility(View.VISIBLE);
		
		iv1.setImageDrawable(getResources().getDrawable(R.drawable.tabbar_icon1_s));
		tv1.setTextColor(getResources().getColor(R.color.normal));
		ll1.setBackgroundColor(getResources().getColor(R.color.grey));
		iv2.setImageDrawable(getResources().getDrawable(R.drawable.tabbar_icon2_s));
		tv2.setTextColor(getResources().getColor(R.color.normal));
		ll2.setBackgroundColor(getResources().getColor(R.color.grey));
		iv3.setImageDrawable(getResources().getDrawable(R.drawable.tabbar_icon3_s));
		tv3.setTextColor(getResources().getColor(R.color.normal));
		ll3.setBackgroundColor(getResources().getColor(R.color.grey));
		iv4.setImageDrawable(getResources().getDrawable(R.drawable.tabbar_icon4_s));
		tv4.setTextColor(getResources().getColor(R.color.normal));
		ll4.setBackgroundColor(getResources().getColor(R.color.grey));
		
		 FragmentManager fm = this.getSupportFragmentManager();  
	        // 开启Fragment事务  
	     FragmentTransaction transaction = fm.beginTransaction();  
		
		if(pos==1){
			iv1.setImageDrawable(getResources().getDrawable(R.drawable.tabbar_icon1_s));
			tv1.setTextColor(getResources().getColor(R.color.select));
			ll1.setBackgroundColor(mContext.getResources().getColor(R.color.select));


			if(fragment1==null){
				fragment1=new HouseFragment();
			}
			transaction.replace(R.id.content, fragment1);
			
		}else if(pos==2){
			iv2.setImageDrawable(getResources().getDrawable(R.drawable.tabbar_icon2_s));
			tv2.setTextColor(getResources().getColor(R.color.select));
			ll2.setBackgroundColor(mContext.getResources().getColor(R.color.select));
			
			//未读清零
			ApplicationCache.INSTANCE.setUnread(0);
			unread.setVisibility(View.INVISIBLE);
			
			if(fragment2==null){
				fragment2=new DynamicFragment(bottom);
			}
			transaction.replace(R.id.content, fragment2);
			
		}else if(pos==3){
			
			if(ApplicationCache.INSTANCE.getUserInfo().getType()!=3){
				
			
				iv3.setImageDrawable(getResources().getDrawable(R.drawable.tabbar_icon3_s));
				tv3.setTextColor(getResources().getColor(R.color.select));
				ll3.setBackgroundColor(mContext.getResources().getColor(R.color.select));
				
				if(fragment3==null){
					fragment3=new CustomFragment();
				}
				transaction.replace(R.id.content, fragment3);
			
			}else{
			
				Toast.makeText(mContext, "您好，请注册成为房贝贝经纪人!", Toast.LENGTH_SHORT).show();;
				Intent in=new Intent(mContext, RegisterActivity.class);
				in.putExtra("flag", true);
				startActivity(in);
			
			}
		
			
		}else if(pos==4){
			iv4.setImageDrawable(getResources().getDrawable(R.drawable.tabbar_icon4_s));
			tv4.setTextColor(getResources().getColor(R.color.select));
			ll4.setBackgroundColor(mContext.getResources().getColor(R.color.select));
			
			if(fragment4==null){
				fragment4=new MineFragment();
			}
			transaction.replace(R.id.content, fragment4);
		}
		
		transaction.commit();
	}


	
	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		// TODO Auto-generated method stub
		if (keyCode == KeyEvent.KEYCODE_BACK
				&& event.getAction() == KeyEvent.ACTION_DOWN) {
			
			if ((System.currentTimeMillis() - ExitTime) > 2000) {
				Toast.makeText(getApplicationContext(), "再按一次退出!", 3)
						.show();
				ExitTime = System.currentTimeMillis();
			} else {
				AppManager.INSTANCE.AppExit(this);
			}
			return true;
		}
		return true;
	}



	@Override
	public void onResponse(int requsetCode, boolean resultStatus, Object result) {
		// TODO Auto-generated method stub
		if(resultStatus){
			BaseResult<Integer> re=(BaseResult<Integer>) result;
			int num=re.getResult();
			if(num>0){
				unread.setVisibility(View.VISIBLE);
			}else{
				unread.setVisibility(View.INVISIBLE);
			}
		}
	}



	@Override
	public void onFailure() {
		// TODO Auto-generated method stub
		
	}

}
