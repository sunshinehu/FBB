package com.sunday.fangbeibei.activity;

import java.util.List;

import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.google.gson.reflect.TypeToken;
import com.loopj.android.http.RequestParams;
import com.sunday.fangbeibei.R;
import com.sunday.fangbeibei.adapter.VolumeAdapter;
import com.sunday.fangbeibei.cache.ApplicationCache;
import com.sunday.fangbeibei.constants.ApplicationConstants;
import com.sunday.fangbeibei.http.HttpResponseInterface;
import com.sunday.fangbeibei.http.HttpTool;
import com.sunday.fangbeibei.orm.BaseResult;
import com.sunday.fangbeibei.orm.VolumeInfo;

/**
 * 成交量列表（经纪人管理）
 * @author 晨曦
 *
 */
public class VolumeActivity extends BaseActivity implements OnClickListener,HttpResponseInterface{

	private ListView listview;
	
	@Override
	protected void createActivity(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		setContentView(R.layout.volume_layout);
		loadView();
		loadData();
	}

	private void loadView(){
		
		TextView title=(TextView) findViewById(R.id.title);
		title.setText("成交量");
		ImageView back=(ImageView) findViewById(R.id.back);
		back.setOnClickListener(this);
	
		listview=(ListView) findViewById(R.id.listview);
	}
	
	

	private void loadData(){
		
		RequestParams params=new RequestParams();
		params.add("storeId", ApplicationCache.INSTANCE.getUserInfo().getStoreId());
		params.add("filter_L_userId", getIntent().getStringExtra("id"));
		HttpTool.HttpPost(0, mContext, ApplicationConstants.SERVER_URL+"/mobi/commission/getCommission", params, this, new TypeToken<BaseResult<List<VolumeInfo>>>() {
		}.getType());
		
	}
	

	
	
	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		switch(v.getId()){
		case R.id.back:
			finish();
			break;
		}
	}

	@Override
	public void onResponse(int requsetCode, boolean resultStatus, Object result) {
		// TODO Auto-generated method stub
		switch(requsetCode){
		case 0:
			if(resultStatus){
				BaseResult<List<VolumeInfo>> re=(BaseResult<List<VolumeInfo>>) result;
				VolumeAdapter adapter=new VolumeAdapter(mContext, re.getResult());
				listview.setAdapter(adapter);
			}
			break;
		}
	}

	@Override
	public void onFailure() {
		// TODO Auto-generated method stub
		
	}

	
}
