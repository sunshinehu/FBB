package com.sunday.fangbeibei.activity;

import java.util.List;

import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.google.gson.reflect.TypeToken;
import com.loopj.android.http.RequestParams;
import com.sunday.fangbeibei.R;
import com.sunday.fangbeibei.adapter.AreaSelectAdapter;
import com.sunday.fangbeibei.cache.ApplicationCache;
import com.sunday.fangbeibei.constants.ApplicationConstants;
import com.sunday.fangbeibei.http.HttpResponseInterface;
import com.sunday.fangbeibei.http.HttpTool;
import com.sunday.fangbeibei.orm.AreaInfo;
import com.sunday.fangbeibei.orm.BaseResult;

public class AreaSelectActivity extends BaseActivity implements OnItemClickListener,OnClickListener,HttpResponseInterface{

	private ListView listview;
	
	private TextView area1;
	private TextView area2;
	
	private String cityId="383";
	private String cityName="杭州市";
	
	@Override
	protected void createActivity(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		setContentView(R.layout.area_select_layout);
		loadView();
		if(ApplicationCache.INSTANCE.getUserInfo().getDistrictId()==null||!ApplicationCache.INSTANCE.getUserInfo().getDistrictId().equals("383")){
			loadMenu(2);
			cityId="321";
		}else{
			loadMenu(1);
		}
		loadData();
	}

	private void loadView(){
		
		TextView title=(TextView) findViewById(R.id.title);
		title.setText("选择地区");
		ImageView back=(ImageView) findViewById(R.id.back);
		back.setOnClickListener(this);
		
		listview=(ListView) findViewById(R.id.listview);
		listview.setOnItemClickListener(this);
		
		area1=(TextView) findViewById(R.id.area1);
		area2=(TextView) findViewById(R.id.area2);
		area1.setOnClickListener(this);
		area2.setOnClickListener(this);
	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		switch(v.getId()){
		case R.id.back:
			setResult(RESULT_CANCELED);
			finish();
			break;
		case R.id.area1:
			loadMenu(1);
			cityId="383";
			cityName="杭州市";
			loadData();
			break;
		case R.id.area2:
			loadMenu(2);
			cityId="321";
			cityName="上海市";
			loadData();
			break;
		}
	}
	
	private void loadMenu(int position){
		if(position==1){
			area1.setBackgroundDrawable(mContext.getResources().getDrawable(R.drawable.area_select));
			area1.setTextColor(mContext.getResources().getColor(R.color.white));
			area2.setBackgroundDrawable(mContext.getResources().getDrawable(R.drawable.area_normal));
			area2.setTextColor(mContext.getResources().getColor(R.color.black));
		}else if(position==2){
			area2.setBackgroundDrawable(mContext.getResources().getDrawable(R.drawable.area_select));
			area2.setTextColor(mContext.getResources().getColor(R.color.white));
			area1.setBackgroundDrawable(mContext.getResources().getDrawable(R.drawable.area_normal));
			area1.setTextColor(mContext.getResources().getColor(R.color.black));
		}
	}
	
	private void loadData(){
		show("");
		RequestParams params=new RequestParams();
		params.add("cityId", cityId);
		HttpTool.HttpPost(0, mContext, ApplicationConstants.SERVER_URL+"/fangbb/city/getAreas", params, this, new TypeToken<BaseResult<List<AreaInfo>>>() {
		}.getType());
		
	}

	@Override
	public void onResponse(int requsetCode, boolean resultStatus, Object result) {
		// TODO Auto-generated method stub
		disMiss();
		switch(requsetCode){
		case 0:
			BaseResult<List<AreaInfo>> re=(BaseResult<List<AreaInfo>>) result;
			List<AreaInfo> list=re.getResult();
			AreaInfo info=new AreaInfo();
			info.setParentId(cityId);
			info.setName("全部");
			list.add(0,info);
			AreaSelectAdapter adapter=new AreaSelectAdapter(mContext, re.getResult());
			listview.setAdapter(adapter);
			break;
		}
	}

	@Override
	public void onFailure() {
		// TODO Auto-generated method stub
		disMiss();
	}

	@Override
	public void onItemClick(AdapterView<?> parent, View view, int position,
			long id) {
		// TODO Auto-generated method stub
		AreaInfo info=(AreaInfo) view.getTag();
		if(info.getId()==null){
			info.setName(cityName);
		}
		ApplicationCache.INSTANCE.setAreaInfo(info);
		ApplicationCache.INSTANCE.getUserInfo().setDistrictId(cityId);
		setResult(RESULT_OK);
		finish();
	}
	
	
}
