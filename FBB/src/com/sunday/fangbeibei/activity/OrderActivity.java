package com.sunday.fangbeibei.activity;

import java.util.Calendar;

import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.fourmob.datetimepicker.date.DatePickerDialog;
import com.fourmob.datetimepicker.date.DatePickerDialog.OnDateSetListener;
import com.google.gson.reflect.TypeToken;
import com.loopj.android.http.RequestParams;
import com.sleepbot.datetimepicker.time.RadialPickerLayout;
import com.sleepbot.datetimepicker.time.TimePickerDialog;
import com.sleepbot.datetimepicker.time.TimePickerDialog.OnTimeSetListener;
import com.sunday.fangbeibei.R;
import com.sunday.fangbeibei.cache.ApplicationCache;
import com.sunday.fangbeibei.constants.ApplicationConstants;
import com.sunday.fangbeibei.http.HttpResponseInterface;
import com.sunday.fangbeibei.http.HttpTool;
import com.sunday.fangbeibei.orm.BaseResult;

/**
 * 预约看房
 * @author 晨曦
 *
 */
public class OrderActivity extends BaseFragmentActivity implements OnClickListener,OnDateSetListener,OnTimeSetListener,HttpResponseInterface{

	private EditText et1;
	private EditText et2;
	private EditText et3;
	
	private TextView tv1;
	private TextView tv2;
	private TextView tv3;
	
	private DatePickerDialog datePickerDialog;
	private TimePickerDialog timePickerDialog;
	
	private boolean dateFlag;
	private boolean timeFlag;
	
	@Override
	protected void createActivity(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		setContentView(R.layout.order_layout);
		loadView();
	}
	
	private void loadView(){
		
		TextView title=(TextView) findViewById(R.id.title);
		ImageView back=(ImageView) findViewById(R.id.back);
		title.setText("我要看房");
		back.setOnClickListener(this);
		
		
        Calendar calendar = Calendar.getInstance();
		datePickerDialog = DatePickerDialog.newInstance(this, calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH), false);
		timePickerDialog = TimePickerDialog.newInstance(this, calendar.get(Calendar.HOUR_OF_DAY) ,calendar.get(Calendar.MINUTE), true, false);
		
		et1=(EditText) findViewById(R.id.et1);
		et2=(EditText) findViewById(R.id.et2);
		et3=(EditText) findViewById(R.id.et3);
		
		tv1=(TextView) findViewById(R.id.tv1);
		tv2=(TextView) findViewById(R.id.tv2);
		tv3=(TextView) findViewById(R.id.tv3);
		
		tv1.setOnClickListener(this);
		tv2.setOnClickListener(this);
		tv3.setOnClickListener(this);
	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		switch(v.getId()){
		case R.id.tv1:
			datePickerDialog.setYearRange(1985, 2028);
            datePickerDialog.setCloseOnSingleTapDay(true);
            datePickerDialog.show(getSupportFragmentManager(),"");
			break;
		case R.id.tv2:
            timePickerDialog.setCloseOnSingleTapMinute(true);
            timePickerDialog.show(getSupportFragmentManager(), "");
			break;
		case R.id.tv3:
			if(!(dateFlag&&timeFlag)){
				Toast.makeText(mContext, "请选择看房时间", Toast.LENGTH_SHORT).show();
				return;
			}
			if(et1.getText().toString().equals("")){
				Toast.makeText(mContext, "请输入姓名", Toast.LENGTH_SHORT).show();
				return;
			}
			if(et2.getText().toString().equals("")){
				Toast.makeText(mContext, "请输入电话", Toast.LENGTH_SHORT).show();
				return;
			}
			
			RequestParams params=new RequestParams();
			params.add("userId", ApplicationCache.INSTANCE.getUserInfo().getId());
			params.add("mobi", et2.getText().toString());
			params.add("name", et1.getText().toString());
			params.add("ordertime", tv1.getText().toString()+" "+tv2.getText().toString());
			params.add("remark", et3.getText().toString());
			params.add("premisesId", getIntent().getStringExtra("id"));
			HttpTool.HttpPost(0, this, ApplicationConstants.SERVER_URL+"/mobi/request/addrequest", params, this, new TypeToken<BaseResult>() {
			}.getType());
			
			break;
		case R.id.back:
			finish();
			break;
		}
	}

	@Override
	public void onTimeSet(RadialPickerLayout view, int hourOfDay, int minute) {
		// TODO Auto-generated method stub
		tv2.setText(String.format("%02d", hourOfDay)+":"+String.format("%02d", minute));
		timeFlag=true;
	}

	@Override
	public void onDateSet(DatePickerDialog datePickerDialog, int year,
			int month, int day) {
		// TODO Auto-generated method stub
		tv1.setText(year+"-"+String.format("%02d", (month+1))+"-"+String.format("%02d", day));
		dateFlag=true;
	}

	@Override
	public void onResponse(int requsetCode, boolean resultStatus, Object result) {
		// TODO Auto-generated method stub
		switch(requsetCode){
		case 0:
			if(resultStatus){
				Toast.makeText(mContext, "预约成功!", Toast.LENGTH_SHORT).show();
				finish();
			}else{
				BaseResult re=(BaseResult) result;
				if(re.getCode()==-3){
					Toast.makeText(mContext, "您已经预约过了!", Toast.LENGTH_SHORT).show();
				}else{
					Toast.makeText(mContext, "预约失败!", Toast.LENGTH_SHORT).show();
				}
			}
			break;
		}
	}

	@Override
	public void onFailure() {
		// TODO Auto-generated method stub
		
	}

}
