package com.sunday.fangbeibei.activity;

import java.util.List;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.reflect.TypeToken;
import com.loopj.android.http.RequestParams;
import com.sunday.fangbeibei.R;
import com.sunday.fangbeibei.adapter.HouseRecordAdapter;
import com.sunday.fangbeibei.constants.ApplicationConstants;
import com.sunday.fangbeibei.http.HttpResponseInterface;
import com.sunday.fangbeibei.http.HttpTool;
import com.sunday.fangbeibei.orm.BaseResult;
import com.sunday.fangbeibei.orm.CustomInfo;
import com.sunday.fangbeibei.orm.RecordHouse;
import com.sunday.fangbeibei.orm.RecordHouseResult;

/**
 * 客户详情
 * @author 晨曦
 *
 */
public class CustomDetailActivity extends BaseActivity implements OnClickListener,HttpResponseInterface{

	private CustomInfo info;
	private ListView listview;
	
	private HouseRecordAdapter adapter;
	
	private List<RecordHouse> list;
	
	
	
	@Override
	protected void createActivity(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		setContentView(R.layout.custom_detail_layout);
		info=(CustomInfo) getIntent().getSerializableExtra("info");
		loadView();
		loadData();
	}

	private void loadView(){
	
		boolean flag=getIntent().getBooleanExtra("flag", true);
		
		
		TextView title=(TextView) findViewById(R.id.title);
		title.setText("客户详情");
		ImageView back=(ImageView) findViewById(R.id.back);
		back.setOnClickListener(this);
		
		TextView name=(TextView) findViewById(R.id.name);
		TextView phone=(TextView) findViewById(R.id.phone);
		TextView need=(TextView) findViewById(R.id.need);
		TextView add=(TextView) findViewById(R.id.add);
		add.setOnClickListener(this);
		
		TextView right=(TextView) findViewById(R.id.right);
		right.setVisibility(View.VISIBLE);
		right.setOnClickListener(this);
		
		listview=(ListView) findViewById(R.id.listview);

		ImageView call=(ImageView) findViewById(R.id.call);
		ImageView send=(ImageView) findViewById(R.id.sms);
		
		
		call.setOnClickListener(this);
		send.setOnClickListener(this);
		
		name.setText(info.getName());
		phone.setText(info.getMobile());
		String price;
		if(info.getMaxPrice().equals("0")){
			price="不限";
		}else{
			price="0-"+info.getMaxPrice()+"万";
		}
		need.setText(price+","+info.getArea()+","+info.getLayout());

		
		//只读
		if(!flag){
		
			right.setVisibility(View.INVISIBLE);
			add.setVisibility(View.INVISIBLE);
			
		}
		
	}
	
	
	private void loadData(){
		
		RequestParams params=new RequestParams();
		params.add("customerId", info.getId());
		HttpTool.HttpPost(0, this, ApplicationConstants.SERVER_URL+"/mobi/report/getreport", params, this, new TypeToken<RecordHouseResult>() {
		}.getType());
		
		
	}
	
	

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		switch(v.getId()){
		case R.id.back:
			finish();
			break;
		case R.id.add:
			Intent in=new Intent(mContext, RecordHouseActivity.class);
			in.putExtra("info", info);
			in.putExtra("flag", true);
			mContext.startActivity(in);
			break;
		case R.id.sms:
			 Intent intent2 = new Intent();
	         intent2.setAction(Intent.ACTION_SENDTO);
	         intent2.setData(Uri.parse("smsto:"+info.getMobile()));
	         //intent2.putExtra("sms_body", "发短信");
	         mContext.startActivity(intent2);
			break;
		case R.id.call:
		    Intent intent = new Intent(Intent.ACTION_DIAL,Uri.parse("tel:"+info.getMobile()));
		    mContext.startActivity(intent);
			break;
		case R.id.btn:
			RecordHouse house=(RecordHouse) v.getTag();
			RequestParams params=new RequestParams();
			params.add("customerId", info.getId());
			params.add("reportId", house.getId());
			HttpTool.HttpPost(1, this, ApplicationConstants.SERVER_URL+"/mobi/report/applylook", params, this, new TypeToken<BaseResult>() {
			}.getType());
			break;
		case R.id.more:
			RecordHouse tag=(RecordHouse) v.getTag();
			tag.setFlag(!tag.isFlag());
			adapter.notifyDataSetChanged();
			break;
		case R.id.right:
			Intent intent1=new Intent(mContext, CustomAddActivity.class);
			intent1.putExtra("info", info);
			intent1.putExtra("flag", true);
			startActivity(intent1);
			break;
		case R.id.callphone:
			String mobile=(String) v.getTag();
			Intent intents = new Intent(Intent.ACTION_DIAL,Uri.parse("tel:"+mobile));
		    mContext.startActivity(intents);
			break;
		}
	}

	@Override
	public void onResponse(int requsetCode, boolean resultStatus, Object result) {
		// TODO Auto-generated method stub
		switch(requsetCode){
		case 0:
			RecordHouseResult re=(RecordHouseResult) result;
			if(resultStatus){
				list=re.getList();
				adapter=new HouseRecordAdapter(mContext,list ,this);
				listview.setAdapter(adapter);
			}else{
				switch(re.getCode()){
				case -1:
					Toast.makeText(mContext, "系统异常！", Toast.LENGTH_SHORT).show();
					break;
				case -2:
					Toast.makeText(mContext, "该用户已被报备", Toast.LENGTH_SHORT).show();
					break;
				}
			}
			break;
		case 1:
			BaseResult res=(BaseResult) result;
			if(resultStatus){
				Toast.makeText(mContext, "申请带看成功", Toast.LENGTH_SHORT).show();
				loadData();
			}else{
				switch(res.getCode()){
				case -1:
					Toast.makeText(mContext, "系统异常！", Toast.LENGTH_SHORT).show();
					break;
				case -2:
					Toast.makeText(mContext, "该用户已被带看此楼盘", Toast.LENGTH_SHORT).show();
					break;
				}
			}
			break;
		}
	}

	@Override
	public void onFailure() {
		// TODO Auto-generated method stub
		
	}
	
}
