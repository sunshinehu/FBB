package com.sunday.fangbeibei.activity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.reflect.TypeToken;
import com.loopj.android.http.RequestParams;
import com.sunday.fangbeibei.R;
import com.sunday.fangbeibei.cache.ApplicationCache;
import com.sunday.fangbeibei.constants.ApplicationConstants;
import com.sunday.fangbeibei.http.HttpResponseInterface;
import com.sunday.fangbeibei.http.HttpTool;
import com.sunday.fangbeibei.orm.AreaInfo;
import com.sunday.fangbeibei.orm.BaseResult;
import com.sunday.fangbeibei.orm.UserInfo;

/**
 * 登陆
 * @author 晨曦
 *
 */
public class LoginActivity extends BaseActivity implements OnClickListener,HttpResponseInterface{

	private TextView account;
	private TextView password;
	
	
	@Override
	protected void createActivity(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		setContentView(R.layout.login_layout);
		loadview();
	}

	private void loadview(){
		
		TextView register=(TextView) findViewById(R.id.register);
		
		TextView login=(TextView) findViewById(R.id.login);
		account=(TextView) findViewById(R.id.account);
		password=(TextView) findViewById(R.id.password);
		
		TextView forget=(TextView) findViewById(R.id.forget);
		forget.setOnClickListener(this);
		
		SharedPreferences settings = getSharedPreferences("fbb", 0);
		String name=settings.getString("name", "");
		String pwd=settings.getString("password", "");
		
		if(!name.equals("")){
			account.setText(name);
		}
		if(!password.equals("")){
			password.setText(pwd);
		}
		login.setOnClickListener(this);
		register.setOnClickListener(this);
	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		switch(v.getId()){
		case R.id.back:
			setResult(RESULT_CANCELED);
			finish();
			break;
		case R.id.login:
			if(account.getText().toString().equals("")){
				Toast.makeText(mContext, "用户名不能为空！", Toast.LENGTH_SHORT).show();
				return;
			}
			if(password.getText().toString().equals("")){
				Toast.makeText(mContext, "密码不能为空！", Toast.LENGTH_SHORT).show();
				return;
			}
			
			show("");
			
			RequestParams params=new RequestParams();
			params.add("mobi", account.getText().toString());
			params.add("password", password.getText().toString());
			HttpTool.HttpPost(0, mContext, ApplicationConstants.SERVER_URL+"/mobi/account/log", params, this, new TypeToken<BaseResult<UserInfo>>() {
			}.getType());
			
			break;
		case R.id.register:	
			Intent in2=new Intent(mContext, RegisterActivity.class);
			startActivity(in2);
			
			break;
		case R.id.forget:
			Intent in3=new Intent(mContext, ForgetPasswordActivity.class);
			startActivity(in3);
			
			break;
			
		}
	}

	@Override
	public void onResponse(int requsetCode, boolean resultStatus, Object result) {
		// TODO Auto-generated method stub
		switch(requsetCode){
		case 0:
			disMiss();
			BaseResult<UserInfo> result0=(BaseResult<UserInfo>) result;
			if(resultStatus){
				SharedPreferences settings = getSharedPreferences("fbb", 0);
				SharedPreferences.Editor editor = settings.edit();
				editor.putString("name", account.getText().toString());
				editor.putString("password", password.getText().toString());
				
				//editor.putString("userinfo", new Gson().toJson(result0.getResult()));
				
				editor.commit();
				
				if(result0.getResult().getType()!=3){
					AreaInfo info=new AreaInfo();
					info.setName(result0.getResult().getCityName());
					info.setParentId(result0.getResult().getDistrictId());//城市id
					ApplicationCache.INSTANCE.setAreaInfo(info);
				}
				
				ApplicationCache.INSTANCE.setUserInfo(result0.getResult());
				
				Intent in=new Intent(mContext, HomeActivity.class);
				startActivity(in);
				
			}else{
				switch(result0.getCode()){
				//服务器网络系统异常  -2密码错误  -3账号不存在  -4账号冻结了
				case -1:
					Toast.makeText(mContext, "服务器网络系统异常！", Toast.LENGTH_SHORT).show();
					break;
				case -2:
					Toast.makeText(mContext, "密码错误！", Toast.LENGTH_SHORT).show();
					break;
				case -3:
					Toast.makeText(mContext, "账号不存在！", Toast.LENGTH_SHORT).show();
					break;
				case -4:
					Toast.makeText(mContext, "账号冻结了！", Toast.LENGTH_SHORT).show();
					break;
				}
				
			}
			break;
		}
	}

	@Override
	public void onFailure() {
		// TODO Auto-generated method stub
		disMiss();
	}
	

	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		// TODO Auto-generated method stub

		if (keyCode == KeyEvent.KEYCODE_BACK&& event.getAction() == KeyEvent.ACTION_DOWN) {
			//回桌面
			Intent i = new Intent(Intent.ACTION_MAIN); 
			i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK); 
			i.addCategory(Intent.CATEGORY_HOME); 
			startActivity(i);
			return true;
		}
		return super.onKeyDown(keyCode, event);
	}



	
}
