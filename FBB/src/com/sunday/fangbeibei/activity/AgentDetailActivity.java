package com.sunday.fangbeibei.activity;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.reflect.TypeToken;
import com.loopj.android.http.RequestParams;
import com.sunday.fangbeibei.R;
import com.sunday.fangbeibei.cache.ApplicationCache;
import com.sunday.fangbeibei.constants.ApplicationConstants;
import com.sunday.fangbeibei.http.HttpResponseInterface;
import com.sunday.fangbeibei.http.HttpTool;
import com.sunday.fangbeibei.orm.AgentInfo;
import com.sunday.fangbeibei.orm.BaseResult;

/**
 * 

* @ClassName: AgentDetailActivity 

* @Description: TODO(经纪人详情) 

* @date 2015年2月12日 下午6:15:30 

*
 */
public class AgentDetailActivity extends BaseActivity implements OnClickListener,HttpResponseInterface{

	private AgentInfo info;
	
	private TextView custom;
	private TextView mount;
	
	@Override
	protected void createActivity(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		setContentView(R.layout.agent_detail_layout);
		info=(AgentInfo) getIntent().getSerializableExtra("info");
		loadView();
		loadData();
	}

	private void loadView(){
		
		TextView title=(TextView) findViewById(R.id.title);
		ImageView back=(ImageView) findViewById(R.id.back);
		
		title.setText("经纪人详情");
		
		back.setOnClickListener(this);
		
		custom=(TextView) findViewById(R.id.custom);
		mount=(TextView) findViewById(R.id.mount);
		
		TextView name=(TextView) findViewById(R.id.name);
		TextView company=(TextView) findViewById(R.id.company);
		TextView mobile=(TextView) findViewById(R.id.mobile);
			
		LinearLayout ll1=(LinearLayout) findViewById(R.id.ll1);
		
		LinearLayout customll=(LinearLayout) findViewById(R.id.customll);
		LinearLayout mountll=(LinearLayout) findViewById(R.id.mountll);
		
		customll.setOnClickListener(this);
		mountll.setOnClickListener(this);
		
		TextView stop=(TextView) findViewById(R.id.stop);
		TextView pass=(TextView) findViewById(R.id.pass);
		
		ImageView call=(ImageView) findViewById(R.id.call);
		ImageView sms=(ImageView) findViewById(R.id.sms);
		
		if(info==null){
			return;
		}
		
		call.setOnClickListener(this);
		sms.setOnClickListener(this);
		
		name.setText(info.getName());
		if(info.getStoreName()!=null){
			company.setText(info.getStoreName());
		}
		mobile.setText(info.getMobi());
		int status=info.getStatus();
		if(status==1){
			ll1.setVisibility(View.GONE);
			stop.setVisibility(View.GONE);
			pass.setOnClickListener(this);
		}else if(status==2||status==3){
			pass.setVisibility(View.GONE);
			stop.setOnClickListener(this);
		}

		if(status==3){
			stop.setText("启用该账号");
		}
		
	}
	

	private void loadData(){
		
		RequestParams params=new RequestParams();
		params.add("userId", info.getId());
		HttpTool.HttpPost(2, mContext, ApplicationConstants.SERVER_URL+"/mobi/account/userdetail", params, this, new TypeToken<BaseResult<AgentInfo>>() {
		}.getType());
		
	}
	
	

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		switch(v.getId()){
		case R.id.back:
			finish();
			break;
		case R.id.pass:
			show("");
			RequestParams params=new RequestParams();
			params.add("userId", ApplicationCache.INSTANCE.getUserInfo().getId());
			params.add("requestId", info.getId());
			HttpTool.HttpPost(0, mContext, ApplicationConstants.SERVER_URL+"/mobi/account/confirmrequest", params, this, new TypeToken<BaseResult>() {
			}.getType());
			
			break;
		case R.id.stop:
			show("");
			RequestParams params2=new RequestParams();
			params2.add("userId", ApplicationCache.INSTANCE.getUserInfo().getId());
			params2.add("requestId", info.getId());
			HttpTool.HttpPost(1, mContext, ApplicationConstants.SERVER_URL+"/mobi/account/forzenuser", params2, this, new TypeToken<BaseResult>() {
			}.getType());
			
			break;
		case R.id.call:
		    Intent intent = new Intent(Intent.ACTION_DIAL,Uri.parse("tel:"+info.getMobi()));
		    mContext.startActivity(intent);
			break;
		case R.id.sms:
			 Intent intent2 = new Intent();
	         intent2.setAction(Intent.ACTION_SENDTO);
	         intent2.setData(Uri.parse("smsto:"+info.getMobi()));
	         //intent2.putExtra("sms_body", "发短信");
	         mContext.startActivity(intent2);
			break;
		case R.id.customll:
			Intent intent3=new Intent(mContext, CustomListActivity.class);
			intent3.putExtra("id", info.getId());
			startActivity(intent3);
			break;
		case R.id.mountll:
			Intent intent4=new Intent(mContext, VolumeActivity.class);
			intent4.putExtra("id", info.getId());
			startActivity(intent4);
			break;
		}
	}

	@Override
	public void onResponse(int requsetCode, boolean resultStatus, Object result) {
		// TODO Auto-generated method stub
		disMiss();
		switch(requsetCode){
		case 0:
			if(resultStatus){
				Toast.makeText(mContext, "您已成功审核该账号！", Toast.LENGTH_SHORT).show();
				finish();
			}else{
				BaseResult re=(BaseResult) result;
				switch(re.getCode()){
				case -1:
					Toast.makeText(mContext, "系统异常！", Toast.LENGTH_SHORT).show();
					break;
				case -2:
					Toast.makeText(mContext, "无此用户！", Toast.LENGTH_SHORT).show();
					break;
				case -3:
					Toast.makeText(mContext, "没有权限！", Toast.LENGTH_SHORT).show();
					break;
				}
			}
			break;
		case 1:
			if(resultStatus){
				if(info.getStatus()==2){
					Toast.makeText(mContext, "您已成功冻结该账号！", Toast.LENGTH_SHORT).show();
				}else if(info.getStatus()==3){
					Toast.makeText(mContext, "您已成功启用该账号！", Toast.LENGTH_SHORT).show();
				}
				finish();
			}else{
				BaseResult re=(BaseResult) result;
				switch(re.getCode()){
				case -1:
					Toast.makeText(mContext, "系统异常！", Toast.LENGTH_SHORT).show();
					break;
				case -2:
					Toast.makeText(mContext, "无此用户！", Toast.LENGTH_SHORT).show();
					break;
				case -3:
					Toast.makeText(mContext, "没有权限！", Toast.LENGTH_SHORT).show();
					break;
				}
			}
			break;
		case 2:
			if(resultStatus){
				BaseResult<AgentInfo> result2=(BaseResult<AgentInfo>) result;
				custom.setText(result2.getResult().getReportcount());
				mount.setText(result2.getResult().getDealcount());
			}else{
				
			}
			break;
		}
	}
	@Override
	public void onFailure() {
		// TODO Auto-generated method stub
		disMiss();
	}

	
}
