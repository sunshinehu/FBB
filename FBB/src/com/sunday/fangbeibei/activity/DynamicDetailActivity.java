package com.sunday.fangbeibei.activity;

import java.util.ArrayList;
import java.util.List;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.RelativeLayout.LayoutParams;
import android.widget.TextView;
import android.widget.Toast;
import cn.sharesdk.framework.ShareSDK;
import cn.sharesdk.onekeyshare.OnekeyShare;

import com.google.gson.reflect.TypeToken;
import com.loopj.android.http.RequestParams;
import com.sunday.fangbeibei.R;
import com.sunday.fangbeibei.adapter.DynamicAdapter;
import com.sunday.fangbeibei.adapter.DynamicAdapter.OnButtonClickListener;
import com.sunday.fangbeibei.cache.ApplicationCache;
import com.sunday.fangbeibei.constants.ApplicationConstants;
import com.sunday.fangbeibei.http.HttpResponseInterface;
import com.sunday.fangbeibei.http.HttpTool;
import com.sunday.fangbeibei.orm.BaseResult;
import com.sunday.fangbeibei.orm.BlogInfo;
import com.sunday.fangbeibei.orm.BlogReply;
import com.sunday.fangbeibei.orm.BlogUser;

/**
 * 动态详情
 * @author SunshineHu
 *
 */
public class DynamicDetailActivity extends BaseActivity implements OnClickListener,HttpResponseInterface,OnButtonClickListener{

	
	private ListView listview;
	
	private DynamicAdapter adapter;

	private RelativeLayout bottom;
	private EditText comment;
	private TextView btn;
	
	private BlogInfo blogInfo;
	
	
	@Override
	protected void createActivity(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		
		setContentView(R.layout.dynamic_detail_layout);
		loadView();
		loadData();
	}

	
	private void loadView(){
		
		
		TextView title=(TextView) findViewById(R.id.title);
		title.setText("动态");
		ImageView back=(ImageView) findViewById(R.id.back);
		back.setOnClickListener(this);
		
		listview=(ListView) findViewById(R.id.listview);
		
		
		
		
		LayoutInflater inflater=LayoutInflater.from(this);
		bottom=(RelativeLayout) inflater.inflate(R.layout.reply_box, null);
		
		RelativeLayout parentrl=(RelativeLayout) this.findViewById(R.id.parentrl);
		RelativeLayout.LayoutParams lp = new RelativeLayout.LayoutParams(LayoutParams.WRAP_CONTENT,LayoutParams.WRAP_CONTENT);
		lp.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM);
		bottom.setLayoutParams(lp);
		parentrl.addView(bottom);
		bottom.setVisibility(View.GONE);
		
	}
	

	private void loadData(){
		
		String id=getIntent().getStringExtra("id");
		
		RequestParams params=new RequestParams();
		params.add("userId", ApplicationCache.INSTANCE.getUserInfo().getId());
		params.add("blogId", id);
		HttpTool.HttpPost(0, mContext, ApplicationConstants.SERVER_URL+"/mobi/blog/getByBlogId", params, this, new TypeToken<BaseResult<BlogInfo>>() {
		}.getType());
	}
	
	

	
	private void loadInput(){
		
		bottom.setVisibility(View.VISIBLE);
		comment=(EditText) bottom.findViewById(R.id.et_sendmessage);
		btn=(TextView) bottom.findViewById(R.id.btn_send);
		comment.setFocusable(true);
		comment.setFocusableInTouchMode(true);
		comment.requestFocus();
		InputMethodManager inputManager =
                    (InputMethodManager)comment.getContext().getSystemService(this.INPUT_METHOD_SERVICE);
        inputManager.showSoftInput(comment, 0);
        btn.setOnClickListener(this);
	}
	
	
	
	

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		switch(v.getId()){
		
		case R.id.back:
			finish();
			break;
		case R.id.btn_send:
			
			RequestParams params=new RequestParams();
			params.add("memberId", ApplicationCache.INSTANCE.getUserInfo().getId());
			params.add("blogId", getIntent().getStringExtra("id"));
			params.add("content", comment.getText().toString());
			HttpTool.HttpPost(2, this, ApplicationConstants.SERVER_URL+"/mobi/blog/comment", params, this, new TypeToken<BaseResult>() {
			}.getType());
			
			break;
		}
	}


	@Override
	public void onResponse(int requsetCode, boolean resultStatus, Object result) {
		// TODO Auto-generated method stub
		switch(requsetCode){
		case 0:
			BaseResult<BlogInfo> result1=(BaseResult<BlogInfo>) result;
			if(resultStatus){
				if(result1.getResult()==null){
					Toast.makeText(this, "该动态不存在！", Toast.LENGTH_SHORT).show();
				}else{
					blogInfo=result1.getResult();
					List<BlogInfo> list=new ArrayList<BlogInfo>();
					list.add(blogInfo);
					adapter=new DynamicAdapter(this,list);
					adapter.setOnButtonClickListener(this);
					listview.setAdapter(adapter);
				}
			}
			break;
		case 1:
			BaseResult result2=(BaseResult) result;
			if(resultStatus){
				//Toast.makeText(getActivity(), "操作成功！", Toast.LENGTH_SHORT).show();
				List<BlogUser> users=blogInfo.getFavUser();
				if(result2.getCode()==0){
					//点赞
					BlogUser user=new BlogUser();
					user.setMemberId(ApplicationCache.INSTANCE.getUserInfo().getId());
					user.setMemberName(ApplicationCache.INSTANCE.getUserInfo().getName());
					users.add(user);
					adapter.notifyDataSetChanged();
				}else if(result2.getCode()==1){
					//取消赞
					for(BlogUser user:users){
						if(user.getMemberId().equals(ApplicationCache.INSTANCE.getUserInfo().getId())){
							users.remove(user);
						}
					}
					adapter.notifyDataSetChanged();
				}
			}
			break;
		case 2:
			if(resultStatus){
				bottom.setVisibility(View.INVISIBLE);
				Toast.makeText(this, "评论成功！", Toast.LENGTH_SHORT).show();
				InputMethodManager inputManager = (InputMethodManager)comment.getContext().getSystemService(this.INPUT_METHOD_SERVICE);
				inputManager.hideSoftInputFromWindow(comment.getWindowToken(), 0);
				BlogReply reply=new BlogReply();
				reply.setContent(comment.getText().toString());
				reply.setMemberName(ApplicationCache.INSTANCE.getUserInfo().getName());
				reply.setMemberId(ApplicationCache.INSTANCE.getUserInfo().getId());
				blogInfo.getReplys().add(reply);
				adapter.notifyDataSetChanged();
				comment.setText("");
			}
			break;
		}
	}


	@Override
	public void onFailure() {
		// TODO Auto-generated method stub
		
	}


	//点赞
	@Override
	public void praise(int position) {
		// TODO Auto-generated method stub
		RequestParams params=new RequestParams();
		params.add("memberId", ApplicationCache.INSTANCE.getUserInfo().getId());
		params.add("objId", blogInfo.getBlogId());
		HttpTool.HttpPost(1, this, ApplicationConstants.SERVER_URL+"/mobi/blog/good", params, this, new TypeToken<BaseResult>() {
		}.getType());
	}
	
	@Override
	public void comment(int position) {
		// TODO Auto-generated method stub
		loadInput();
		
	}
	
	@Override
	public void share(int position) {
		// TODO Auto-generated method stub
		
		String id=blogInfo.getBlogId();
		String url=ApplicationConstants.SERVER_URL+"/mobi/blog/share?blogId="+id;
		
		showShare(url);

	}
	
	



	private void showShare(String url){
	   ShareSDK.initSDK(this);
	   OnekeyShare oks = new OnekeyShare(); 
	   // 分享时Notification的图标和文字
	   oks.setNotification(R.drawable.icon, getString(R.string.app_name));
	   oks.setText(url);
	   oks.setDialogMode();
	   // 启动分享GUI
	   oks.show(this);
	}
	
	
	
}
