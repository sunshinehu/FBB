package com.sunday.fangbeibei.adapter;

import java.util.List;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.sunday.fangbeibei.R;
import com.sunday.fangbeibei.orm.MessageInfo;

/**
 * 消息适配器
 * @author SunshineHu
 *
 */
public class MessageAdapter extends BaseAdapter{

	private List<MessageInfo> mylist;
	private LayoutInflater inflater;
	
	public MessageAdapter(Context c,List<MessageInfo> list) {
		// TODO Auto-generated constructor stub
		inflater=LayoutInflater.from(c);
		mylist=list;
	}
	
	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return mylist.size();
	}

	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		if(convertView==null){
			convertView=inflater.inflate(R.layout.message_adapter, null);
		}
		TextView title=(TextView) convertView.findViewById(R.id.title);
		TextView time=(TextView) convertView.findViewById(R.id.time);
		TextView content=(TextView) convertView.findViewById(R.id.content);
		title.setText(mylist.get(position).getTitle());
		time.setText(mylist.get(position).getCreateTime());
		content.setText(mylist.get(position).getContent());
		return convertView;
	}

}
