package com.sunday.fangbeibei.adapter;

import java.util.List;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.sunday.fangbeibei.R;
import com.sunday.fangbeibei.orm.HouseInfo;

/**
 * 
 * @author 晨曦
 *
 */
public class RecordHouseAdapter extends BaseAdapter{

	private LayoutInflater inflater;
	private List<HouseInfo> mylist;
	private Context mContext;
	
	public RecordHouseAdapter(Context c,List<HouseInfo> list) {
		// TODO Auto-generated constructor stub
		inflater=LayoutInflater.from(c);
		mContext=c;
		mylist=list;
	}
	
	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return mylist.size();
	}

	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		if(convertView==null){
			convertView=inflater.inflate(R.layout.record_house_adapter, null);
		}
		TextView name=(TextView) convertView.findViewById(R.id.name);
		TextView status=(TextView) convertView.findViewById(R.id.status);
		name.setText(mylist.get(position).getName());
		if(mylist.get(position).getIsReported()==1){
			status.setVisibility(View.VISIBLE);
		}else{
			status.setVisibility(View.INVISIBLE);
		}
		convertView.setTag(mylist.get(position));
		return convertView;
	}

}
