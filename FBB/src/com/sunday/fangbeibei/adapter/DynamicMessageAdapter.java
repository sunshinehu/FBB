package com.sunday.fangbeibei.adapter;

import java.util.List;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.sunday.fangbeibei.R;
import com.sunday.fangbeibei.orm.DynamicMessage;

/**
 * 
 * @author 晨曦
 *
 */
public class DynamicMessageAdapter extends BaseAdapter{

	private LayoutInflater inflater;
	private List<DynamicMessage> mylist;

	private DisplayImageOptions option;
	
	public DynamicMessageAdapter(Context c,List<DynamicMessage> list) {
		// TODO Auto-generated constructor stub
		mylist=list;
		inflater=LayoutInflater.from(c);
		option=new DisplayImageOptions.Builder().cacheInMemory(true)
				.cacheOnDisc(true)
				.build();
	}
	
	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return mylist.size();
	}

	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		if(convertView==null){
			convertView=inflater.inflate(R.layout.dynamic_message_adapter, null);
		}
		
		ImageView logo=(ImageView) convertView.findViewById(R.id.logo);
		ImageView praise=(ImageView) convertView.findViewById(R.id.praise);
		
		TextView name=(TextView) convertView.findViewById(R.id.name);
		TextView comment=(TextView) convertView.findViewById(R.id.comment);
		TextView time=(TextView) convertView.findViewById(R.id.time);
		
		name.setText(mylist.get(position).getBlogReply().getMemberName());
		if(mylist.get(position).getBlog().getImage()!=null&&!mylist.get(position).getBlog().getImage().equals("")){
			ImageLoader.getInstance().displayImage(mylist.get(position).getBlog().getImage(), logo,option);
		}
		
		if(mylist.get(position).getBlogReply().getStatus()!=null&&mylist.get(position).getBlogReply().getStatus().equals("5")){
			praise.setVisibility(View.VISIBLE);
			comment.setText("");
		}else{
			praise.setVisibility(View.GONE);
			comment.setText(mylist.get(position).getBlogReply().getContent());
		}
		time.setText(mylist.get(position).getBlogReply().getCreateTime());
		
		convertView.setTag(mylist.get(position).getBlog().getId());
		
		return convertView;
	}

}
