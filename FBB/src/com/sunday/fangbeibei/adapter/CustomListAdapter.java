package com.sunday.fangbeibei.adapter;

import java.util.List;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.sunday.fangbeibei.R;
import com.sunday.fangbeibei.orm.CustomInfo;

/**
 * 经纪人管理报备列表
 * @author 晨曦
 *
 */
public class CustomListAdapter extends BaseAdapter{

	private LayoutInflater inflater;
	private List<CustomInfo> mylist;
	
	public CustomListAdapter(Context c,List<CustomInfo> list) {
		// TODO Auto-generated constructor stub
		mylist=list;
		inflater=LayoutInflater.from(c);
	}
	
	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return mylist.size();
	}

	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		if(convertView==null){
			convertView=inflater.inflate(R.layout.custom_list_adapter, null);
		}
		TextView name=(TextView) convertView.findViewById(R.id.name);
		TextView phone=(TextView) convertView.findViewById(R.id.phone);
		TextView house=(TextView) convertView.findViewById(R.id.house);
		name.setText(mylist.get(position).getName());
		phone.setText(mylist.get(position).getMobile());
		house.setText(mylist.get(position).getPremisesName());
		
		convertView.setTag(mylist.get(position));
		
		return convertView;
	}

}
