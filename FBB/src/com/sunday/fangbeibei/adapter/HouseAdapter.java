package com.sunday.fangbeibei.adapter;

import java.util.List;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.sunday.fangbeibei.R;
import com.sunday.fangbeibei.activity.OrderActivity;
import com.sunday.fangbeibei.activity.RecordCustomActivity;
import com.sunday.fangbeibei.cache.ApplicationCache;
import com.sunday.fangbeibei.orm.HouseInfo;

/**
 * 

* @ClassName: HouseAdapter 

* @Description: TODO(这里用一句话描述这个类的作用) 

* @author sunshineHu hucx@itouch.com.cn

* @date 2015年2月5日 下午5:28:28 

*
 */
public class HouseAdapter extends BaseAdapter implements OnClickListener{

	private LayoutInflater inflater;
	private DisplayImageOptions option;
	private List<HouseInfo> mylist;
	private Context mContext;
	
	public HouseAdapter(Context c,List<HouseInfo> list) {
		// TODO Auto-generated constructor stub
		mylist=list;
		inflater=LayoutInflater.from(c);
		mContext=c;
		option = new DisplayImageOptions.Builder()
        .cacheInMemory(true)// 是否緩存都內存中  
        .cacheOnDisc(true).build();
	}
	
	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return mylist.size();
	}

	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		if(convertView==null){
			convertView=inflater.inflate(R.layout.house_adapter,null);
		}
		ImageView iv1=(ImageView) convertView.findViewById(R.id.iv1);
		TextView tv1=(TextView) convertView.findViewById(R.id.tv1);
		TextView tv2=(TextView) convertView.findViewById(R.id.tv2);
		TextView tv3=(TextView) convertView.findViewById(R.id.tv3);
		TextView tv4=(TextView) convertView.findViewById(R.id.tv4);
		TextView tv5=(TextView) convertView.findViewById(R.id.tv5);
		TextView tv6=(TextView) convertView.findViewById(R.id.tv6);
		TextView tv7=(TextView) convertView.findViewById(R.id.tv7);
		TextView tv8=(TextView) convertView.findViewById(R.id.tv8);
		TextView tv9=(TextView) convertView.findViewById(R.id.tv9);
		TextView tv10=(TextView) convertView.findViewById(R.id.tv10);
		
		ImageView favor=(ImageView) convertView.findViewById(R.id.favor);
		
		WindowManager wm = (WindowManager) mContext.getSystemService(Context.WINDOW_SERVICE);
		iv1.setLayoutParams(new RelativeLayout.LayoutParams(android.widget.RelativeLayout.LayoutParams.MATCH_PARENT, wm.getDefaultDisplay().getWidth()*140/320));
		
		
		ImageLoader.getInstance().displayImage(mylist.get(position).getImage(), iv1, option);
		tv3.setText(mylist.get(position).getName());
		tv1.setText(mylist.get(position).getCommission());
		if(mylist.get(position).getRewardremark()==null){
			mylist.get(position).setRewardremark("");
		}
		if(mylist.get(position).getPrice()==null){
			mylist.get(position).setPrice("");
		}
		tv4.setText(mylist.get(position).getPrice()+"元/米");
		tv5.setText(" "+mylist.get(position).getReward());
		
		tv10.setOnClickListener(this);
		tv10.setTag(mylist.get(position));
		
		int type=ApplicationCache.INSTANCE.getUserInfo().getType();
		if(type==1){
			tv1.setVisibility(View.VISIBLE);
			tv2.setVisibility(View.VISIBLE);
			tv8.setText("合作经纪人");
			tv9.setText("意向客户");
			tv10.setText("+客户");
			tv6.setText(mylist.get(position).getManageramount());
			tv7.setText(mylist.get(position).getCustomeramount());
		}else if(type==2){
			tv1.setVisibility(View.INVISIBLE);
			tv2.setVisibility(View.INVISIBLE);
			tv8.setText("合作经纪人");
			tv9.setText("意向客户");
			tv10.setText("+客户");
			tv6.setText(mylist.get(position).getManageramount());
			tv7.setText(mylist.get(position).getCustomeramount());
		}else if(type==3){
			tv1.setVisibility(View.INVISIBLE);
			tv2.setVisibility(View.INVISIBLE);
			tv5.setVisibility(View.GONE);
			tv8.setText(mylist.get(position).getActivity());
			tv9.setText(mylist.get(position).getBenefit());
			tv10.setText("我要看房");
			tv6.setText("活动");
			tv7.setText("优惠");
		}
		
		if(mylist.get(position).getIsfav()==0){
			favor.setVisibility(View.GONE);
		}else{
			favor.setVisibility(View.VISIBLE);
		}
		
		convertView.setTag(mylist.get(position));
		
		return convertView;
	}

	public void updateData(List<HouseInfo> lists){
		
		mylist.addAll(lists);
		
	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		if(ApplicationCache.INSTANCE.getUserInfo().getType()==3){
			Intent in=new Intent(mContext, OrderActivity.class);
			HouseInfo info=(HouseInfo) v.getTag();
			in.putExtra("id", info.getId());
			mContext.startActivity(in);
		}else{
			Intent in=new Intent(mContext, RecordCustomActivity.class);
			HouseInfo info=(HouseInfo) v.getTag();
			in.putExtra("info", info);
			mContext.startActivity(in);
		}
	}
	
}
