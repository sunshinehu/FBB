package com.sunday.fangbeibei.adapter;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import android.content.Context;
import android.util.SparseArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.sunday.fangbeibei.R;
import com.sunday.fangbeibei.orm.RecordHouse;
import com.sunday.fangbeibei.orm.RecordInfo;

/**
 * 楼盘详情
 * @author 晨曦
 *
 */
public class HouseRecordAdapter extends BaseAdapter{

	private List<RecordHouse> mylist;
	private Context mContext;
	private LayoutInflater inflater;
	private OnClickListener listener;

	private SparseArray<String> array;
	
	public HouseRecordAdapter(Context c,List<RecordHouse> list,OnClickListener lis) {
		// TODO Auto-generated constructor stub
		mContext=c;
		mylist=list;
		inflater=LayoutInflater.from(c);
		listener=lis;
		array=new SparseArray<String>();
		array.put(1, "已报备");
		array.put(2, "申请带看");
		array.put(3, "已带看");
		array.put(4, "已成交");
		array.put(5, "已结佣");
		array.put(0, "未报备");
	}
	
	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return mylist.size();
	}

	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		
		convertView=inflater.inflate(R.layout.house_record_adapter, null);
		
		ImageView iv1=(ImageView) convertView.findViewById(R.id.iv1);
		ImageView iv2=(ImageView) convertView.findViewById(R.id.iv2);
		ImageView iv3=(ImageView) convertView.findViewById(R.id.iv3);
		ImageView iv4=(ImageView) convertView.findViewById(R.id.iv4);
		ImageView iv5=(ImageView) convertView.findViewById(R.id.iv5);
		ImageView iv6=(ImageView) convertView.findViewById(R.id.iv6);
		ImageView iv7=(ImageView) convertView.findViewById(R.id.iv7);
	
		TextView name=(TextView) convertView.findViewById(R.id.name);
		
		TextView btn=(TextView) convertView.findViewById(R.id.btn);
		
		LinearLayout recordll=(LinearLayout) convertView.findViewById(R.id.recordll);
		
		ImageView more=(ImageView) convertView.findViewById(R.id.more);
		
		RecordHouse item=mylist.get(position);

		List<RecordInfo> rlist=item.getRecords();
		
		if(rlist!=null&&rlist.size()>1){
			recordll.removeAllViews();
			int size=rlist.size();
			if(item.isFlag()){
				more.setImageDrawable(mContext.getResources().getDrawable(R.drawable.uptri));
				for(int i=size-1;i>=0;i--){
					
					if(rlist.get(i).getStatus()==2){
						//处于带看状态
						View view=inflater.inflate(R.layout.house_order_item, null);
						TextView tv1=(TextView) view.findViewById(R.id.name);
						TextView tv2=(TextView) view.findViewById(R.id.content);
						ImageView iv=(ImageView) view.findViewById(R.id.callphone);
						
						Date date = new Date(mylist.get(position).getCt());
						SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd HH:mm");
						
						iv.setOnClickListener(listener);
						iv.setTag(mylist.get(position).getHeaderMobi());
						
						tv1.setText("确定预约经纪人("+sdf.format(date)+")");
						tv2.setText("房贝贝"+mylist.get(position).getHeaderName()+"为您提供服务");
						recordll.addView(view);
					}
					
					View view=inflater.inflate(R.layout.house_record_item, null);
					TextView tv=(TextView) view.findViewById(R.id.name);
					tv.setText(array.get(rlist.get(i).getStatus())+"("+rlist.get(i).getCreatTime()+")");
					recordll.addView(view);
				}
			}else{
				more.setImageDrawable(mContext.getResources().getDrawable(R.drawable.downtri));
				View view=inflater.inflate(R.layout.house_record_item, null);
				TextView tv=(TextView) view.findViewById(R.id.name);
				tv.setText(array.get(rlist.get(size-1).getStatus())+"("+rlist.get(size-1).getCreatTime()+")");
				recordll.addView(view);
			}
			more.setOnClickListener(listener);
			more.setTag(item);
		}else{
			if(rlist.size()==1){
				View view=inflater.inflate(R.layout.house_record_item, null);
				TextView tv=(TextView) view.findViewById(R.id.name);
				tv.setText(array.get(rlist.get(0).getStatus())+"("+rlist.get(0).getCreatTime()+")");
				recordll.addView(view);
			}
			more.setVisibility(View.INVISIBLE);
		}
		
		name.setText(item.getPremisesName());
		
		if(item.getStatus()==3){
			iv2.setImageDrawable(mContext.getResources().getDrawable(R.drawable.o_btn));
			iv5.setBackgroundDrawable(mContext.getResources().getDrawable(R.drawable.bar2));
		}else if(item.getStatus()==4){
			iv2.setImageDrawable(mContext.getResources().getDrawable(R.drawable.o_btn));
			iv3.setImageDrawable(mContext.getResources().getDrawable(R.drawable.p_btn));
			iv5.setBackgroundDrawable(mContext.getResources().getDrawable(R.drawable.bar2));
			iv6.setBackgroundDrawable(mContext.getResources().getDrawable(R.drawable.bar3));
		}else if(item.getStatus()==5){
			iv2.setImageDrawable(mContext.getResources().getDrawable(R.drawable.o_btn));
			iv3.setImageDrawable(mContext.getResources().getDrawable(R.drawable.p_btn));
			iv4.setImageDrawable(mContext.getResources().getDrawable(R.drawable.r_btn));
			iv5.setBackgroundDrawable(mContext.getResources().getDrawable(R.drawable.bar2));
			iv6.setBackgroundDrawable(mContext.getResources().getDrawable(R.drawable.bar3));
			iv7.setBackgroundDrawable(mContext.getResources().getDrawable(R.drawable.bar4));
			
		}
		
		if(item.getStatus()==1){
			btn.setVisibility(View.VISIBLE);
			btn.setTag(mylist.get(position));
			btn.setOnClickListener(listener);
		}else{
			btn.setVisibility(View.INVISIBLE);
		}
		
		return convertView;
	}

}
