package com.sunday.fangbeibei.adapter;

import android.app.Activity;
import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView.LayoutParams;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ImageView.ScaleType;

import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;

public class DynamicImageAdapter extends BaseAdapter{

	private String[] mylist;
	private Context mContext;
	private DisplayImageOptions option;
	
	public DynamicImageAdapter(String[] list,Context c) {
		// TODO Auto-generated constructor stub
		mylist=list;
		mContext=c;
		option=new DisplayImageOptions.Builder().cacheInMemory(true)
				.cacheOnDisc(true)
				.build();
	}
	
	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return mylist.length;
	}

	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		ImageView iv;
		if(convertView==null){
			iv=new ImageView(mContext);
		}else{
			iv=(ImageView) convertView;
		}
		iv.setLayoutParams(new LayoutParams(LayoutParams.MATCH_PARENT, (int) ((Activity)mContext).getWindowManager().getDefaultDisplay().getWidth()/4));
		iv.setScaleType(ScaleType.CENTER_CROP);
		ImageLoader.getInstance().displayImage(mylist[position], iv,option);
		return iv;
	}

	
	
}
