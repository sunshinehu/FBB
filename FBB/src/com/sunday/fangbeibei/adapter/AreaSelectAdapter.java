package com.sunday.fangbeibei.adapter;

import java.util.List;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.sunday.fangbeibei.R;
import com.sunday.fangbeibei.orm.AreaInfo;

/**
 * 

* @ClassName: AreaSelectAdapter 

* @Description: TODO(区域选择adapter) 

* @author sunshineHu hucx@itouch.com.cn

* @date 2015年1月4日 下午12:44:17 

*
 */
public class AreaSelectAdapter extends BaseAdapter{

	private List<AreaInfo> mylist;
	private LayoutInflater inflater;
	private int checked=-1;
	
	private Context mContext;
	
	public AreaSelectAdapter(Context c,List<AreaInfo> lists){
		
		mContext=c;
		mylist=lists;
		inflater=LayoutInflater.from(c);
		
	}
	
	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return mylist.size();
	}

	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		TextView tv;
		if(convertView==null){
			convertView=inflater.inflate(R.layout.area_adapter, null);
		}
		tv=(TextView) convertView.findViewById(R.id.area);
		tv.setText(mylist.get(position).getName());
		tv.setTextColor(Color.BLACK);
		if(checked==position){
			tv.setBackgroundColor(mContext.getResources().getColor(R.color.grey));
		}else{
			tv.setBackgroundColor(Color.TRANSPARENT);
		}
		convertView.setTag(mylist.get(position));
		
		return convertView;
	}

	
	public void setChecked(int pos){
		
		checked=pos;
		
	}
	
}
