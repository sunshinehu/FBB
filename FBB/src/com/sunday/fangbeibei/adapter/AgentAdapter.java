package com.sunday.fangbeibei.adapter;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.sunday.fangbeibei.R;
import com.sunday.fangbeibei.orm.AgentInfo;

/**
 * 

* @ClassName: AgentAdapter 

* @Description: TODO(经纪人列表) 

* @author sunshineHu hucx@itouch.com.cn

* @date 2015年2月12日 下午5:18:29 

*
 */
public class AgentAdapter extends BaseAdapter{

	private List<AgentInfo> mylist;
	private LayoutInflater inflater;
	private Context mContext;
	
	public AgentAdapter(Context c,List<AgentInfo> list) {
		// TODO Auto-generated constructor stub
		mylist=list;
		inflater=LayoutInflater.from(c);
		mContext=c;
	}
	
	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return mylist.size();
	}

	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		if(convertView==null){
			convertView=inflater.inflate(R.layout.agent_adapter, null);
		}
		TextView name=(TextView) convertView.findViewById(R.id.name);
		TextView mobile=(TextView) convertView.findViewById(R.id.mobile);
		TextView time=(TextView) convertView.findViewById(R.id.time);
		TextView status=(TextView) convertView.findViewById(R.id.status);
		
		AgentInfo info=mylist.get(position);
		if(info.getStatus()!=null){
			if(info.getStatus()==1){
				status.setText("待审核");
				status.setTextColor(mContext.getResources().getColor(R.color.red));
			}else if(info.getStatus()==2){
				status.setText("审核通过");
				status.setTextColor(mContext.getResources().getColor(R.color.green));
			}else if(info.getStatus()==3){
				status.setText("账号已停用");
				status.setTextColor(mContext.getResources().getColor(R.color.darkgrey));
			}
		}else{
			status.setVisibility(View.INVISIBLE);
		}
		name.setText(info.getName());
		mobile.setText(info.getMobi());
		
		Date date=new Date(info.getUt());
		SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd HH:mm");
		
		time.setText(sdf.format(date));
		
		convertView.setTag(info);
		
		return convertView;
		
	}

}
