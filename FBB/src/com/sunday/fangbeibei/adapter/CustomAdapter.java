package com.sunday.fangbeibei.adapter;

import java.util.List;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.sunday.fangbeibei.R;
import com.sunday.fangbeibei.activity.RecordHouseActivity;
import com.sunday.fangbeibei.orm.CustomInfo;

/**
 * 客户adapter
 * @author 晨曦
 *
 */
public class CustomAdapter extends BaseAdapter implements OnClickListener{

	private LayoutInflater inflater;
	private Context mContext;
	private List<CustomInfo> mylist;
	
	public CustomAdapter(Context c,List<CustomInfo> list) {
		// TODO Auto-generated constructor stub
		inflater=LayoutInflater.from(c);
		mContext=c;
		mylist=list;
	}
	
	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return mylist.size();
	}

	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		if(convertView==null){
			convertView=inflater.inflate(R.layout.custom_adapter, null);
		}
		TextView name=(TextView) convertView.findViewById(R.id.name);
		TextView phone=(TextView) convertView.findViewById(R.id.phone);
		TextView content=(TextView) convertView.findViewById(R.id.content);
		
		ImageView call=(ImageView) convertView.findViewById(R.id.call);
		ImageView send=(ImageView) convertView.findViewById(R.id.send);
		
		call.setTag(mylist.get(position));
		send.setTag(mylist.get(position));
		
		call.setOnClickListener(this);
		send.setOnClickListener(this);
		
		name.setText(mylist.get(position).getName());
		phone.setText(mylist.get(position).getMobile());
		if(mylist.get(position).getPremisesName()==null){
			mylist.get(position).setPremisesName("");
		}
		content.setText(mylist.get(position).getPremisesName());
		
		TextView add=(TextView) convertView.findViewById(R.id.add);
		
		
		if(mylist.get(position).getStatus()==0){
			add.setText("报备楼盘");
			add.setTag(mylist.get(position));
			add.setOnClickListener(this);
			add.setBackgroundDrawable(mContext.getResources().getDrawable(R.drawable.add_btn));
		}else{
			add.setBackgroundDrawable(null);
			switch(mylist.get(position).getStatus()){
			case 1:
				add.setText("已报备");
				break;
			case 2:
				add.setText("申请带看");
				break;
			case 3:
				add.setText("已带看");
				break;
			case 4:
				add.setText("已成交");
				break;
			case 5:
				add.setText("已结佣");
				break;
			}
			
		}
		convertView.setTag(mylist.get(position));
		
		return convertView;
	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		CustomInfo info=(CustomInfo) v.getTag();
		switch(v.getId()){
		case R.id.add:
			Intent in=new Intent(mContext, RecordHouseActivity.class);
			in.putExtra("info", info);
			mContext.startActivity(in);
			break;
		case R.id.call:
		    Intent intent = new Intent(Intent.ACTION_DIAL,Uri.parse("tel:"+info.getMobile()));
		    mContext.startActivity(intent);
			break;
		case R.id.send:
			 Intent intent2 = new Intent();
	         intent2.setAction(Intent.ACTION_SENDTO);
	         intent2.setData(Uri.parse("smsto:"+info.getMobile()));
	         //intent2.putExtra("sms_body", "发短信");
	         mContext.startActivity(intent2);
			break;
		}
	}
	
}
