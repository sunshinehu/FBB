package com.sunday.fangbeibei.adapter;

import java.util.List;

import android.app.Activity;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;

import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.sunday.fangbeibei.orm.ImageInfo;
import com.sunday.fangbeibei.widget.ZoomableImageView;

public class ImageBrowseAdapter extends PagerAdapter{
	private Activity mActivity;
	private List<ImageInfo> mylist;
	private DisplayImageOptions option;
	
	
	public ImageBrowseAdapter(Activity c,List<ImageInfo> images){

		mActivity=c;
		mylist=images;

		option = new DisplayImageOptions.Builder()
        .cacheInMemory(true)// 是否緩存都內存中  
        .cacheOnDisc(true).build();
	}
	
	
	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return mylist.size();
	}

	@Override
	public boolean isViewFromObject(View arg0, Object arg1) {
		// TODO Auto-generated method stub
		return arg0==arg1;
	}

	@Override
	public void destroyItem(View container, int position, Object object) {
		// TODO Auto-generated method stub
		((ViewPager) container).removeView((View) object);
	}

	@Override
	public Object instantiateItem(View container, int position) {
		// TODO Auto-generated method stub
		ImageView view=new ZoomableImageView(mActivity);
		view.setLayoutParams(new LinearLayout.LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT));
		ImageLoader.getInstance().displayImage(mylist.get(position).getPath(), view);
	/*	
		View view=inflater.inflate(R.layout.image_browse_adapter, null);
		ImageView iv=(ImageView) view.findViewById(R.id.image);
		ImageLoader.getInstance().displayImage(mylist[position], iv);*/
		((ViewPager)container).addView(view);
		return view;
	}



}
