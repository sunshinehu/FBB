package com.sunday.fangbeibei.adapter;

import java.util.List;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.sunday.fangbeibei.R;
import com.sunday.fangbeibei.orm.VolumeInfo;

/**
 * 成交量
 * @author 晨曦
 *
 */
public class VolumeAdapter extends BaseAdapter{

	private LayoutInflater inflater;
	private List<VolumeInfo> mylist;
	
	public VolumeAdapter(Context c,List<VolumeInfo> list) {
		// TODO Auto-generated constructor stub
		inflater=LayoutInflater.from(c);
		mylist=list;
	}
	
	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return mylist.size();
	}

	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		if(convertView==null){
			convertView=inflater.inflate(R.layout.volume_adapter, null);
		}
		
		TextView name=(TextView) convertView.findViewById(R.id.name);
		TextView house=(TextView) convertView.findViewById(R.id.house);
		TextView time=(TextView) convertView.findViewById(R.id.time);
		TextView tv1=(TextView) convertView.findViewById(R.id.tv1);
		TextView tv2=(TextView) convertView.findViewById(R.id.tv2);
		TextView tv3=(TextView) convertView.findViewById(R.id.tv3);
		
		name.setText(mylist.get(position).getManagerName()+"·");
		house.setText(mylist.get(position).getPremisesName());
		time.setText(mylist.get(position).getCreateTime());
		
		tv1.setText(mylist.get(position).getCommission()+"");
		tv2.setText((mylist.get(position).getCommission()-mylist.get(position).getNopyedcommission())+"");
		tv3.setText(mylist.get(position).getNopyedcommission()+"");
		return convertView;
	}

}
