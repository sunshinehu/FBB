package com.sunday.fangbeibei.adapter;

import java.util.List;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView.LayoutParams;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ImageView.ScaleType;

import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.sunday.fangbeibei.R;

/**
 * 

* @ClassName: ImageAdapter 

* @Description: TODO(这里用一句话描述这个类的作用) 

* @date 2015年2月4日 下午12:47:52 

*
 */
public class ImageAdapter extends BaseAdapter{

	private LayoutInflater inflater;
	private Activity mActivity;
	private List<String> mylist;
	private DisplayImageOptions option;
	
	
	public ImageAdapter(Activity a,List<String> list) {
		// TODO Auto-generated constructor stub
		mylist=list;
		inflater=LayoutInflater.from(a);
		mActivity=a;
		option = new DisplayImageOptions.Builder()
        .cacheInMemory(true)// 是否緩存都內存中  
        .cacheOnDisc(true)// 是否緩存到sd卡上 
        .build();
	}
	
	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return mylist.size()+1;//多一个加号
	}

	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		convertView=inflater.inflate(R.layout.image_adapter, null);
		convertView.setLayoutParams(new LayoutParams(LayoutParams.MATCH_PARENT, (int) (mActivity.getWindowManager().getDefaultDisplay().getWidth()/3.5)));
		ImageView image=(ImageView) convertView.findViewById(R.id.image);
		if(position==mylist.size()){
			image.setScaleType(ScaleType.CENTER_INSIDE);
			image.setImageDrawable(mActivity.getResources().getDrawable(R.drawable.btn_plusimg));
		}else{
			image.setScaleType(ScaleType.CENTER_CROP);
			ImageLoader.getInstance().displayImage(mylist.get(position), image, option);
		}
		return convertView;
	}
	
	
}
