package com.sunday.fangbeibei.adapter;

import java.util.List;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.sunday.fangbeibei.R;
import com.sunday.fangbeibei.activity.RecordHouseActivity;
import com.sunday.fangbeibei.orm.CustomInfo;

/**
 * 客户adapter
 * @author 晨曦
 *
 */
public class RecordCustomAdapter extends BaseAdapter implements OnClickListener{

	private LayoutInflater inflater;
	private Context mContext;
	private List<CustomInfo> mylist;
	
	public RecordCustomAdapter(Context c,List<CustomInfo> list) {
		// TODO Auto-generated constructor stub
		inflater=LayoutInflater.from(c);
		mContext=c;
		mylist=list;
	}
	
	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return mylist.size();
	}

	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		convertView=inflater.inflate(R.layout.record_custom_adapter, null);
		TextView name=(TextView) convertView.findViewById(R.id.name);
		TextView content=(TextView) convertView.findViewById(R.id.content);
		
		ImageView select=(ImageView) convertView.findViewById(R.id.select);
		
		name.setText(mylist.get(position).getName());
		content.setText(mylist.get(position).getMobile());
		
		if(mylist.get(position).isSelected()){
			select.setImageDrawable(mContext.getResources().getDrawable(R.drawable.ico_selct_s));
		}else{
			select.setImageDrawable(mContext.getResources().getDrawable(R.drawable.ico_selct));
		}
		
		convertView.setTag(mylist.get(position));
		
		return convertView;
	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		Intent in=new Intent(mContext, RecordHouseActivity.class);
		mContext.startActivity(in);
	}
	
}
