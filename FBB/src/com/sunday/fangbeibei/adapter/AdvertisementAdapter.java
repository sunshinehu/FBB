/**
 * 2014年7月25日 上午9:23:37
 * TODO
 * @author SunshineHu
 * @email hucx@itouch.com.cn
 */
package com.sunday.fangbeibei.adapter;

import java.util.ArrayList;
import java.util.List;

import android.content.Context;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup.LayoutParams;
import android.widget.ImageView;
import android.widget.ImageView.ScaleType;

import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.assist.ImageScaleType;

/**
 * 2014年7月25日 上午9:23:37
 * TODO
 * @author SunshineHu
 * @email hucx@itouch.com.cn
 */
public class AdvertisementAdapter extends PagerAdapter{

	private List<String> mylists;
	private OnClickListener listener;
	private Context mContext;
	private boolean scaleType;

	
	public AdvertisementAdapter(Context c,List<String> lists,OnClickListener listener,boolean scaleType){
		mContext=c;
		mylists=lists;
		this.listener=listener;
		this.scaleType=scaleType;
		
	}
	/* (non-Javadoc)
	 * @see android.support.v4.view.PagerAdapter#getCount()
	 */
	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return mylists.size();
	}

	/* (non-Javadoc)
	 * @see android.support.v4.view.PagerAdapter#isViewFromObject(android.view.View, java.lang.Object)
	 */
	@Override
	public boolean isViewFromObject(View arg0, Object arg1) {
		// TODO Auto-generated method stub
		return arg0==arg1;
	}

	@Override
	public Object instantiateItem(View container, int position) {
		// TODO Auto-generated method stub
		List<View> viewarray=new ArrayList<View>();
		for(String item:mylists){
			ImageView image=new ImageView(mContext);
			image.setLayoutParams(new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT));
			if(scaleType){
				image.setScaleType(ScaleType.FIT_XY);
			}else{
				image.setScaleType(ScaleType.CENTER_CROP);
			}
			// ImageLoader  
			ImageLoader imageLoader = ImageLoader.getInstance();  
			DisplayImageOptions options;  
			options = new DisplayImageOptions.Builder()
			        .cacheInMemory(true)// 是否緩存都內存中  
			        .cacheOnDisc(true)// 是否緩存到sd卡上 
			        .imageScaleType(ImageScaleType.EXACTLY_STRETCHED).build();
			
			imageLoader.displayImage(item, image, options);
			
			image.setOnClickListener(listener);
			image.setId(position);
			
			viewarray.add(image);
			((ViewPager)container).addView(image);
		}
		
		return viewarray.get(position);
	}

	@Override
	public void destroyItem(View container, int position, Object object) {
		// TODO Auto-generated method stub
		((ViewPager) container).removeView((View) object);
	}

	
}
