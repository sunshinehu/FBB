package com.sunday.fangbeibei.adapter;

import java.util.List;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.sunday.fangbeibei.R;
import com.sunday.fangbeibei.orm.RewardInfo;

/**
 * 

* @ClassName: RewardAdapter 

* @Description: TODO(这里用一句话描述这个类的作用) 

* @date 2015年2月26日 下午2:57:21 

*
 */
public class RewardAdapter extends BaseAdapter{

	private List<RewardInfo> mylist;
	private LayoutInflater inflater;
	private Context mContext;
	private int type;
	
	public RewardAdapter(Context c,List<RewardInfo> list,int type) {
		// TODO Auto-generated constructor stub
		mylist=list;
		inflater=LayoutInflater.from(c);
		this.type=type;
		mContext=c;
	}
	
	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return mylist.size();
	}

	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		if(convertView==null){
			convertView=inflater.inflate(R.layout.reward_adapter, null);
		}
		TextView amount=(TextView) convertView.findViewById(R.id.amount);
		TextView name=(TextView) convertView.findViewById(R.id.name);
		TextView house=(TextView) convertView.findViewById(R.id.house);
		TextView time=(TextView) convertView.findViewById(R.id.time);
		
		if(type==1){
			String ss=mylist.get(position).getCommission();
			if(ss.contains(".")){
				ss=ss.substring(0, ss.indexOf("."));
			}
			amount.setText(ss);
			amount.setTextColor(mContext.getResources().getColor(R.color.orange));
		}else if(type==2){
			String ss=mylist.get(position).getAmount();
			if(ss.contains(".")){
				ss=ss.substring(0, ss.indexOf("."));
			}
			amount.setText("+"+ss);
			amount.setTextColor(mContext.getResources().getColor(R.color.orange));
		}else if(type==3){
			String ss=mylist.get(position).getNopyedcommission();
			if(ss.contains(".")){
				ss=ss.substring(0, ss.indexOf("."));
			}
			amount.setText(ss);
			amount.setTextColor(mContext.getResources().getColor(R.color.darkgrey));
		}
		name.setText("经纪人·"+mylist.get(position).getManagerName()+"/"+mylist.get(position).getUserName());
		house.setText("/"+mylist.get(position).getPremisesName()+mylist.get(position).getFloor()+"·"+mylist.get(position).getRoom());
		time.setText(mylist.get(position).getCreateTime());
		return convertView;
	}

	
	
}
