package com.sunday.fangbeibei.adapter;

import android.app.Activity;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;

import com.nostra13.universalimageloader.core.ImageLoader;
import com.sunday.fangbeibei.R;
import com.sunday.fangbeibei.widget.ZoomableImageView;
import com.sunday.fangbeibei.widget.ZoomableImageView.CommonImageTouchedListener;

public class DynamicImageBrowseAdapter extends PagerAdapter implements CommonImageTouchedListener{
	private Activity mActivity;
	private String[] mylist;
	private LayoutInflater inflater;
	
	public DynamicImageBrowseAdapter(Activity c,String[] images){

		mActivity=c;
		inflater=LayoutInflater.from(c);
		mylist=images;
	}
	
	
	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return mylist.length;
	}

	@Override
	public boolean isViewFromObject(View arg0, Object arg1) {
		// TODO Auto-generated method stub
		return arg0==arg1;
	}

	@Override
	public void destroyItem(View container, int position, Object object) {
		// TODO Auto-generated method stub
		((ViewPager) container).removeView((View) object);
	}

	@Override
	public Object instantiateItem(View container, int position) {
		// TODO Auto-generated method stub
		ZoomableImageView view=new ZoomableImageView(mActivity);
		view.setBackgroundColor(mActivity.getResources().getColor(R.color.black));
		view.setLayoutParams(new LinearLayout.LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT));
		ImageLoader.getInstance().displayImage(mylist[position], view);
		view.setOnImageTouchedListener(this);
		((ViewPager)container).addView(view);
		return view;
	}


	@Override
	public void onImageTouched() {
		// TODO Auto-generated method stub
		mActivity.finish();
	}


}
