package com.sunday.fangbeibei.adapter;

import java.util.List;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.sunday.fangbeibei.R;
import com.sunday.fangbeibei.activity.DynamicImageBrowseActivity;
import com.sunday.fangbeibei.cache.ApplicationCache;
import com.sunday.fangbeibei.orm.BlogInfo;
import com.sunday.fangbeibei.orm.BlogReply;
import com.sunday.fangbeibei.orm.BlogUser;
import com.sunday.fangbeibei.widget.NoScrollGridView;

public class DynamicAdapter extends BaseAdapter implements OnItemClickListener,OnClickListener{
	Context mContext;
	private DisplayImageOptions option;
	//private String[] images={"drawable://"+R.drawable.img1,"drawable://"+R.drawable.img2,"drawable://"+R.drawable.img3};

	private OnButtonClickListener listener;
	
	private List<BlogInfo> mylist;
	
	public DynamicAdapter(Context context,List<BlogInfo> list) {
		this.mContext = context;
		option=new DisplayImageOptions.Builder().cacheInMemory(true)
				.cacheOnDisc(true)
				.build();
		mylist=list;
	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return mylist.size();
	}

	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return position;
	}

	@Override
	public View getView(final int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		if(convertView==null){
			convertView = LayoutInflater.from(mContext).inflate(
					R.layout.dynamic_adapter, null);
		}
		
		BlogInfo info=mylist.get(position);
		
		NoScrollGridView gridView = (NoScrollGridView) convertView.findViewById(R.id.gridView);
		gridView.setSelector(new ColorDrawable(Color.TRANSPARENT));
		ImageView logo=(ImageView) convertView.findViewById(R.id.logo);
		TextView name = (TextView) convertView.findViewById(R.id.name);
		TextView time = (TextView) convertView.findViewById(R.id.publish_time);
		TextView content = (TextView) convertView.findViewById(R.id.content);
		LinearLayout replyll=(LinearLayout) convertView.findViewById(R.id.replyll);
		
		TextView job=(TextView) convertView.findViewById(R.id.job);
		TextView house=(TextView) convertView.findViewById(R.id.house);
		
		if(info.getBlog().getImage()==null||info.getBlog().getImage().equals("")){
			logo.setImageDrawable(mContext.getResources().getDrawable(R.drawable.pic_head));
		}else{
			ImageLoader.getInstance().displayImage(info.getBlog().getImage(), logo,option);
		}
		
		content.setText(info.getBlog().getSimpleContent());
		
		if(info.getAuthor().equals("admin")){

			name.setText("房贝贝");
			job.setText("/管理员");
			
		}else{
			
			name.setText(info.getAuthor());
			job.setText("/"+info.getStoreName());
			
		}
		
		house.setText(info.getPremisesName());
		time.setText(info.getCreateTime());
		
		if(info.getImages()!=null&&info.getImages().size()>0){
			
			gridView.setVisibility(View.VISIBLE);
			
			String[] images=new String[info.getImages().size()];
			for(int i=0;i<info.getImages().size();i++){
				images[i]=info.getImages().get(i).getImage();
			}
			gridView.setAdapter(new DynamicImageAdapter(images, mContext));
			gridView.setTag(images);
			gridView.setOnItemClickListener(this);
		}else{
			gridView.setVisibility(View.GONE);
		}
		
		
		//分享
		ImageView share=(ImageView) convertView.findViewById(R.id.share);
		share.setTag(position);
		share.setOnClickListener(this);
		
		
		if(ApplicationCache.INSTANCE.getUserInfo().getType()==3){
			
			//c不需要查看评论列表和点赞回复
			RelativeLayout replyrl=(RelativeLayout) convertView.findViewById(R.id.replyrl);
			replyrl.setVisibility(View.GONE);
			return convertView;
			
		}
		
		
		TextView praise=(TextView) convertView.findViewById(R.id.praise);
		TextView comment=(TextView) convertView.findViewById(R.id.comment);
		
		praise.setTag(position);
		comment.setTag(position);
		
		praise.setOnClickListener(this);
		comment.setOnClickListener(this);
		
		boolean flag=false;
		
		if(info.getFavUser().size()>0||info.getReplys().size()>0){
			
			//点赞
			replyll.setVisibility(View.VISIBLE);
			
			replyll.removeAllViews();
			
			if(info.getFavUser().size()>0){
				LinearLayout ll=(LinearLayout) LayoutInflater.from(mContext).inflate(R.layout.reply_adapter, null);
				TextView tv1=(TextView) ll.findViewById(R.id.replyname);
				tv1.setGravity(Gravity.CENTER_VERTICAL);
				Drawable drawable= mContext.getResources().getDrawable(R.drawable.praise_icon);  
				/// 这一步必须要做,否则不会显示.  
				drawable.setBounds(0, 0, drawable.getMinimumWidth(), drawable.getMinimumHeight());  
				tv1.setCompoundDrawables(drawable,null,null,null);  
				String names="";
				for(BlogUser user:info.getFavUser()){
					
					if(user.getMemberId().equals(ApplicationCache.INSTANCE.getUserInfo().getId())){
						flag=true;
					}
					
					if(names.equals("")){
						names=user.getMemberName();
					}else{
						names=names+","+user.getMemberName();
					}
				}
				tv1.setText(" "+names);
				replyll.addView(ll);
			}
			
			for(BlogReply reply:info.getReplys()){
				LinearLayout ll1=(LinearLayout) LayoutInflater.from(mContext).inflate(R.layout.reply_adapter, null);
				TextView tv3=(TextView) ll1.findViewById(R.id.replyname);
				TextView tv4=(TextView) ll1.findViewById(R.id.replycontent);
				tv3.setText(reply.getMemberName()+":");
				tv4.setText(reply.getContent());
				ll1.setLayoutParams(new LayoutParams(LayoutParams.MATCH_PARENT,LayoutParams.WRAP_CONTENT));
				
				replyll.addView(ll1);
			}
		}else{
			
			replyll.setVisibility(View.GONE);
			
		}
		
			
		if(flag){
			praise.setText("  取消赞");
		}else{
			praise.setText("     赞");
		}
		
		return convertView;
	}

	@Override
	public void onItemClick(AdapterView<?> parent, View view, int position,
			long id) {
		// TODO Auto-generated method stub
		Intent in=new Intent(mContext, DynamicImageBrowseActivity.class);
		in.putExtra("images", (String[])parent.getTag());
		in.putExtra("pos", position);
		mContext.startActivity(in);
	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		
		int tag=(Integer) v.getTag();
		switch(v.getId()){
		
		case R.id.praise:
			listener.praise(tag);
			break;
		case R.id.comment:
			listener.comment(tag);
			break;
		case R.id.share:
			listener.share(tag);
			break;
		
		}
	}
	

	public void setOnButtonClickListener(OnButtonClickListener listener){
		
		this.listener=listener;
		
	}


	
	public static interface OnButtonClickListener {

		public void praise(int position);
		public void comment(int position);
		public void share(int position);
		
	}
	
}
