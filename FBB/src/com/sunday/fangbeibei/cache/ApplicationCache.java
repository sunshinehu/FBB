package com.sunday.fangbeibei.cache;

import com.sunday.fangbeibei.orm.AreaInfo;
import com.sunday.fangbeibei.orm.UserInfo;

/**
 * 

* @ClassName: ApplicationCache 

* @Description: TODO(程序运行期缓存) 

* @author sunshineHu hucx@itouch.com.cn

* @date 2015年1月20日 下午1:20:11 

*
 */
public enum ApplicationCache {

	INSTANCE;
	
	private UserInfo userInfo;
	private AreaInfo areaInfo;
	
	private int unread;//未读消息条目

	public UserInfo getUserInfo() {
		return userInfo;
	}

	public void setUserInfo(UserInfo userInfo) {
		this.userInfo = userInfo;
	}

	public AreaInfo getAreaInfo() {
		return areaInfo;
	}

	public void setAreaInfo(AreaInfo areaInfo) {
		this.areaInfo = areaInfo;
	}

	public int getUnread() {
		return unread;
	}

	public void setUnread(int unread) {
		this.unread = unread;
	}

}

