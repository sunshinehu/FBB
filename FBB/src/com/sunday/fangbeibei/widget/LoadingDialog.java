package com.sunday.fangbeibei.widget;

import android.app.Dialog;
import android.content.Context;
import android.graphics.drawable.AnimationDrawable;
import android.view.Gravity;
import android.widget.ImageView;
import android.widget.TextView;

import com.sunday.fangbeibei.R;

public class LoadingDialog extends Dialog {

	private static LoadingDialog customProgressDialog = null;
	private AnimationDrawable animationDrawable;

	public LoadingDialog(Context context) {
		super(context);
	}

	public LoadingDialog(Context context, int theme) {
		super(context, theme);
	}

	public static LoadingDialog createDialog(Context context) {
		synchronized (LoadingDialog.class) {
			customProgressDialog = new LoadingDialog(context,
					R.style.CustomProgressDialog);
			customProgressDialog
					.setContentView(R.layout.loadingdialog_layout);
			customProgressDialog.getWindow().getAttributes().gravity = Gravity.CENTER;
		}
		return customProgressDialog;
	}

	@Override
	public void onWindowFocusChanged(boolean hasFocus) {

		if (customProgressDialog == null) {
			return;
		}

		ImageView imageView = (ImageView) customProgressDialog.findViewById(R.id.logingdialog_imageview);
		imageView.setImageResource(R.drawable.loading_anim);
		animationDrawable = (AnimationDrawable) imageView.getDrawable();
		animationDrawable.start();
	}

	@Override
	public void dismiss() {
		// TODO Auto-generated method stub
		super.dismiss();
		if (animationDrawable != null && animationDrawable.isRunning()) {
			animationDrawable.stop();
		}

	}

	public LoadingDialog setTitile(String strTitle) {
		return customProgressDialog;
	}

	public LoadingDialog setMessage(String strMessage) {
		TextView tvMsg = (TextView) customProgressDialog
				.findViewById(R.id.loaingdialog_textview);

		if (tvMsg != null) {
			tvMsg.setText(strMessage);
		}

		return customProgressDialog;
	}

}
