package com.sunday.fangbeibei.fragment;

import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import cn.sharesdk.framework.ShareSDK;
import cn.sharesdk.onekeyshare.OnekeyShare;

import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.sunday.fangbeibei.R;
import com.sunday.fangbeibei.activity.AgentManagerActivity;
import com.sunday.fangbeibei.activity.FeedbackActivity;
import com.sunday.fangbeibei.activity.LoginActivity;
import com.sunday.fangbeibei.activity.MessageActivity;
import com.sunday.fangbeibei.activity.RewardCountActivity;
import com.sunday.fangbeibei.activity.UserInfoActivity;
import com.sunday.fangbeibei.cache.ApplicationCache;
import com.sunday.fangbeibei.tool.VersionUpdate;

/**
 * 我的
 * @author 晨曦
 *
 */
public class MineFragment extends Fragment implements OnClickListener{

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onActivityCreated(savedInstanceState);
	}

	
	
	@Override
	public void onStart() {
		// TODO Auto-generated method stub
		super.onStart();
		loadView();
	}



	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		return inflater.inflate(R.layout.mine_fragment, null);
	}

	private void loadView(){
		
		RelativeLayout toprl=(RelativeLayout) getActivity().findViewById(R.id.toprl);
		toprl.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, (int) (getActivity().getWindowManager().getDefaultDisplay().getWidth()/1.815)));
		
		TextView title=(TextView) getActivity().findViewById(R.id.title);
		ImageView back=(ImageView) getActivity().findViewById(R.id.back);
		ImageView logo=(ImageView) getActivity().findViewById(R.id.logo);
		TextView logout=(TextView) getActivity().findViewById(R.id.logout);
		TextView name=(TextView) getActivity().findViewById(R.id.name);
		TextView company=(TextView) getActivity().findViewById(R.id.company);
		RelativeLayout version= (RelativeLayout) getActivity().findViewById(R.id.version);
		
		LinearLayout leftll=(LinearLayout) getActivity().findViewById(R.id.leftll);
		LinearLayout rightll=(LinearLayout) getActivity().findViewById(R.id.rightll);
		
		LinearLayout message=(LinearLayout) getActivity().findViewById(R.id.message);
		LinearLayout feedback=(LinearLayout) getActivity().findViewById(R.id.feedback);
		LinearLayout share=(LinearLayout) getActivity().findViewById(R.id.share);
		
		TextView versionName=(TextView) getActivity().findViewById(R.id.versionname);
		versionName.setText("V"+getVersionName());
		
		message.setOnClickListener(this);
		feedback.setOnClickListener(this);
		share.setOnClickListener(this);
		
		leftll.setOnClickListener(this);
		rightll.setOnClickListener(this);
		
		ImageView right=(ImageView) getActivity().findViewById(R.id.right_icon);
		right.setOnClickListener(this);
		
		if(ApplicationCache.INSTANCE.getUserInfo().getPath()!=null&&!ApplicationCache.INSTANCE.getUserInfo().getPath().equals("")){
			DisplayImageOptions option = new DisplayImageOptions.Builder().cacheInMemory(true).cacheOnDisc(true).build();
			ImageLoader.getInstance().displayImage(ApplicationCache.INSTANCE.getUserInfo().getPath(), logo, option);
		}
		
		
		logo.setOnClickListener(this);
		version.setOnClickListener(this);
		logout.setOnClickListener(this);
		back.setVisibility(View.GONE);
		title.setText("我的");
		
		name.setText(ApplicationCache.INSTANCE.getUserInfo().getName());
		company.setText(ApplicationCache.INSTANCE.getUserInfo().getStoreName());
		
		if(ApplicationCache.INSTANCE.getUserInfo().getType()==3){
			//c 
			LinearLayout bottom=(LinearLayout) getActivity().findViewById(R.id.bottom);
			bottom.setVisibility(View.GONE);
			company.setVisibility(View.GONE);
		}else if(ApplicationCache.INSTANCE.getUserInfo().getType()==2){
			//b
			LinearLayout bottom=(LinearLayout) getActivity().findViewById(R.id.bottom);
			bottom.setVisibility(View.GONE);
		}
		
	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		switch(v.getId()){
		
		case R.id.logo:
			Intent in=new Intent(getActivity(), UserInfoActivity.class);
			startActivity(in);
			break;
		case R.id.version:
			VersionUpdate update=new VersionUpdate(getActivity());
			update.checkVersion(false);
			break;
		case R.id.logout:
			if(ApplicationCache.INSTANCE.getUserInfo()!=null){
				ApplicationCache.INSTANCE.setUserInfo(null);
				SharedPreferences settings = getActivity().getSharedPreferences("fbb", 0);
				SharedPreferences.Editor editor = settings.edit();
				editor.putString("password", "");
				editor.commit();
				Intent intent=new Intent(getActivity(), LoginActivity.class);
				startActivity(intent);
			}
			break;
		case R.id.right_icon:
			Intent in2=new Intent(getActivity(), UserInfoActivity.class);
			startActivity(in2);
			break;
		case R.id.leftll:
			Intent in3=new Intent(getActivity(), AgentManagerActivity.class);
			startActivity(in3);
			break;
		case R.id.rightll:
			Intent in4=new Intent(getActivity(), RewardCountActivity.class);
			startActivity(in4);
			break;
		case R.id.message:
			Intent in5=new Intent(getActivity(), MessageActivity.class);
			startActivity(in5);
			break;
		case R.id.feedback:
			Intent in6=new Intent(getActivity(), FeedbackActivity.class);
			startActivity(in6);
			break;
		case R.id.share:
			showShare("http://admin.o2o2m.com/app/download?appcode=fbb");
			break;
		}
	}
	
	


	private void showShare(String url){
	   ShareSDK.initSDK(getActivity());
	   OnekeyShare oks = new OnekeyShare(); 
	   // 分享时Notification的图标和文字
	   oks.setNotification(R.drawable.icon, getString(R.string.app_name));
	   oks.setText(url);
	   oks.setDialogMode();
	   // 启动分享GUI
	   oks.show(getActivity());
	}
	
	
	/*
	 * 获取当前程序的版本号 
	 */
	private String getVersionName(){
		
		//获取packagemanager的实例 
		PackageManager packageManager = getActivity().getPackageManager();
		//getPackageName()是你当前类的包名，0代表是获取版本信息
		PackageInfo packInfo;
		try {
			packInfo = packageManager.getPackageInfo(getActivity().getPackageName(), 0);
		} catch (NameNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return "";
		}
	    return packInfo.versionName; 
	}
	
	
}
